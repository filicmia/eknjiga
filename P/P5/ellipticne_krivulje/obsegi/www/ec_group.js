var a = 1;
var b = 1;
var base = 13;
var points = [];
var Pset = false;
var Qset = false;
var Rset = false;
var chart;
var P = [];
var Q = [];
var explanationShown = false;
var info = [-1, -1];

var rangeaConfig = {
    polyfill: false,
    onSlideEnd: function(position, value){
        a = value;
        $("#label_a").text("a = " + a);

        plot();
    }, 
    onSlide: function(position, value){
        a = value;
        $("#label_a").text("a = " + a);
    }
  }

var rangebConfig = {
    polyfill: false,
    onSlideEnd: function(position, value){
        b = value;
        $("#label_b").text("b = " + b);

        plot();
    },
    onSlide: function(position, value){
        b = value;
        $("#label_b").text("b = " + b);
    }
  }

function checkValidCurve() {
    var D = (4*a*a*a + 27*b*b)%base;
    if (D == 0) {
        $("#curve_equation").html("Ta eliptična krivulja ne ustreza pogoju, da mora biti 4*a<sup>3</sup>+27*b<sup>2</sup> mod p različen od 0");
        $("#curve_equation").css("color", "red");
        return false;
    } else {
        $("#curve_equation").html("y<sup>2</sup> = x<sup>3</sup> + " + a + "*x + " + b);
        $("#curve_equation").css("color", "black");
        return true;
    }
}

function isPrime(number)
 { 
   if (number <= 1)
   return false;

   // The check for the number 2 and 3
   if (number <= 3)
   return true;

   if (number%2 == 0 || number%3 == 0)
   return false;

   for (var i=5; i*i<=number; i=i+6)
   {
      if (number%i == 0 || number%(i+2) == 0)
      return false;
   }

   return true;
 }

function calculateInverse(n,p) {
    //console.log(n,p)
    if(!isNaN(n) && !isNaN(p) && n>0 && p>0) {
        if(n == 1) {
            return 1;
        }
        var a = [0,1];
        var remainder = p;
        var myn = n;
        var myp = p;
        var iteration = 0;
        var inverse;
        while(remainder != 0) {
            //console.log(remainder);
            var c = Math.floor(myp/myn);
            remainder = myp%myn;
            a.push(mod(a[iteration] - a[iteration+1]*c, p));
            
            myp = myn;
            myn = remainder;

            if(remainder == 1) {
                inverse = a[a.length-1];
            }
            iteration++;
        }
        
        return inverse;
    } else {
        return NaN;
    }
}

function mod(n, m) {
    return ((n % m) + m) % m;
}


function resetCanvas() {
    $("#chart_ec_finite").remove();
    $("#canvas").append("<canvas id='chart_ec_finite' ></canvas>");
}

function selectPoint(event, activeElements) {
    if(activeElements[0] !== undefined) {
        var elementIndex = activeElements[0]._index;
        var datasetIndex = activeElements[0]._datasetIndex;
        var point = this.data.datasets[datasetIndex].data[elementIndex];
        
        if(!Pset) {
            //if P = Q don't delete from all points
            if(!(Qset && Q[0] == point.x && Q[1] == point.y)) {
                this.data.datasets[datasetIndex].data.splice(elementIndex, 1);
            }
            

            this.data.datasets.unshift({
                data: [point],
                pointBackgroundColor: "red",
                pointRadius: 5,
                id: "P"
            })
            Pset = true;
            P = [point.x, point.y];

            $("#P").text("P = (" + point.x + ", " + point.y + ")");
            $("#P_container").show();
            
        } else if (!Qset) {
            //if P = Q don't delete from all points
            if(!(Pset && P[0] == point.x && P[1] == point.y)) {
                this.data.datasets[datasetIndex].data.splice(elementIndex, 1);
            }

            this.data.datasets.unshift({
                data: [point],
                pointBackgroundColor: "yellow",
                pointRadius: 5,
                id: "Q"
            })
            Qset = true;
            Q = [point.x, point.y];

            $("#Q").text("Q = (" + point.x + ", " + point.y + ")");
            $("#Q_container").show();
            
        }

        this.update();

        //infoPoint(point);
    }

    if(Pset && Qset && !Rset) {
        calculateR();
    }
    
}

function calculateR() {
    
    Rset = true;
    $("#R_container").show();

    var lambda;
    // if P = Q
    if(P[0] == Q[0] && P[1] == Q[1] && P[1] != 0 && Q[1] != 0) {
        var inverse = calculateInverse(2*P[1], base);
        lambda = mod((3*P[0]*P[0] + a) * inverse, base);
        $("#lambda").text("$$\\lambda = {(3*x^2 + a) * (2 * y)^{-1} \\text{ mod p} } = \\\\ { (3*" + P[0] + "^2 + " + addParantheses(a) + ") * (2* " + P[1] + ")^{-1} \\text{ mod " +  base + "}} = \\\\ { " + (3*P[0]*P[0] + a) + "*" + (2*P[1]) + "^{-1} \\text{ mod " +  base + "}} = \\\\ { " + mod(3* + P[0]*P[0] + a, base) + "*" + inverse + " \\text{ mod " +  base + "}} =" + lambda + " $$");
    } 
    //if P = -Q
    else if(P[0] == Q[0] || P[1] == 0 || Q[1] == 0) {
        $("#R").html("R = &infin;");
        $("#lambda").html("Ker je P = -Q, je P + Q = &infin;");
        return;
    } else {
        var inversetobe = mod(P[0]-Q[0], base)
        var inverse = calculateInverse(inversetobe, base)
        lambda = mod((P[1]-Q[1]) * inverse, base);
        $("#lambda").text("$$\\lambda = {(y_1 - y_2) * (x_1 - x_2)^{-1} \\text{ mod p} } = \\\\ {" + addParantheses(P[1] - Q[1]) + " * " + addParantheses(P[0] - Q[0]) + "^{-1} \\text{ mod " +  base + "}} = \\\\ {" + addParantheses(P[1] - Q[1]) + " * " + inversetobe + "^{-1} \\text{ mod " +  base + "}} = \\\\{" + (P[1]-Q[1]) + " * " + inverse + " \\text{ mod " + base + "}} = " + lambda + " $$")
    }
    MathJax.Hub.Queue(["Typeset",MathJax.Hub, 'lambda']);

    Rx = mod(lambda*lambda - P[0] - Q[0], base);
    Ry = mod(lambda*(P[0] - Rx) - P[1], base);
    
    $("#Rx").text("$$x_3 = {\\lambda^2 - x_1 - x_2 \\text{ mod p }} = \\\\ { " + lambda*lambda + " - " + P[0] + " - " + Q[0] + " \\text{ mod " + base + "}} = " + Rx + " $$");
    MathJax.Hub.Queue(["Typeset",MathJax.Hub, 'Rx']);

    $("#Ry").text("$$y_3 = {\\lambda(x_1 - x_3) - y_1 \\text{ mod p} } = \\\\ {" + lambda + "(" + P[0] + " - " + Rx + ") - " + P[1] + " \\text{ mod " + base + "}} = " + Ry + "$$");
    MathJax.Hub.Queue(["Typeset",MathJax.Hub, 'Ry']);

    $("#R").text("R = (" + Rx + ", " + Ry + ")");

    drawR(Rx, Ry);

    
    

    
    
}

//if result is infinity, it returns empty array
function calculateRBare(mP, mQ) {
    var lambda;
    // if P = Q
    if(mP[0] == mQ[0] && mP[1] == mQ[1] && mP[1] != 0 && mQ[1] != 0) {
        var inverse = calculateInverse(2*mP[1], base);
        lambda = mod((3*mP[0]*mP[0] + a) * inverse, base);
    } 
    //if P = -Q
    else if(mP[0] == mQ[0] || P[1] == 0 || Q[1] == 0) {
        return [];
    } else {
        var inversetobe = mod(mP[0]-mQ[0], base)
        var inverse = calculateInverse(inversetobe, base)
        lambda = mod((mP[1]-mQ[1]) * inverse, base);
        
    }  

    Rx = mod(lambda*lambda - mP[0] - mQ[0], base);
    Ry = mod(lambda*(mP[0] - Rx) - mP[1], base);

    return [Rx, Ry];

}

function drawR(Rx, Ry) {
    //deletePointFromAllDataset(Rx, Ry);

    chart.data.datasets.unshift({
        data: [{x: Rx, y: Ry}],
        pointBackgroundColor: "#98FB98",
        pointRadius: 5,
        id: "R"
    });
    chart.update();
}

function deletePointFromAllDataset(Rx, Ry) {
    var datasetIndex = getDatasetIndex(chart, "all");
    for(var i = 0; i<chart.data.datasets[datasetIndex].data.length; i++) {
        var point = chart.data.datasets[datasetIndex].data[i];
        if(point.x == Rx && point.y == Ry) {
            chart.data.datasets[datasetIndex].data.splice(i, 1);
            break;
        }
    }
}

function getDatasetIndex(chart, id) {
    for(var i=0; i<chart.data.datasets.length; i++) {
        if (chart.data.datasets[i].id === id) {
            return i;
        }
    }
    return -1;
}

function addParantheses(x) {
    if(x < 0) {
        return "("+x+")";
    }
    else {
        return x;
    }
}

function resetAddition() {
    Pset = false;
    Qset = false;
    Rset = false;
    
    $("#P_container").hide();
    $("#Q_container").hide();
    $("#R_container").hide();
}

function hideExplanation() {
    $("#lambda").text("");
    $("#Rx").text("");
    $("#Ry").text("");
    if(explanationShown) {
        explanationShown = false;
        $('#div_explaination').toggle('1000');
        $("#button_explanation").toggleClass("fa-angle-double-down fa-angle-double-up");
    }
    
}

/*function infoPoint(event, activeElements) {
    if(activeElements[0] !== undefined) {
        var elementIndex = activeElements[0]._index;
        var datasetIndex = activeElements[0]._datasetIndex;
        var point = this.data.datasets[datasetIndex].data[elementIndex];*/
//the problem with info is that it takes some time to compute and it can crash the page for an impatient user
function infoPoint(point) {
        if(point.x != info[0] || point.y != info[1]) {
            $("#point").text("(" + point.x + ", " + point.y + ")");
            $("#point_inverse").text("(" + point.x + ", " + mod(-point.y, base) + ")");

            var mP = [point.x, point.y];
            var mQ = [point.x, point.y];
            var subgroupText = "";
            info = mP;

            //because of infinity
            var subgroupOrder = 1;
            
            while(mQ.length > 0) {
                subgroupOrder++;
                subgroupText += "(" + mQ[0] + ", " + mQ[1] + ") &rarr; ";

                mQ = calculateRBare(mP, mQ);
                console.log("calculating for point " + mP);
            }

            subgroupText += "&infin;";

            $("#subgroup_order").html(subgroupOrder);
            $("#subgroup").html(subgroupText);

            $("#additional_info").show();

            

            console.log("calculation done for point " + mP);
        }

        
    //}
}

function plot() {

    

    if(checkValidCurve()) {
        reset();
        calculatePoints();

        var ctx = document.getElementById("chart_ec_finite").getContext('2d');
        chart = new Chart(ctx, {
            type: 'scatter',
            data: {
                datasets: [{
                    data: points,
                    pointBackgroundColor: "#698e8e",
                    pointRadius: 5,
                    id: "all"
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom'
                    }],
                    
                },
                animation: false,
                legend: {
                    display: false
                },
                onClick: selectPoint,
                //onHover: infoPoint,
                
            }
        });
        
        //console.log("end plot");
    } else {
       showEmptyPlot();
    }
    
    
}



function reset() {
    hideExplanation();
    resetAddition();
    resetCanvas();
    $("#additional_info").hide();
}

function showEmptyPlot() {
    reset();
    var ctx = document.getElementById("chart_ec_finite").getContext('2d');
    chart = new Chart(ctx, {
        type: 'scatter',
        data: {
            datasets: [{
                data: [],
                pointBackgroundColor: "#698e8e",
                pointRadius: 5,
                id: "all"
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'linear',
                    position: 'bottom'
                }],
                
            },
            animation: false,
            legend: {
                display: false
            },
            onClick: selectPoint,
            onHover: infoPoint,
            
        }
    });
}

function calculatePoints() {
    points = [];
    for(var x=0; x<base; x++) {
        var right = (x*x*x + a*x + b)%base;
        for(var y=0; y<Math.ceil(base/2); y++) {
            var left = (y*y)%base;
            if(left == right) {
                points.push({x: x, y: y});
                if(y !== 0) {
                   points.push({x: x, y: mod(-y, base)}); 
                }
                
            }
        }
    }
    //console.log("points calculated")
    $("#number_of_points").text("Skupaj z neskončnostjo je v grupi " + (points.length+1) + " točk.");
}

function resetSlider(slider, max) {
    slider.attr("max", max);
    //slider.val(1).change();
    slider.rangeslider('update', true);
}

function changeBase() {
    
    var newBase = parseInt($("#base").val(), 10);
    if(isPrime(newBase)) {
        if(newBase < 5000) {
            base = newBase;
            $("#not_prime").text("");

            resetSlider($("#range_a"), base-1);
            resetSlider($("#range_b"), base-1);
            plot();
        } else {
            $("#not_prime").text("Zaradi preglednosti izrisanih točk na krivulji, je največji dovoljen modulo enak 4999");
            $("#not_prime").css("color", "red");
            showEmptyPlot();
        }
        
    } else {
        $("#not_prime").text(newBase + " ni praštevilo. Izbrati moraš praštevilo");
        $("#not_prime").css("color", "red");
        showEmptyPlot();
    }
    
    
    
}

function deletePQ() {
    var datasetIndex;
    if(this.id === "delete_P") {
        datasetIndex = getDatasetIndex(chart, "P");

    } else if(this.id === "delete_Q") {
        datasetIndex = getDatasetIndex(chart, "Q");
    }

    

    // if P != Q
    if(!(P[0] == Q[0] && P[1] == Q[1])) {
        // get the point
        var point = chart.data.datasets[datasetIndex].data[0];
        //add point to all points
        points.push(point);
        chart.data.datasets[chart.data.datasets.length-1].data = points;
    }
    
    
    //delete point from its own array
    chart.data.datasets.splice(datasetIndex, 1)[0];

    //R point
    var datasetIndexR = getDatasetIndex(chart, "R");
    //console.log(datasetIndexR);
    //console.log(chart.data.datasets);
    
    //if R point exists
    if(datasetIndexR > -1) {
        // get the point
        var point = chart.data.datasets[datasetIndexR].data[0];
        
        //add point to all points
        /*points.push(point);
        chart.data.datasets[changeBase.data.datasets.length-1].data = points; */

        //delete R from its own array
        chart.data.datasets.splice(datasetIndexR, 1)[0];
        
    }
    
    Rset = false;
    //update chart
    chart.update();

    if(this.id === "delete_P") {
        Pset = false;
        $("#P_container").hide();
        

    } else if(this.id === "delete_Q") {
        Qset = false;
        $("#Q_container").hide();
    }

    hideExplanation();
    $("#R_container").hide();
}

window.addEventListener("load", function(event) {
    $("#range_a").rangeslider(rangeaConfig);
    $("#range_b").rangeslider(rangebConfig);

    $("#button_base").click(changeBase);
    $("#base").on("keypress", function(e) {
        if(e.which === 13) {
            changeBase();
        }
    })

    $(".delete").each(function(index) {
        $(this).on("click", deletePQ);
    });
    
    $("#div_explaination").hide();
    $("#button_explanation").click(function() {
        explanationShown = !explanationShown;
        $('#div_explaination').toggle('1000');
        $(this).toggleClass("fa-angle-double-down fa-angle-double-up");
    });

    $("#additional_info").hide();

    plot();
});