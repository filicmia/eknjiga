# Chipers

When we want to protect the data so that they can not read those people whose message is not intended, we need to protect them in some way.
One of the procedures for achieving this goal is encryption (cipher), and comes from the Hebrew word saphar, which means to count / signify.
With the problem of protecting these messages that people who are not dedicated to it can not decipher, cryptography is being dealt with.
This term is composed of Greek words kryptos, which means a mystery, and a logo that represents a word.

## Cezar chiper 

Caesar's code is one of the oldest and most commonly used forms of encryption.
They were known even in Roman times, and among them was Julius Caesar, who was named after this code.
Encryption is done by deleting each letter of the alphabet for a certain number of characters. It is therefore possible to break the code quite simply, because there are only 24 combinations available, which can be broken into "hands".

### Steps

The procedure can be illustrated by marking each letter in a row, with a number from 1 to 25.
Then we will choose how many places we will shift our cleanliness. Then we convert our text into numbers that belong to a single letter, and then we add the lag.
We deduct 25 digits of all numbers above 25 and then look at the table in which number is any letter, so that we convert the text back to letters.


*alternative:*

A circle of letters can be used instead of a table.
<iframe src="./P4/cezar.html" frameborder="0" scrolling="no" id="the_iframe" width="100%" height='500'></iframe>

First, let's figure out which letter represents our lag. This is the letter which is the nth (lag + 1) in the order of all letters. Then A of the outer circle with our n-letter of the inner circle.
Then we begin to copy the letters one at a time from the pamphlet to the chronicle, but instead of writing the letter itself, we search for it on the outer part of the circle and write the letter that is located on the inner circle in the secret circle.


## Vigenere chiper

Vigener's code is a version of Caesar's code, in which for each number we use the second Caesar's password, which is set by the password.
The code was first recorded in 1553 in the book by Giovan Battista Bellas but was incorrectly attributed to Blaise de Vigenère in the 19th century, from where she also received her name.
The code was extremely easy to implement and explained, but the scholars failed to break for more than 300 years, when Friedrich Kasiski managed to develop a general method of breaking.
For this reason, the name "le chiffre indéchiffrable" (French for the code that can not be broken) was taken at that time.

### Steps

We will reuse the table that we used with Caesar's code. The first step is to convert individual passwords into numbers and then deduct 1 from each.
Then we do this procedure with our cleaner. The first number of the pamphlet is summed up with the first number from the password, the second pamphlet with another password, and so on.
When we run out of passwords, we start the password again with the first number while we clean the letterhead one letter for another.
When we get to the end of all numbers, we deduct 25 digits each and then every number is found in the table, and we record them in a row, which is a secret record.

*alternative*

For this encryption process, we can also use the circle of letters that we used in Caesar's code, which makes this process even easier.
On the outer circle A, we align with the first letter of the password. Then, on the outer circle, find the first letter of the pamphlet. The letter below is the first letter of the secrecy. We do this for each letter.
If we run out of a password, we simply start with it from the beginning.

<iframe src="./P4/cezar_a.html" frameborder="0" scrolling="no" id="cezar_a_iframe" width="100%" height='500'></iframe>

#### Vigenerjeva šifra z krogom

<iframe src="./P4/vignare_step.html" frameborder="0" scrolling="no" id="vignare_step-iframe" width="100%" height='500'></iframe>

#### Vigenerjeva šifra brez kroga

<iframe src="./P4/v_sif.html" frameborder="0" scrolling="no" id="v_sif_iframe" width="100%" height='500'></iframe>


#### Napad na Vigenerjevo šifro

<iframe src="./P4/razbijanje_v.html" frameborder="0" scrolling="no" id="razbijanje_v_iframe" width="100%"></iframe>


## DES

By the construction, DES is a block chiper. As for every block chiper, we would
like for them to be as secure as possible, and as fast as possible.

This is why most of blovk chipers in practice rely on the basic 
construction framework *Iterated chiper*. Instantiations of this kind 
are conducted of two parts:

1. *the round chiper*,
2. *the key expansion function*.

The round chiper is some simple block chiper $\overline{\epsilon} := (\overline{E},
\overline{D})$.
Simplicity brings speed, but reduces security. Lack of $\overline{\epsilon}$
security is not a problem here. It is just one building block of the overall
system.

<blockquote class='quote'>System's security is not measured by the security of each, but the security of
the overall composition.
</blockquote>

In case of DES, one uses *Feistel permutation* to build round chiper 
$\overline{\epsilon}$.

<blockquote class='quote'> *Feistel permutation* is a permutation built from an arbitrary function
 $f: \Psi \to \Psi$ as follows: 
 $\Pi(x,y) := (y, x \xor f(y)), x,y \in \Psi$. It is one
 to one mapping, thus having an inverse.
</blockquote>

In DES, functioin $f$ takes 32-bit input. Consequentally, $\Pi$ operates on
64-bit block. 
<img src='./static/img/DES_round_chiper.jpg'>
> Figure: Description of round chiper which corresponds to applying Feistel
> permutation once.

Number found for DES is 16. Thus,  we are applying *Feistel permutatioin* 16
times.
<blockquote class='quote'>
$n$ chaining applications of the *Feistel permutatioin* is called *n-round Fiestel
network*. In other words, DES is 16-round Feistel network.
</blockquote>
Up to this point we have not used DES's (block chiper's) key. To obtain a decent 
security level, DES has incorporated the chiper key in Feistel permutation
function generator. <br>
Consequentally, each DES round uses a bit different Feistel permutation function 
$f$. Let $f_i$ denote the function used in the $i-th$ round. Then, we have
$$
\begin{align}
f_i &: \Psi \to \Psi \
f_i (x) &:= F(k_i,x)
\end{align}
$$
where $F$ denotes DES round function and $k_i$ the i-th round key $k_i$ 
which is 48-bit long.
<div id="functionF">
  <img src='./static/img/DES_functionF.jpg' id='hover'>
</div>

* $E: \Psi \to \Omega$, $\Psi := \{ 0,1 \}^{32}, \Omega := \{ 0,1 \}^{48}$ 
$E$ rearrange and replicate the output bits to expand the input. <br>
* $P$ is *mixing permutation* from $\Psi$ to $\Psi$ <br>
* S-box maps 6-bit input to 4-bit output. Those are defined by DES standard.<br>

The only non-linear component of the DES round chiper are the S-boxes. Thus, 
those, together with the permutation $P$, form the basis of the DES
security.

As having 16 rounds, DES needs 16 bit key. Those keys are derived from  56-bit
DES 
key using *key expansion function*. Each round key $k_i$ is 48-bit subset of
those 56-bits. Every suubset $k_i$ is unique.

Beyond the *kkey expansion function*, and round chiper, DES consistes of initial
(IP) and final permutation (FP). Those do not have security significance.
Moreover
those are rather slow when implemented in software and really fast when
implemented on hardware. One theory is that those are here to slow 
down software implementations of the DES.

**Caution**
Exhaustive search on DES is rather efficient. Given only three plaintext
chipertext pair block, there is only one DES key that they corresponds to,
with high probability. This ensures that an exhaustive search will find the only
possible key. The time to run an exhaustiive search on a key space of 
size $2^{56}$ was long enough to enable security in 1974. Nowdays, 
it takes only 12.8 days on a single machine (COPACABANA project 2007).
Thus, 56-bit key lenght is way too short for a block chiper to 
resist the exhaustive search attack.

**Triple DES**
It is worth mentioning that, dispite its rather simple structure, DES is
remarable resilent to more sophisticated attacks. That has lead to 
a *trple DES* constructioin. *Triple DES* (3DES) has been derived from *DES*
by streightening it against the most powerful attack on *DES*,
a simple exhaustive key space search attack (the brute-force attack).

*3DES* uses $3*56 = 168$ bits long key and work as follows:

$$
\begin{align}
E_3(k,x) &:= E(k[129:168], E(k[57:128], E(k[1:56],x)))
\end{align}
$$
where $\epsilon = (E,D)$ is a simple *DES* over $(K,\Psi)$, thus giviing a 
*3DES* $\overline{\epsilon}_3 = (E_3, D_3)$ over $(K^3, \Psi)$.

The brute force attack on *3DES* is considered non practical as it
needs to check $2^{168}$ possible keys which is impractical
(and will be for a long period of 'eternity'). 

<iframe src="./P4/DES.html" frameborder="0" scrolling="no" id="DES_iframe"></iframe>

**The Triple-DES standard.** The NIST approved Triple-DES for government use up
to 2030. In precise, the NIST version of Triple-DES is a bit different from the
presented one: 
$$
\begin{align}
E_3(k,x) &:= E(k[129:168], D(k[57:128], E(k[1:56],x)))
\end{align}
$$
The resaon for the reformulation is so that *3DES* implementations can de used for
*DES* encryption as well. 
Setting $k[129:168] = k[57:128] = k[1:56]$ reduces the NIST *3DES* to simeple *DES*.

The interaction is on the following link:
https://filicmia.gitlab.io/html/hidden/DES.html

<blockquote class='quote'>
Although *3DES* iis secure, *2DES* is not. Here, *meets in the middle strategy*
becomes very poverful.
</blockquote>
<script>
  iFrameResize({ log: true, warningTimeout: 10000}, '#cezar_a_iframe, #razbijanje_v_iframe, #DES_iframe')
</script>