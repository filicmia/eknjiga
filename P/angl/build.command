#!/bin/sh

cd /Users/mfilic/Documents/FRI20182019/SIPK/crypto-e-book/ebook-SIPK/P/angl/
cp -r ../static ./
cp -r ../code ./
cp -r ../params.R ./

#Rscript -e "install.packages("knitr", repos = c("https://xran.yihui.name", "https://cran.r-project.org"))"

clear
echo
echo "START BUILDING THE SCRIPT."
echo

export LANG=en_US.UTF-8

echo
echo "Preparing an environment for building a script for the artists."
echo

Rscript -e "bookdown::clean_book(TRUE)"
Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook')"

echo
echo "Updating links and cleaning."
echo

Rscript -e "source('./code/helper.R'); cleanScript()"

rm -rf *.md
rm -rf *.rds

cp -r ../P* ./_crypto_eknjiga/
cp -r ../js-libs* ./_crypto_eknjiga/
cp -r ../static* ./_crypto_eknjiga/
rm -rf ./_crypto_eknjiga/*.md
rm -rf ./_crypto_eknjiga/*.Rmd

echo
echo "Copy sources."
echo

echo
echo "THE END of build."
echo