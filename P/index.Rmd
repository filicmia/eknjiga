--- 
title: "eKripto-knjiga"
author: "LKRV - FRI, UNI LJ"
date: "`r format(Sys.Date(), '%A, %d. %B, %Y', tz='Europe/Zagreb')`"
site: bookdown::bookdown_site
output: bookdown::gitbook
documentclass: book
bibliography: [bibliography.bib]
biblio-style: apalike
link-citations: yes
description: "Crypto ebook 2018/2019"
---

```{r filtriranjeVsebine}
source('params.R')
```

# Uvod {#uvod -}

<!--Mia - all-->
Večina bralcev se je najbrž že srečala z besedo *kriptografija*,
ne ve pa točno, kaj ta veda je. Na kratko povedano, gre za znanost
o skrivnem pisanju in varnem dostopu do podatkov.
Povedano natančneje, kriptografija poskuša omogočiti zasebnost. 
in še naprej zaupnost, avtentičnost, celovitost in preprečevanje zanikanja.
V splošnem uporabljamo naslednja prostopa:

1. simetrična kriptografija,
2. kriptografija javnih ključev.

Glavna razlika med obema je v njunem načinu šifriranja in odšifriranja.
Pustimo to za kasneje in razložimo najprej, 
kaj si lahko predstavljamo pod pojmom *kriptosistem*. 

```{r out.width='60%'}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/Uvod_Anita&Bojan.PNG"))
```

Na sliki je predstavljen osnovni primer komunikacije dveh oseb,
Alice in Bob (ki si imata veliko za povedati). 
Zaradi zaupnosti želita, da njun pogovor ostane skriven. 
V ta namen uporabljata neke vrste kriptografijo.
Pred začetkom morata doreči naslednje stvari:

1. algoritem, ki bo naredil vsako sporočilo med njima skrivno 
	(tj. *šifrirno funkcija*),
2. algoritem za odšifriranje, ki bo omogočila drugi osebi prebrati 
	šifrirano sporočilo (*odšifrirna funkcija*),
3. množico iz katere bosta izbirala skrivne ključe (*prostor ključev*), 
4. množici sporočil in šifranih sporočil, ki jih bosta izmenjevala prek 
	kanala, tj.
   (a) definicijsko območje šifrirne funkcije,
   (b) zaloga vrednosti šifrirne funkcije 
	(definicijsko območje odšifrirne funkcije).

> Zgornjemu bomo rekli opis kriptosistema.

Postopek, s katerim naredimo sporočilo skrivno, imenujemo *šifriranje*. 
Zašifrirano sporočilo imenujemo *tajnopis*.
Postopek, s katerim naredimo sporočilo spet berljivo, 
imenujemo *odšifriranje*. 
Odšifrirano sporočilo je enako začetnemu tekstu, ki ga imenujemo *čistopis*.
Ko je kriptosistem opisan, Alice in Bob lahko začneta pogovor.

0. Kaj se zgodi, ko Alice želi poslati sporočilo Bobu? 

Ko Alice želi poslati sporočilo $m$ Bobu, najprej zašifrira $m$ 
v tajnopis $c$ in ga pošlje Bobu prek kanala (kot to naredi https protokol).
Ko Bob prejme $c$, ga mora najprej odšifrirati, 
da ga lahko prebere. Tajnopis $c$ ne sme biti na lahek način povezan 
s čistopisom $m$ brez poznavanja ključa, saj nočemo, da lahko kdorkoli, 
ki je povezan s komunikacijskim kanalom, prebral sporočilo.

*Primer 1.*
Opišimo en konkreten simetrični kriptosistem. 

(1) Kaj bo šifrirna funkcija?

```{R, echo = T, tidy=FALSE, eval=FALSE}
Enc(m, x):
	for each letter in m:
			letter = letter + x;											
```

Povedano drugače, če imamo abecedo a,b,c,...,z,ž,  v tabeli `a`, 
zašifrira posamično črko s pravilom:

```{R, echo = T, tidy=FALSE, eval=FALSE}
Enc(m, x):
	for each letter in m:
       letter = a[index(a==letter) + x]
```

(2) Kaj bo odšifrirna funkcija?

```{R, echo = T, tidy=FALSE, eval=FALSE}
Dec(c, x):
	for each letter in c:
       letter = a[index(a==letter) - x]
```

Ta kriptosistem uporablja za šifriranje in odšifriranje sporočil 
tokovno šifro.

*Tokovna šifra* šifrira in odšifrira sporočilo črko po črko. \
*Bločna šifra* šifrira in odšifrira sporočilo blok za blokom. \

(3) Katera sporočila želita pošiljati? 
Npr. slovenski tekst (abeceda s 25 črkami, 
v tem primeru presledke in ločila ne bomo šifrirali).

(4) Kako bodo izgledala šifrirana sporočila? 
Neke čudne besede, sestavljene iz slovenskih črk. 

*Primer 2.*
Definirajmo poseben primer zgornjega kriptosistema z določitvijo (skrivnega) parametra
`x`, npr. `x = 3`. \
Preizkusi sam ta postopek oz. uporabi spodnji primer: 

<div class = "blue pre">

če je imel za povedati kar koli skrivnega, je napisal v šifrah, tj.,
s spreminjanjem zaporedja črk v abecedi, v kateri nobene besede ni
moč ugotoviti.

</div>

<script src="./P7/ceaserChiper/www-shared/ceaserChiper.js"></script>
<script src="./P7/ceaserChiper/ceaserEncrypt/ceaserEncrypt.js"></script>
<link rel="stylesheet" type="text/css" href="./P7/ceaserChiper/www-shared/ceaserStyle.css">

<div id='ceaserEncrypt'> Add Chiper!</div>

Za zgornji primer dobimo:

<div class = "pre">

čh mh lpho cd sryhgdwl ndu nrol vnulyqhjd, mh qdslvdo y šliudk, wm., v vsuhplqmdqmhp cdsruhgmd čun
y dehfhgl, y ndwhul qrehqh ehvhgh ql prč xjrwrylwl.

</div>

Preverimo lahko tudi, če je rezultat odšifriranja enak začetnemu sporočilu.

<script src="./P7/ceaserChiper/ceaserDecrypt/ceaserDecrypt.js"></script>
<div id='ceaserDecrypt'></div>


Pridevnik simetrična pomeni, da na obeh straneh (šifriranje/odšifriranje)
uporabimo enak ključ (oz. bolj splošno, lahko ključ za šifriranje izpeljemo iz ključa za
odšifriranje in obratno).

<!--  The adjective symmetric is used to denote the symmetry in encryption -
description process, to emphasise the usage of the same crypto parameter
(or one s.t. encryption parameter can be deduced from the decryption
parametter and vice versa). -->

```{r out.width='65%'}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/Uvod_Anita&Bojan3.PNG"))
```
Po drugi strani bomo kasneje videli, da asimetrični kriptosistemi 
namesto skupnega ključa uporabljajo pare ključev (zasebni,javni).

*Kriptosistem* je algoritem oz. tehnika, ki ohranja skrivnost,
omogoča avtentikacijo, celovitost in prepoznavanje nepoštenih
ljudi [@stinson2018cryptography].
Slednji ima za cilj, da ne sporoči več, 
kot je vidno v šifriranem sporočilu.

<!-- 
V komunikacijskih kanalih
(kjer je cilj zagotoviti skrivnost komunikacije)
smatramo, da  *niso* skrivni:

(1) protokol,
(2) šifrirna funkcija,
(3) prostora čistopisov in tajnopisov.

Edina stvar, ki omogoča kriptosistem varen in uporaben, je modra uporaba
kriptografskih parametrov, tj. skrivnih ključev! -->
<!-- uporabimo raje Kerhoff principle from my slides and put it in the 
box -->

Poudarimo, da so kriptosistemi kontrolirani s ključi, 
ki določijo transformacijo podatkov. 
Seveda imajo ključi digitalno obliko 
(binarno zaporedje: 01001101010101...). 
Za analizo varnosti kriptosistema opazujemo moč napadalca na 
tem sistemu. 
Držali se bomo **Kerckhoffsovega principa**, ki pravi, da 

<blockquote class='quote'>
"napadalec" *pozna kriptosistem oziroma algoritme,
ki jih uporabljamo, ne pa tudi ključe,
ki nam zagotavljajo varnost.*
</blockquote>
Močnejši je napadalec, manj varen je kriptosistem.


<!--https://stackoverflow.com/questions/14667770/how-to-call-a-javascript-function-when-iframe-finished-loading-->
<!--iframe id ='myframe' src='"./P7/ceaserChiper/ceaserDecrypt/
ceaserDecrypt.html"' onload="onLoadHandler();"></iframe-->

