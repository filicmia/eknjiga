# Ključi

<!--Metod - all-->
```{r filtriranjeVsebine, echo=F}
source('../P/params.R')
```
<!--Uvod: TO-DO Poseben primer ključev so gesla in PIN-i.
Sem bi lahko uvrstili še nekaj upravljanja s ključi
in s tem motivirali sim/asim kriptografijo, kodne knjige,...
-->

## Gesla

<div class="row flex-nowrap">
  <div class="col">

```{r out.width='50%'}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/pass1234.jpg"))
```
  </div>
  <div class="col"> 
  Gesla uporabljate vsak dan in povsod. Za odklepanje telefona, za odklepanje prenosnika, prijava v e-pošto, račun za družabna omrežja, do vašega delovnega prostora, za dostop do shranjevanja, morda celo do toalet itd ... Ko nastavljate geslo, imate veliko omejitev. Kot: 1. imeti mora 8 znakov, (2) mora vsebovati vsaj ena črka in (3) ena številka itd. Včasih je moteče, vendar si zada velik cilj zaščitite svojo zasebnost in si geslo naredite tako težko kolikor je mogoče razpokati.
  </div>
</div>
<br>

<div class="row flex-nowrap">
  <div class="col">
**Časovni stroj gesel** <br>
Gesla so res stari zaščitni oraklji. V starih časih (pred začetkom štetja), so lahko napadalci videli stražarje zunaj mestne utrdbe, kateri so varovali mestna vrata. Opremljeni so bili z orožjem in nikogar niso pustili noter brez pravilnega gesla (kodna beseda). 
 <br /><small>Kaj se je zgodilo s tistimi, ki so povedali napačno geslo? 
 Pravilo je bilo: "Poskrbite zanje!"</small>
<br>
<br>
Kljub imenu geslo – `password`, geslu ni potrebno biti dejanska, smiselna beseda. 
Lahko je alfa-numerično in vsebuje kup posebnih znakov in presledkov. Več jih je, boljša je varnost gesla. To nam omogoča, da razširimo geslo v cele stavke ali poglavje v knjigi (ne pozabite da imamo mesto). 
Geslom, ki vsebujejo več kot eno pomenljivo besedo, pravimo skrivna fraza 
  [*passphrase*](http://en.wikipedia.org/wiki/Passphrase). 
<small>Ni treba, da ta fraza pomeni nekaj, besede so lahko vzete iz konteksta.</small>

Če želite biti čim bolj preprosti in izbirati, tim. enobesedno geslo, se boste
soočili  z občutno zmanjšano varnostjo. Napadalec zna jezik, ki ga uporabljate,
lahko bi vlomil v približno 5 urah z uporabo napadov s slovarji, npr. poskuša
najti ujemanje besed določenega jezika. Dandanes obstajajo celo podobni napadi
na enobesedno geslo, kljub temu da ne vedo jezika.
 
<br>
Kaj pa 2-besedna fraza? Geslo je možno razbiti na isti način z nekoliko večjo
(stilno sprejemljiva) količino časa in moči računalnika.
Po drugi strani pa 8-besednih gesel ni enostavno razbiti s pomočjo napada s
slovarji, zato velja za dovolj varnega. Seveda se to zgodi le če napadalec ugane
strukturo gesla. <br>


  </div>
  <div class="col">

```{r out.width='50%'}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/roman_guard.jpg"))
```
  </div>
</div>
<blockquote class='quote'>
Slovarski napad je poskus ugibanja gesla z uporabo dobro znanih besed ali fraz. Ker večino gesel izberejo uporabniki, je utemeljeno, da večina gesel sestoji iz skupnih besed. V angleškem jeziku je nekaj več kot milijon besed in obstaja 308.915.776 možnih kombinacij 6 črk. Večina napadalcev bo računala na to, ko poskušajo vdreti v vaš sistem, in bo uporabila sezname besed v kombinaciji s skupnimi seznami gesel, ko bodo poskušali uganiti gesla.


<cite>[Hackplaining](https://www.hacksplaining.com/glossary/dictionary-attacks)</cite>
</blockquote> 
<br>
<div class="row flex-nowrap">
  <div class="col">

  ```{r out.width='80%'}
  sourceDir <- getSrcDirectory(function(dummy) {dummy})
  # All defaults
  include_graphics(paste0(normalizePath(sourceDir), "/static/img/type_difference_ascii_alpha_numeric.jpg"))
  ```
  </div>
  <div class="col">
  Kaj se zgodi, če napadalec ne bo mogel uganiti strukture in še vedno želi uporabiti napad s slovarji? Potem mora poiskati par proti veliko večjemu slovarju, ki vsebuje vsa mogoča gesla.
  

  Izračunajmo približno velikost slovarja. Za namen tega primera, najprej opazujemo le gesla, ki vsebujejo 8 znakov. Vsak znak je črka ([azA-Z]), številka ([0-9]) ali en od naslednjih posebnih
  znakov {,}, (,), [,], #,!,?, _, @, /, \$,%, ^, &, *, ~, |, +, - ali prosto
  mesto. Omogoča nam \(26 * 2 + 10 + 23 = 85 \) možnih znakov. 
  Če geslo vsebuje natanko 8 znakov, potem ima napadalec \(85 ^ 4 = 52200625 \) 
  elementov v slovarju in ne more razbiti gesla v manj kot desetletju. To nam daje priložnost, da ostanemo varni, tudi pri uporabi kratkih gesel, če jih pogosto spreminjate, tj. vsako leto.
  To bo napadalcu preprečilo napad! :)
  </div>
</div>
<br>

| Primer gesla  | Dolžina gesla  | Potreben čas za razbijanje gesla      |
|---|---|---|
| 'abcdefg'         | 7                | 0.29  milisekund |
| 'abcdefgh'        | 8                | 5 ur            |
|  'abcdefghi'      |  9               | 5 dni             |
| 'abcdefghij'      | 10               | 4 mesce           |
| 'abcdefghijk'     | 11               | 1 desetletje           |
| 'abcdefghijkl'    | 12               | 2 stoletji        |
<cite>[Better buys](https://www.betterbuys.com/estimating-password-cracking-times/)</cite>

### Izbira

Združevanje dveh ali več nepovezanih besed (najbolj 8) in spreminjanje nekaterih črk v
posebne znake ali številke je dober način. Še en dober način je imeti osebno 
zasnovan algoritem za ustvarjanje nejasnih
gesel.
Na voljo imaš 2 različni implementaciji sestavljanja gesel. Prva (levo) ti poleg
standardnih načinov generiranja gesel omogoča tudi ročni vnos besed, ki se bodo
naključno uporabila v procesu sestavljanja priporočenih gesel. Druga
implementacija (desno) pa je bolj specifična na izbiri znakov, dolžine ali
entropije za generiranje gesel. Preizkusi obe. Za testiranje lahko generirana
gesla skopiraš in preveriš v aplikaciji preverjanja moči gesel.
<div class="row flex-nowrap">
  <div class="col">
  **Password  generator 1**
  <iframe src="./P1/passphrase_generator_BREZ_entropije/passphrase_generator.html" id='passphrase_generator_Be_iframe'></iframe>
  </div>
  <div class="col">
  **Password  generator 2**
  <iframe src="./P1/passphrase_generator_z_entropijo/passphrase_generator.html" id='passphrase_generator_Ze_iframe'></iframe>
  </div>
</div>

Za večjo varnost torej uporabite daljša gesla. Vendar bodite previdni, dandanašnja varnost in varnost čez deset let ne bosta enaki. Na primer: če vzamemo geslo in izračunamo, da napadalec potrebuje cca 4 leta, da ga razbije s slovarskim napadom, potem čez 10 let lahko pričakujemo, da bo potreboval le 4 mesece .Pri vsem tem smo opazovali napadalčevo moč
glede na strukturo gesla.<br>

**Primer** <br>

Vreme za pridobitev naslednjega Gesla: *security1*

|   |  Years |  Months |  Weeks | Days  |  Hours |  Minutes |  Seconds |  Miliseconds |   |  
|:-:|---|---|---|---|---|---|---|---|---|
| 2000  | 3  | 10  | 1  | 5  | 14  | 54  | 19  | 21  | 1.37 |  
|  2001 | 2  | 9  |  0 |  5 | 11  | 28  | 35  | 4  | 10 |   
|  2002 |  2 | 1  |  3 |  0 |  22 |  20 | 41  |  9 |  4 |   
| 2003  |  1 |  9 |  1 |  6 |  18 |  22 | 50  |  32 |  6 |   
|  2004 |  1 |  0 |  1 |  2 |  1  |  57 | 49  |  50 |  6 |   
| 2005  |  0 |  7 |  1 |  5 |  6  |  6  | 52  |  12 |  5 |   
|  2006 |  0 |  6 |  2 |  3 |  13 |  26 | 6   |  32 |  4 |   
|  2007 |  0 |  4 |  0 |  5 |  15 |  26 | 39  |  85 |  8 |   
| 2008  |  0 |  4 |  3 |  4 |  1  |  26 |  2  |  13 |  5 |   
| 2009  |  0 |  4 |  3 |  2 |  8  |  9  |  8  |  56 |  5 |   
| 2010  |  0 |  4 |  1 |  4 |  19 |  7  |  57 |  74 |  6 |   
| 2011  |  0 |  4 |  1 |  2 |  12 |  25 |  23 |  33 |  6 |   
| 2012  |  0 |  4 |  0 |  0 |  21 |  11 |  42 |  77 |  0.9 |   
| 2013  |  0 |  3 |  3 |  5 |  23 |  36 |  29 |  81 |  10 |   
| 2014  |  0 |  3 |  3 |  1 |  0  |  39 |  45 |  23 |  9 |   
| 2015  |  0 |  3 |  1 |  6 |  14 |  44 |  19 |  66 |  5 | 
| 2016  |  0 |  2 |  4 |  1 |  10 |  12 |  11 |  31 |  3 |   

Kaj se zgodi, če je napadalec ne ve strukture gesla? Ali se potem res poveča varnost? <br>
Samo v primeru, da struktura ni ravno pogosta. Napadalci bodo vedno najprej preizkusili 
vse najmočnejše napade, proti večini skupne strukture gesel. Čeprav razplet strukture 
ne pomaga veliko. Le če je res edinstvena. Iskreno povedano, to je zelo redko 
in ne predlagamo da se zanašate na varnost le-tega.  
Kakorkoli, daljše in bolj
naključno je geslo, močnejše je. Ker si nismo vsi sposobni zapomniti "%
S'HJS6793jio777. & @ # $$ (???)% (%" kot enostavno kot
"Herego, wehereareyou!", mnogi predlagajo da uporabljate gesla, negujte,
pišite in shranjujte varno. Za začetek uporabite naslednje orodje besedno
zvezo, ki jo lahko nato spremenite bolj varno s saniranjem, ali nasljednim
postopkom:

* Izberi zaporedje besed, stavka ali citata, i.e.
  "To je dober citat!"
* Prenesite ga v nekaj podobnega: "T_j%%agutQot", vendar ga ne črkujte.

in to je to!
Samo ne užali nikogar s svojimi gesli! :) <br>
Zdaj, ko poznate postopek, poskusi sanirato eno gesli, in potem ocenite
splošnu moč končnega gesla.

<blockquote class='quote'>
Zahteva uporabnikom, da uporabijo “črke in številke”, bo pogosto vodila do
enostavnih zamenjav, kot so “E” → “3” in “I” → “1”, to so zamenjave, ki jih
napadalci dobro poznajo. Podobno vnašanje gesla z eno vrstico tipkovnice je
navaden trik, ki ga napadalci poznajo.
</blockquote>

**Ocenjevalnik moči**<br>
“Zxcvbn” je ocenjevalnik moči gesla, ki ga navdihujejo krekerji. S primerjanjem
vzorcev in konzervativno oceno prepozna in oceni 30 000 popularnih gesel, imen
in priimkov po podatkih iz popisa v ZDA, priljubljenih angleških besed iz
Wikipedije in ameriške televizije in filmov, ter drugih običajnih vzorcev, kot
so datumi, ponavljanja (aaa), zaporedja (abcd),… Uporabniku hitro in enostavno
poda informacije o vpisanem geslu.<br>
Podano imaš tabelo, v kateri so izračunane moči gesel po algoritmu knjižnjice
“Zxcvbn”. Oglej si gesla v tabeli in premisli, kateri bi bil najboljši in zakaj.
Poleg grafičnega prikaza moči imaš tudi dodatne informacije o izbranem geslu, s
katerimi lažje razumeš kriterij po čemer se določa moč gesla.

<!-- width=1000px height=300px -->
<iframe src="./P1/password_test/password_test.html" id ='password_test_iframe'></iframe>
<br>

Tu imaš možnost preverjanja poljubnih gesel. Brez skrbi, spletna stran ne beleži
tvojega vnosa :)

<!-- width=1000px height=300px  -->

<iframe src="./P1/password_test_insert_style/password_test_insert_style.html" id='password_test_insert_style_iframe'></iframe>

Uporabite orodje kot orientacijo. Opredelitev širine gesla je pristranska in se
spreminja vsako leto. Vsakič, ko se odkrije nov močan napad znatno povečane moči
procesorja, je treba definicijo pregledati in jo ustrezno prilagoditi.<br>

Če ne uporabljate sanitazije, bodite dodatno previdni, da citatov ne uporabljate
kot gesla. Namreč, isti citati se pojavljajo na internetu pogosteje kot druga
gesla. Tako jih je enostavno zbirati in uporabljati v napadu na slovar. Na
žalost je leta 2012 ta grožnja postala resničnost. Vsa gesla, povezana s
citatom, so bila razbita. LinkedIn je na splošno poročal o 6,5 milijona
ogroženih uporabniških računih.<br>


```{r out.width='50%', out.extra='style=center'}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/password_tooth.png"))
```

<div class='clear'></div>

### Dejavniki varnosti sistema gesel

Varnost sistema, zaščitenega z geslom, je odvisna od večih
dejavnikov. Celoten sistem mora biti zasnovan za zanesljivo varnost,
z zaščito pred računalniškimi virusi , napadi “človeka v
sredini” in podobno. Vprašanja v zvezi s fizično varnostjo so prav
tako zaskrbljujoča, od vohunjenja do bolj sofisticiranih fizičnih
groženj, kot so video kamere in snifferji s tipkovnico. Gesla je treba
izbrati tako, da jih napadalec težko ugiba in težko odkrije (uporaba
sistemov samodejnih napadov).

Danes je običajna praksa, da računalniški sistemi skrijejo gesla,
ko so vnesena. Namen tega ukrepa je preprečiti, da bi drugi opazovalci
prebrali geslo; vendar pa nekateri trdijo, da lahko ta praksa privede
do napak in stresa ter spodbudi uporabnike k izbiri šibkih gesel. Poleg
tega morajo imeti uporabniki možnost, da prikažejo ali skrijejo gesla,
ko jih vnašajo. Nekatera specifična vprašanja upravljanja z gesli,
ki jih je treba upoštevati pri razmišljanju in izbiri gesla so:.

1) Stopnja, pri kateri lahko napadalec poskusi uganiti gesla.
2) Hitrost, s katero lahko napadalec predloži uganjena gesla sistemu,
je ključni dejavnik pri določanju varnosti sistema. Nekateri sistemi
nalagajo časovni presledek nekaj sekund po majhnem številu (npr. treh)
neuspelih poskusih vnosa gesla. V odsotnosti drugih ranljivosti so
lahko takšni sistemi učinkovito zavarovani s sorazmerno enostavnimi
gesli. Gesla, ki se uporabljajo za ustvarjanje kriptografskih ključev
(npr. za šifriranje diska ali za zaščito brezžičnega omrežja), so
lahko prav tako izpostavljena visokim stopnjam ugibanja. Seznami skupnih
gesel so dostopni in napadi so zelo učinkoviti. Varnost je v takih
situacijah odvisna od uporabe gesel ali gesel z zadostno zahtevnostjo,
zaradi česar je napad nemočen. Nekateri sistemi, kot sta npr. PGP
in Wi-Fi WPA , uporabljajo geslo, ki zahteva intenzivno računanje,
da upočasni takšne napade.


<!-- width=500px height=500px -->



Shranjevanje gesel je prav tako pomembna lastnost moči gesla. Dobro geslo, 
shranjeno v navadnem tekstu je praktično neuporabno. Zato se gesla kodira 
z zgoščevalnimi funkcijami (podrobneje opisane v 2. poglavju). Eno izmed 
zgoščevalnih kodiranj je MD5.

#### MD5

MD5 (Message-Digest algorithm 5) je znan kot pogosto uporabljena kodirna 
funkcija s 128-bitnim izhodom. Po internetnem standardu (RFC 1321) je bil
 MD5 priznan in uporabljen v velikem številu aplikacij za izboljšanje varnosti. 
 Pogosto se uporablja tudi za preverjanje datotek.

MD5 si je zamislil Ronald Rivest leta 1991 z namenom, da bi zamenjal zgodnejšo
 funkcijo MD4. Leta 1996 so našli napako v zasnovi algoritma MD5. Kljub temu,
  da to ni bila velika napaka, so strokovnjaki za kodiranje začeli priporočati 
  uporabo drugih varnostnih algoritmov kot na primer SHA-1. Leta 2004 so 
  odkrili dodatne pomanjkljivosti v algoritmu, nadaljnje odkrivanje napak 
  pa je sledilo do leta 2007. Urad US-CERT je izjavil, da je »MD5 algoritem 
  zlomljen in neprimeren za nadaljnjo uporabo«.

Za prikaz delovanja tega kodiranja vstavi poljubno geslo, dolgo največ 5 znakov 
za hitrejšo rešitev. Izpisal se bo geslu izračunan MD5 hash zapis, katerega 
bodo “delavci” (računalnik) skušali z “bruteforce” načinom vpisovanja gesel 
najti enak MD5 hash zapis. S številom delavcev lahko vplivaš na hitrost 
razbijanja gesla. Ta proces je računsko zahteven in bo tvoj računalnik zaposlil 
z veliko dela. Z bližnjico “Ctrl + Shift + Esc” lahko v Upravitelju opravil 
spremljaš uporabo procesorja. Kljub veliki zaposlitvi lahko vidiš, da je čas 
razbijanja zelo majhen, zato torej je kodiranje v MD5 algoritmu zelo neučinkovito.

<iframe src="./P1/md5-cracker/index.html" id='md5_cracker_iframe'></iframe>

### Omejitve števila ugibanj gesla

Alternativa omejevanju hitrosti, s katero lahko napadalec ugiba o geslu,
je omejiti skupno število ugibanj, ki jih je mogoče narediti. Geslo
je mogoče onemogočiti, kar zahteva ponastavitev, po majhnem številu
zaporednih slabih ugibanj (recimo 5); in uporabnik bo morda moral
spremeniti geslo po večjem kumulativnem številu slabih ugibanj
(npr. 30).S tem prepreči, da bi napadalec naredil poljubno veliko slabih
ugibanj. Napadalci lahko nasprotno uporabijo znanje o tem (ublažitev
izvajati DDoS) proti uporabniku z namernim zaklepanjem uporabnika
iz lastne naprave; to zavrnitev storitve lahko odpre napadalcu druge
možnosti za manipulacijo situacije v njihovo korist preko socialnega
inženiringa.

Viri: Password - [Wikipedia](https://en.wikipedia.org/wiki/Password)


## PIN

Posebna vrsta gesel, ki je vezana na posameznika.
<!-- Do not start with the same word as title -->
PIN (Personal Identification Number) ~ (osebna identifikacijska številka)
- je numerično ali alfanumerično geslo, ki se uporablja v postopku
avtentikacije uporabnika, ki dostopa do sistema.

Osebna identifikacijska številka je bila ključna pri izmenjavi zasebnih
podatkov med različnimi centri za obdelavo podatkov v računalniških
omrežjih za finančne institucije, vlade in podjetja. Bolj splošno
znana uporaba PIN številke je predvsem pri preverjanju pristnosti
imetnika bančne kartice.

### Zgodovina

PIN številka se je začela uporabljati ob prihodu bankomatov leta
1967, ko so banke iznašle učinkovitejši način dviga denarja svojih
strank. Prvi sistem bankomatov je bil sistem Barclays v Londonu leta
1967. Sprejemal je čeke s strojno berljivo enkripcijo, uporabnik pa je za
odobritev transakcije moral vpisati PIN številko. Podjetje Lloyds Bank je
leta 1972 izdalo prvo bančno kartico, ki je uporabljala PIN za varnost.

### Uporaba

V okviru finančne transakcije je ponavadi tako zasebna koda PIN, kot
tudi javni identifikator uporabnika potrebna za overjanje uporabnika
v sistemu. Po prejemu obeh sistem pošjle kodo PIN na podlagi ID-ja
uporabnika in ga primerja z vnešenim. Uporabniku je dovoljen dostop le,
če se vneseno število ujema s številko shranjeno v sistemu. Zato,
kljub imenu, PIN ne identificira uporabnika.

### Dolžina PIN

Mednarodni standard za upravljanje s PIN-om za finančne storitve,
ISO 9564-1, dovoljuje uporabo PIN-ov od štirih do dvanajstih številk,
vendar priporoča, da izdajatelj kartice zaradi razlogov uporabnosti ne
dodeli PIN-a, daljšega od šestih mest.
Izumitelj bankomata, John Shepherd-Barron, si je sprva zamislil
šestmestno številčno kodo, njegova žena pa si je lahko zapomnila le
štirimestno številko, ki je postala najpogosteje uporabljena dolžina,
čeprav banke v Švici in številnih drugih državah uporabljajo
šestmestno kodo PIN.


### Preverjanje PIN-a

Naravna koda PIN se ustvari s šifriranjem primarne številke računa
(PAN) s pomočjo šifrirnega ključa, izdelanega posebej za ta namen. Ta
tipka se včasih imenuje ključ generacije PIN (PGK). Ta koda PIN je
neposredno povezana s primarno številko računa. Za potrditev PIN-a
banka izdajatelju z zgornjo metodo regenerira PIN in to primerja z
vnešeno PIN kodo.
Naravni PIN-i se ne morejo izbrati, ker so izpeljani iz PAN-a. Če je
kartica ponovno izdana z novo PAN, mora biti ustvarjena nova koda PIN.
Naravni PIN-i omogočajo bankam, da ob izdaji PIN-a izdajajo opomnike PIN.

Naravni PIN + offset metoda 

Za omogočanje uporabniško izbranih kod PIN je možno shraniti
vrednost odmika PIN. Odmik se ugotovi z odštevanjem naravnega PIN-a od
uporabniško izbranega PIN-a po modulu 10. Na primer, če je naravni
PIN 1234 in uporabnik želi imeti PIN 2345, je odmik 1111. Odmik se
lahko shrani bodisi na podatkih o poti, bodisi v podatkovni bazi pri
izdajatelju kartice. Za potrditev kode PIN banka izdajatelj izračuna
naravno PIN kot v zgornji metodi, nato doda odmik in dobljeno vrednost
primerja z vnešeno PIN kodo.

### Metoda VISA

Metoda VISA ustvari vrednost preverjanja PIN (PVV). Podobno kot vrednost
odmika se lahko shrani na podatke o skladbi kartice ali v podatkovni
bazi pri izdajatelju kartice. To se imenuje referenčni PVV.

Metoda VISA popelje najbolj desno petnajst številk PAN, razen vrednosti
kontrolne vsote, indeksa ključa za preverjanje PIN (PVKI, izbranega
od enega do šestih) in zahtevane vrednosti PIN za 64-bitno številko,
PVKI izbere ključ za preverjanje (PVK) , 128 bitov) za šifriranje te
številke. Iz te šifrirane vrednosti se najde PVV.

Za potrditev kode PIN banka izdajatelj izračuna vrednost PVV iz vnesene
PIN in PAN in primerja to vrednost z referenčnim PVV. Če se referenčni
PVV in izračunani PVV ujemata, je bil vnesen pravilen PIN.
Za razliko od IBM-ove metode metoda VISA ne prinaša PIN-a. Vrednost
PVV se uporablja za potrditev PIN-a, ki je bil vnesen na terminalu,
prav tako je bil uporabljen za generiranje referenčnega PVV-ja. PIN,
ki se uporablja za ustvarjanje PVV, se lahko naključno generira ali
izbere uporabnik ali celo izvede z uporabo metode IBM.


### Varnost PIN-a

Štirimestna PIN gesla imajo razpon od 0000 do 9999, kar je skupno 10 000
različnih kombinacij. Navadno sistem dopušča spreminjanje PIN-a, a je
priporočeno, da se ne uporablja rojstnih datumov, številk registrskih
tablic, ponavljajočih števil (1111, 2222, 3333, ....), zaporedij (1234,
2345, …), števil, ki se začnejo z ničlami.

Številni sistemi za preverjanje PIN-a omogočajo tri poskuse, s čimer
daje tatu kartice domnevno 0.3% verjetnost, da bo uganil pravilen PIN,
preden je kartica blokirana.To velja le, če so vsi PIN-i enako verjetni
in napadalec nima na voljo dodatnih informacij, kar pa ni bilo v primeru
številnih algoritmov za generiranje in preverjanje PIN, ki so jih v
preteklosti uporabljale finančne institucije in proizvajalci bankomatov.

Opravljene so bile raziskave o običajno uporabljenih kodah PIN. Posledica
tega je, da brez previdnosti velik del uporabnikov meni, da je njihov PIN
ranljiv. "Oboroženi s samo štirimi možnostmi lahko hekerji razbijejo
20% vseh PIN-ov. Dovolite jim ne več kot petnajst številk in lahko
uporabijo račune več kot četrtine imetnikov kartic."

Težava lahko ugibljivih PIN-ov se poveča, ko so stranke prisiljene
uporabljati dodatne številke, ki povečajo možnost razbitja na
okoli 25%, pri petnajstih številkah celo na več kot 30% (brez
upoštevanja 7-mestnih števil telefonskih številk). Približno
polovica vseh 9-mestnih PIN-ov ima dobre možnosti razbitja že po 35%
uporabi zaporedja 123456789, za preostalih 64% pa je dobra možnost,
da uporabljajo svoj EMŠO.

### Viri:
Personal identification number 
- [Wikipedia](https://en.wikipedia.org/wiki/Personal_identification_number)



<script>
  iFrameResize({ log: true, warningTimeout: 10000}, '#password_test_iframe, #password_test_insert_style_iframe, #md5_cracker_iframe, #passphrase_generator_Be_iframe, #passphrase_generator_Ze_iframe')
</script>
