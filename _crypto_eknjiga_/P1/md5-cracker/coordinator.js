/* var numWorkers = 4 */ // NOTE: can't set this to be more than 8 without fixing the way numbers are carried
var workers = []
var startTime = undefined


function nastaviDelavce(numWorkers){
  for (var i = 0; i < numWorkers; i++) {
    // Create worker
      /* Naredi novega delavca */
  var nDivDelavec = document.createElement("DIV");
  var nLogDelavec = document.createElement("DIV");
  nDivDelavec.setAttribute("id","worker"+i);
  nDivDelavec.setAttribute("class","status");
  document.getElementById("okno").appendChild(nDivDelavec);

  nLogDelavec.setAttribute("id","worker"+i+"log");
  nLogDelavec.setAttribute("class","log");
  document.getElementById("okno").appendChild(nLogDelavec);
  }
}


function vnosButtonFunkcija(){
  
  var numWorkers = undefined;
  var vnosText = undefined;
  var vnosHash = undefined;
  



  if(document.getElementById("vnosButton").innerHTML == "Ponastavi"){
    
      location.reload();
      
  }else{
    startTime = +new Date
    numWorkers = parseInt(document.getElementById("vnosStDelavcev").innerHTML);
    nastaviDelavce(numWorkers); 
    
    if(document.getElementById("vnosString").value != ""){
      vnosText = document.getElementById("vnosString").value;
      vnosHash = md5(vnosText);
    }
    else{
      
      
    }

  
  
    
    document.getElementById("vnosButton").innerHTML = "Ponastavi";
  }
  
  

  
  
  document.getElementById("izpisHexHash").innerHTML = 'Vaše geslo: '+vnosText.bold()+'</br>MD5 hash zapis: '+vnosHash.bold();
  
  document.getElementById("vnosString").innerHTML = "Vstavljeno geslo: ";
  
    
  
    
    for (var i = 0; i < numWorkers; i++) {
    var worker = new Worker('worker.js')
      workers.push(worker)
      
      // Message handler
      worker.addEventListener('message', function (e) {
        switch (e.data.cmd) {
          case "status":
            status(e.data.data, e.data.id)
            break
      
          case "log":
            log(e.data.data, e.data.id)
            break
      
          case "setRate":
          status(addCommasToInteger(e.data.data) + " gesel na sekundo", e.data.id)
            break
      
          case "foundPassword":
            log("NAŠEL GESLO: " + e.data.data)
      
            var totalTime = (+new Date - startTime) / 1000
            log("Skupen čas: " + totalTime + " sekund")
      
            workers.forEach(function(worker) {
              worker.terminate()
            })
            log("Ustavil vse delavce.")
            break
      
          default:
            log("Glavna stran ne razume navodil " + e.data.cmd)
            break
        }
      })
      
      // Error handler
      worker.addEventListener('error', function(e) {
        log(['NAPAKA: Linija ', e.lineno, ' v ', e.filename, ': ', e.message].join(''))
      })
      
      
      // Set worker settings
      worker.postMessage({ cmd: "setWorkerId", data: i })
      worker.postMessage({ cmd: "setMaxPassLength", data: 5 })
      worker.postMessage({ cmd: "setPassToCrack", data: vnosHash })
      
      // Start worker
      worker.postMessage({ cmd: "performCrack", data: {start: i, hop: numWorkers} })
    }
  


  /* status("Iščem geslo") */
  log("Testiram male, velike tiskane črke in številke.")
}



/* var vnosText = document.getElementById("vnosString"); */
var vnosButton = document.getElementById("vnosButton");

  if(vnosButton != null){
    /* Izbrisal sem dodatne argumente, ker berem samo iz enega vnosnega polja */
    vnosButton.setAttribute("onClick", 'vnosButtonFunkcija()');
  }








// Helper functions

function addCommasToInteger(x) {
x = parseInt(x) + ''
var rgx = /(\d+)(\d{3})/
while (rgx.test(x)) {
  x = x.replace(rgx, '$1' + ',' + '$2')
}
return x
}

function status(msg, workerId) {
var prefix = workerId != null
  ? "Delavec " + (workerId+1) + ": "
  : " "

var selector = workerId != null
  ? "#worker" + workerId
  : "#main"

document.querySelector(selector).textContent = prefix + msg
}

function log(msg, workerId) {
var prefix = workerId != null
  ? "Delavec " + workerId + " pravi: "
  : " "

var fragment = document.createDocumentFragment();
fragment.appendChild(document.createTextNode(prefix + msg));
fragment.appendChild(document.createElement('br'));

var selector = workerId != null
  ? "#worker" + workerId + "log"
  : "#mainlog"

document.querySelector(selector).appendChild(fragment)
}

