# Šifre 
<!--(BLAŽ)
UVOD: TO-DO
-->



<!-- =============================================================== -->
## Klasične šifre
<!--(AJ) -->
<!-- in zdravstvena kartica -->

Pomena informacije se v naši družbi dobro zavedamo -
na koncu koncev se veliko posameznikov in podjetij preživlja zgolj z
zbiranjem, urejanjem in posredovanjem informacij. 
<!-- --> 
Včasih želimo tudi omejiti dostop do informacij in ena izmed 
možnosti je njihovo **šifriranje**.
<!-- --> 
Takemu postopku pravimo **šifra** (angl. cipher), izhaja pa iz 
hebrejske besede *saphar*, ki pomeni *šteti/označiti*.
<!-- Cipher izhaja iz hebrejske besede *saphar*, ki pomeni 
*šteti/označiti*. SASA ugotovi kaj res pomeni! --> 
S problemom, kako si osebi (poimenujmo ju Anita in Bojan) 
izmenjata sporočilo, ne da bi ga opazovalec (Oskar) razvozlal, 
se (med drugim) ukvarja **kriptologija**.
Ta izraz je sestavljen iz grških besed *kryptos*, 
ki pomeni skrivnost, in *logos*, ki pomeni beseda.
<!-- --> 
Spoznali bomo dva pristopa za reševanje tega problema. 
<!-- --> 
V tem poglavju predstavimo *zasebne* oziroma *simetrične* šifre,
<!-- --> 
kasneje pa bomo predstavili tudi *šifre z javnimi ključi* oziroma 
*asimetrične* šifre.

Zelo enostaven način za šifriranje besedila so poznali
že v rimskih časih, šifrirali so tako, da so zamaknili abecedo 
za nekaj znakov. Število, ki nam pove, za koliko znakov smo zamaknili 
abecedo, označimo s $K$ in predstavlja *ključ* za šifriranje 
in odšifriranje sporočila. 
<!-- --> 
Če si izberemo $K=3$, namesto črke A v sporočilu pišemo Č, 
namesto B črko D in tako naprej do Ž, namesto katerega pišemo črko C. 
<!-- --> 
To šifriranje je pogosto uporabljal tudi Julij Cezar in ni preveč varno, 
saj bi lahko vsakdo v kratkem času kar "na roko" preveril vseh 24 možnosti. 
<!-- SASA: v resnici je vse odvisno od dolžine teksta.
Če je kratek (eno samo črko), potem ni prav nobenega problema. --> 

<!-- slike/cezar_sifra.eps HBČT ŽNČBČO RČŠČG -->

**Slika 1**
(a) Cezarjeva šifra zašifrira njegovo ime v Ehbčt.


Najbolj razširjena šifra po drugi svetovni voj\-ni je bil
DES (Data Encription Standard) in je uporabljal 56-bitne ključe, 
kar pomeni $2^{56}$ možnosti za izbiro ključa. 
<iframe src="./P4/DES.html" frameborder="0" scrolling="no" id="DES_iframe"></iframe>

<!-- --> 
Zaradi vse hitrejših računalnikov so ga leta 2000 nadomestili z 
AES (Advanced Encription Standard), ki lahko uporablja ključe 
dolžin 128, 196 in 256 bitov.

**DES** in **AES** sta bločni šifri, ker delujeta na blokih (tj.  skupini 
zaporednih znakov). Znake mešata v več krogih z enostavnimi operacijmi, 
katerih vrstnega reda ne moremo spreminjati ne da bi ostala 
šifra nespremenjena.  
Take operacije so modularna aritmetika, ekskluzivni ali, 
ciklični zamik, pogled v tabele itd. 
<!-- --> 
Podroben opis teh šifer pa žal presega okvire uvoda.
<!-- --> 
Za računske moči napadalcev si oglejte npr.
http://crypto-systems.com/,
http://keylength.html in
http://valjhun.fmf.uni-lj.si/~ajurisic/sc/m3.pdf.

Preden si lahko Anita in Bojan začneta pošiljati sporočila, se
morata dogovoriti, na kakšen način bosta šifrirala sporočila,
in določiti *skrivni ključ* $K$. 
<!-- --> 
Bistveno je, da napadalec Oskar ne izve za skrivni ključ, saj bi 
lahko sicer brez težav prebiral (in ponarejal) sporočila.
<!-- --> 
Načina šifriranja pa običajno ne skrivamo, saj izkušnje kažejo,
da ga napadalec slej ko prej odkrije.

<blockquote class='quote'>
**Kerckhoffov princip**
Nasprotnik'' pozna šifro oziroma algoritme,
\phantom{a} ki jih uporabljamo, ne pa tudi ključev,
ki nam zagotavljajo varnost.
</blockquote>

Varnost šifriranja je tako odvisna od zasebnega ključa, za
katerega se dogovorita Anita in Bojan. 
<!-- --> 
Za tako šifro pravimo, da je *zasebna* oziroma *simetrična*,
saj imata Anita in Bojan enak zasebni ključ (glej sliko 2).


**Slika 2**

<!-- slike/sim_sifra.eps -->

(a) Simetrična šifra uporablja isti ključ za šifriranje in odšifriranje.

Največja nevšečnost takšnega pristopa je torej, da se morata
Anita in Bojan pred dopisovanjem sestati. To je za zaljubljen par
vsekakor smiselno, v primeru internetnega trgovanja pa nikakor ne.
<!-- --> 
Omenimo še eno pomanjkljivost, ki jo ima takšen način: če povečujemo
število posameznikov, ki želijo varno komunicirati v omrežju, narašča
tudi število kjučev za vsakega uporabnika. Na sliki 3 lahko vidimo
primer omrežja z devetnajstimi uporabniki.

**Slika 3**

<!-- slike/02-diagonale.eps -->

(a) Če v kriptosistemu, ki uporablja simetrično šifro,
predstavimo uporabnike s točkami, je ključ za vsak par predstavljen z
ustrezno povezavo. Že pri devetnajstih uporabnikih postane
prostor okrog vsakega uporabnika precej zapolnjen. 

Za občutek kakšen problem predstavlja prostor za shranjevanje 
ključev, si zamislimo, da jih uporabniki shranjujejo na pametnih 
karticah (mednje štejemo npr. SIM kartice v mobilnih telefonih, 
zdravstvene kartice $\ldots$). 
<!-- --> 
Če imamo na razpolago npr. 8 KB pomnilnika in uporabljamo 128-bitne 
ključe, lahko na kartico shranimo le 512 ključev. To je odločno 
premalo že za varno komunikacijo uslužbencev v velikem trgovskem
podjetju. Tudi če bi imeli na razpolago večji pomnilnik, pa
za potrebe bank, pošt ali zdravstva nikakor ne bi mogli 
v njem hraniti vseh ključev prebivalcev naše države.
<!-- --> 
V praksi problem pomanjkanja prostora rešimo tako, 
da uporabnike razdelimo v skupine (glej sliko 4).

**Slika 4** 

<!-- slike/simetricen1.eps -->

(a) Delitev na skupine pri naši zdravstveni kartici. 
S $K$ so označeni posamezni ključi, glavni ključ je označen z $M$. 
Vsak uporabnik ima tudi enolično serijsko številko ${\tt SN}_i$.

Vsak uporabnik ima svoj ključ in ključe vsake skupine. 
Vsaka skupina ima ključ za to skupino in t.i. ``glavni'' ključ 
(angl.  master key), s katerim lahko iz serijske številke uporabnika
izračuna uporabnikov ključ. S pomočjo teh ključev se uporabnik
in skupina medsebojno overita (tj. preverita pristnost) na način, 
prikazan na sliki 5.

**Slika 5**

<!-- slike/izziv-odg.eps -->

(a) Pametna kartica predstavlja uporabnika, terminal pa skupino. 
<!-- --> 
Z $e_K$ označimo šifrirno funkcijo s ključem $K$. Le-ta pretvori 
s ključem $K$ vsebino v oklepaju v šifrirano besedilo.
<!-- --> 
V zgornjem delu kartica overi terminal s skupinskim ključem
$K_S$ skupine $S$, v spodnjem delu pa terminal overi pametno kartico 
z glavnim ključem $M$ in serijske številke kartice (${\tt SN}$).
Kartica vsebuje samo $K_U$ in $K_S$ ne pa tudi $M$, medtem ko 
terminal ne pozna $K_U$, ampak ga izračuna z $M$. 

Vendar pa imajo vsi kriptosistemi, ki temeljijo na takšni shemi, 
kar nekaj pomanjkljivosti. Ena izmed večjih je ta, da varnost
celotnega sistema temelji na varnosti ene same kartice. 
<!-- --> 
Če namreč uspemo odpreti uporabniško kartico, se dokopljemo do 
vseh skupinskih ključev. To nam omogoča, da se predstavimo ostalim 
uporabnikom kot katerakoli skupina, ne da bi uporabnik posumil, 
da ne komunicira s pravim članom skupine. Če pa se
dokopljemo do glavnega ključa, lahko enostavno ponarejamo
uporabniške kartice, saj si serijsko številko izmislimo,
ustrezen uporabnikov ključ pa nato z glavnim ključem preprosto izračunamo.


<blockquote class='quote'>
Varnost celotnega kriptosistema ne sme biti odvisna od varnosti 
ene same kartice.
</blockquote>


Naslednja pomanjkljivost je ta, da so vse osebe znotraj ene
skupine enakovredne, tj. če uporabnik komunicira npr. z 
lekarno, lahko katerakoli oseba iz skupine lekarn prisluškuje pogovoru.
<!-- -->
Pa ne samo to, osebe znotraj ene skupine so tudi neločljive, kar 
pomeni, da se lahko ena oseba iz iste skupine izgovarja, da nečesa 
ni naredila, čeprav v resnici temu ni tako. 
<!-- -->
Poleg tega je shema povsem neuporabna, če nimamo naravne hierarhične 
zgradbe, ki nam omogoča delitev na skupine.

Druga rešitev problema s prostorom pa je prikazana na sliki 6,
njen najbolj poznan predstavnik pa je Kerberos.

**Slika 6**

<!-- slike/5-centrsim.eps -->

(a) Centralna rešitev s simetričnim sistemom. 


V tem primeru imamo center, s katerim vsak uporabnik deli
svoj skrivni ključ. Ko želi Anita komunicirati z Bojanom, pošlje
centru zašifriran zahtevek. Center generira 
<!-- sejni -->
ključ in zašifriranega pošlje Aniti in Bojanu. Bojan nato obvesti 
Anito, da je prejel ključ in komunikacija se lahko začne. 
Celoten potek je prikazan na sliki 7.

**Slika 7** 

<!-- slike/06-simprot2.eps -->

(a) V prvem koraku Anita pošlje centru zašifriran 
zahtevek za pogovor z Bojanom. Center nato v drugem koraku generira
<!-- sejni -->
ključ in ga pošlje obema. Bojanu tudi sporoči, kdo želi komunicirati 
z njim. Bojan v četrtem koraku pošlje sporočilo Aniti, da potrdi
sprejetje ključa. Nato se začne komunikacija. 

Očitna pomanjkljivost te sheme je, da moramo vsakič, ko želimo
komunicirati, najprej vzpostaviti povezavo s centrom. Pogosto to
ni zaželeno ali pa sploh ni možno. Če uporabniški ključi v
centru niso primerno zaščiteni, je varnost celotne sheme
ogrožena.

<blockquote class='quote'>
Kartic ni potrebno vedno odpirati, da bi prišli do ključev, ki 
jih hranijo, pač pa je dovolj, da znamo natančno izmeriti porabo 
energije v času računanja ter uporabiti nekaj statistike.
</blockquote>

Zaradi vseh teh pomanjkljivosti se sheme, ki temeljijo na simetričnem
pristopu, uporabljajo bodisi v zaprtih okoljih bodisi tam,
kjer ni velike potrebe po varnosti. 
<!-- -->
Takšne sheme se na primer
uporabljajo za kartice, ki hranijo administrativne podatke, tako
da ni potrebno ročno vnašati podatkov v formularje. Naša
zdravstvena kartica je tipičen primer take kartice. Za hranjenje
občutljivih podatkov pa so takšne sheme primerne le v 
okoljih, kjer so vsi uporabniki ves čas povezani v mrežo.
V primeru naše zdravstvene kartice pa temu ni tako, saj nam sicer 
ne bi bilo potrebno skoraj vsakič, ko jo želimo uporabiti, iti 
najprej do terminala, da ji podaljšamo veljavnost.

<!-- =============================================================== -->
## Cezarjeva šifra
<!--(Blaž)-->

Kadar želimo podatke zaščititi, da jih ne morejo prebrati tiste osebe,
katerim sporočilo ni namenjeno, jih moramo na nek način zaščititi.
Eden od postopkov za dosego tega cilja je šifriranje (angl. cipher),
izhaja pa iz hebrejske besede saphar, ki pomeni šteti/označiti.
S problemom kako zaščititi ta sporočila, da jih ljudje, katerim
ta niso namenjena, ne morejo razvozlati, se ukvarja kriptografija.
Ta izraz je sestavljen iz grških besed kryptos, ki pomeni skrivnost,
in logos, ki pomeni beseda.

Cezarjeva šifra je ena od nastarejših in najpogosteje uporabljenih
oblik šifriranja. Poznali so jo že v rimskih časih in ga je med
drugimi uporabljal tudi Julij Cezar po komer se ta šifra imenuje.
Šifriranje poteka tako, da vsako črko abecede zamaknemo za določeno
število znakov. Šifro je zato možno razbiti precej preprosto kajti
na voljo je le 24 kombinacij, ki pa jih lahko razbijemo kar "na roke".

### Postopek

Postopek lahko ponazorimo tako, da vsako črko označimo po vrsti,
s številko od 1 do 25. Nato izberemo za koliko mest bomo zamaknili
naš čistopis. Naše besedilo nato pretvorimo v številke ki pripadajo
posamični črki in tem potem prištejemo zamik. Vsem številkam ki so
višje od 25 odštejemo 25 in nato v tabeli pogledamo katera številka
je katera črka, s čimer pretvorimo besedilo nazaj v črke.


*alternativa:*

Namesto tabele lahko uporabimo tudi krog črk.
<iframe src="./P4/cezar.html" frameborder="0" scrolling="no" id="the_iframe" width="100%" height='500'></iframe>

Pri tem najprej ugotovimo katero črko predstavlja naš zamik. Ta je
črka ki je n-ta (zamik + 1) po vrsti vseh črk. Nato A zunanjega kroga
z našo n-to črko notranjega kroga. Nato črke eno po eno začnemo
prepisovati iz čistopisa v tajnopis, vendar namesto da zapišemo črko
samo, jo poiščemo na zunanjem delu kroga in črko ki se na tem nahaja
na notranjem krogu zapišemo v tajnopis.


## Vigenerjeva šifra
<!--(Blaž)-->

Vigenerjeva šifra je različica Cezarjeve šifre pri kateri za vsako
številko uporabimo drugo cezarjevo šifro, ki jo določa geslo. Šifra
je bila prvič zabeležena leta 1553 v knjigi Giovana Battista Bellasa
ampak je bila v 19. stoletju napačno pripisana Blaiseju de Vigenèrju
od koder je tudi dobila svoje ime. Šifra je bila izredno preprosta
za implementacijo in pojasnilo, vendar je učenjaki niso uspeli razbiti
več kot 300 let, ko je Friedrich Kasiski uspel razviti splošno metodo
za razbijanje. Zaradi tega razloga se jo je v tem času prijelo ime le
chiffre indéchiffrable (francosko za šifra, ki se je ne da razbiti).


### Postopek

Ponovno bomo uporabili tabelo, ki smo jo uporabili pri Cezarjevi
šifri. Prvi korak je, da pretvorimo posamične črke gesla v številke
in nato od vsake odštejemo 1. Nato ta postopek naredimo še z našim
čistopisom. Prvo številko čistopisa seštejemo s prvo številko iz
gesla, drugo čistopisa z drugo gesla in tako naprej. Ko nam zmanjka
gesla začnemo geslo ponovno s prvo številko medtem ko čistopis
nadaljujemo eno črko za drugo. Ko pridemo do konca vsem številkam
večjim od 25 odštejemo 25 in nato vsako številko poiščemo v tabeli
ter jih po vrsti zabeležimo, kar nam predstavlja tajnopis.

*alternativa*

Za ta postopek šifriranja lahko uporabimo tudi krog črk, ki smo ga
uporabili pri Cezarjevi šifri, kar naredi ta postopek še lažji.
Na zunanjem krogu A poravnamo s prvo črko gesla. Nato na zunanjem
krogu poiščemo prvo črko čistopisa. Črka, ki se nahaja pod njo
predstavlja prvo črko tajnopisa. To naredimo za vsako črko. 
Če nam med tem zmanjka gesla preprosto pričnemo z njim od začetka.

<iframe src="./P4/cezar_a.html" frameborder="0" scrolling="no" id="cezar_a_iframe" width="100%" height='500'></iframe>

### Vigenerjeva šifra z krogom

<iframe src="./P4/vignare_step.html" frameborder="0" scrolling="no" id="vignare_step-iframe" width="100%" height='500'></iframe>

### Vigenerjeva šifra brez kroga

<iframe src="./P4/v_sif.html" frameborder="0" scrolling="no" id="v_sif_iframe" width="100%" height='500'></iframe>


### Napad na Vigenerjevo šifro

<iframe src="./P4/razbijanje_v.html" frameborder="0" scrolling="no" id="razbijanje_v_iframe" width="100%"></iframe>

## Afina šifra

### Postopek
<!--(Mia)-->

Postopek lahko ponazorimo tako, da vsako črko označimo po vrsti,
s številko od 1 do 25. Linearnom funkcijom, definiranom ključem, 
izberemo za 
koliko mest bomo zamaknili naš čistopis. Naše besedilo nato pretvorimo v številke ki pripadajo
posamični črki in tem potem prištejemo zamik. Vsem številkam ki so
višje od 25 odštejemo 25 in nato v tabeli pogledamo katera številka
je katera črka, s čimer pretvorimo besedilo nazaj v črke.

<!--(Klemen)-->
**Vhod** naj bodo štirije tekstovni vhodi oz. izhodi:

 * Vhodna abeceda (poljubni različni znaki (UTF-8)).
 * Kjuč za enkripcijo $(a,b)$ - dva vhoda.
 * Vhodni čistopis $P$.

**Šifriranje oz. dešifriranje s ključem $(a,b)$** 

 * Šifriramo vsako črko posebej.
 * $E_{(a,b)}(x) = a x + b \mod n$, kjer je $x$ indeks črke v abecedi in $n$ dolžina abecede.
 * Dešifriranje je v obratnem vrstnem redu šifriranja $D_{(a,b)}(x) = a^{-1} (x - b) \mod n$. 

**Izhod** naj bo tajnopis in tabela, ki definira preslikavo.

 * Tajnopis, kot tekstovni izhod.
 * Šifrirna tabela. 

<!--(Denis interakcija)-->
 <iframe src="./P4/affinaSifra/affina.html" id = 'affina-iframe'></iframe>



**Namigi in dodatne ideje**:

 * Za dešifriranje lahko uporabimo isto shemo. Potrebno je preračunati kjuč $(a, b) \mapsto (a^{-1}, -a^{-1}b)$ (lahko uporabiš REA za izračun obratnega elementa v $\mathbb{Z}_n$).
 * V primeru, da uporabnik uporabi napačen kjuč (šifriranje ni bijekcija), 
   ga interakcija opozori (ponudi mu možne izbire za $a$ - števila, ki so tuja z $n$),
   ampak  pripadajočo tabelo še vedno izpiši.
 * Za smiselno funkcioniranje imamo gumbe s pripadajočimi akcijami:

   + Gumb, ki samo izpise šifrirano oz. dešifrirano tabelo.
   + Gumb, ki izračuna, kjuč za dešifriranje.
   + Gumb, ki šifrira oz. dešifrira besedilo.

<!--(Denis interakcija)-->
<iframe src="./P4/affinaSifra/affina_desif_key_calc.html" id = 'affina_desif_key_calc-iframe'></iframe>

## Pretvornik med abecedami
<!--Prije bilo pod kode. Klemen.-->

Začnite pisati na enem od polj. S klikom na gumb za vnos,
pretvorba bo opravljena. V primeru težav nas kontaktirajte s spodnjim obrazcem.
<iframe src="./P3/pretvornik_med_abecedami/pretvornik.html" id = 'pretvornik-iframe'></iframe>

### Učenje novih adeced

Po opazovanju primera pretvorbe med slovenske gajice
in srbska cilirice, in slovenske gajice
in ruske cilirice, in slovenske gajice
in glagolice, poskusite, koliko ste se naučili.
<iframe src="./P3/ucenje_abeced_s_kartami/karte.html" id = 'karte-iframe'></iframe>

<script>
  iFrameResize({ log: true, warningTimeout: 10000}, '#razbijanje_v_iframe, #the_iframe, #DES_iframe,  #affina-iframe, #affina_desif_key_calc-iframe,  #pretvornik-iframe, #karte-iframe')
</script>
