function mainPT(){
  var strength = {
          0: "Zelo šibko geslo",
          1: "Šibko geslo",
          2: "Vredu geslo",
          3: "Dobro geslo",
          4: "Zelo dobro geslo"
      }
  
          var password = document.getElementById('password');
          var meter = document.getElementById('password-strength-meter');
          var text = document.getElementById('password-strength-text');
          var textGuess = document.getElementById('password-guess-text');
          var textTimeToCrack = document.getElementById('password-timeToCrack');
  
          password.addEventListener('input', function() {
          var val = password.value;
          var result = zxcvbn(val);
          // Update the password strength meter
          meter.value = result.score;
          
          // Update the text indicator
          if (val !== "") {
              text.innerHTML = "Moc gesla: " + strength[result.score]; 
              textGuess.innerHTML = "Stevilo poskusov: " + result.guesses;
              textTimeToCrack.innerHTML = "Casovna ocena razbijanja gesla: " + result.crack_times_display.online_throttling_100_per_hour;
          } else {
              text.innerHTML = "";
          }
          });
}

window.addEventListener("load", function(event) {
  document.getElementById('Password_test').innerHTML = '<div style="display:inline-block">'+
  '<label for="password">Enter password</label>'+
  '<input type="text" id="password" required>'+

  '<meter max="4" id="password-strength-meter"></meter>'+
  '<p id="password-strength-text"></p>'+
  '<p id="password-guess-text"></p>'+
  '<p id="password-timeToCrack"></p>'+
'</div>'
  mainPT();
});