ui<-fluidPage(
  wellPanel(
    fluidRow(
      column(6, textInput(inputId = "vhod", label="Vnesi 15 števk")),
      column(3, tags$p(" _ "), actionButton("gumb", "OK!", style="color: #fff; background-color: #337ab7; border-color: #2e6da4"))
    ),
    h4(textOutput(outputId = "izhod"))
  )
)

server<-function(input, output){
  vhodTxt <- reactive(input$vhod);
  
  izracunajKontrol<-eventReactive(input$gumb, {
    vhodTxt<-input$vhod
    if(str_length(vhodTxt)!=15)
      vrni<-"Nepravilna dolžina. Vnesi 15 cifer"
    else{
      sum<-0;
      for(i in 1:15){
        cifra<- as.numeric(substring(vhodTxt, i, i));
        if(i %% 2 != 0){
          if(cifra>4){
            sum<-sum+1
            sum<-sum + ((cifra*2) %% 10)
          }
          else sum <- sum + cifra*2;
        }else{
          sum<- sum+cifra;
        }
      }
      vrni<- sum %% 10;
      if(vrni==0)vrni<-0
      else vrni<-10-vrni
    }
  })
  
  output$izhod<-renderText({
    paste(vhodTxt(), izracunajKontrol(), sep="", collapse = NULL);
  })
  
}
shinyApp(ui<-ui, server<-server)