window.addEventListener('load', function() {
  console.log("wolcoome to this web site");
  
  var reset=document.getElementById("reset");
  var next_step=document.getElementById("next_step");
  var collapse=document.getElementById("collapse");
  var helpSum=document.getElementById("helpSum");
  next_step.style.display="none";
  var tabOut=document.getElementById("myTable");  
  var tab; //tabela, kjer so shrajene vse vrednosti
  var stVidnih=0; //koliko vrstic je trenutno vidnih v tabeli
  var stVidnihSum=0; //v zadnji vrstici dodajamo eno stevko po drugi. Ta parameter nam pove, koliko jih je vidnih
  var prvo=document.getElementById("prvo_st").value;
  var drugo=document.getElementById("drugo_st").value;
  var baza; //baza ki uporabljamo
  var lenPrvo; //dolzina prvega stevila
  var lenDrugo; //dolzina drugega stevila
  var kolikoKoll=0; //koliko zdruzevanj je bilo narejenih
  var stCollapse=0; //koliko vrstic moramo sesteti pri naslednjemu zzdruzevanju
  var veljavenVhod=false;
  
  collapse.addEventListener('click', function(event) {
    if(veljavenVhod==false)return;
	if(stVidnih<3 || stCollapse==0)return;
    kolikoKoll++;
    var vrstica = new Array(lenDrugo+lenPrvo+2);
    var overflow=0;
    for(var i=tab[0].length-1; i>=0; i--){
      var sum=-1;
      for(var j=1; j<stCollapse; j++){
        if(tab[j][i]==-1) continue;
        if(sum==-1)sum=0;
        sum += parseInt(tab[j][i], baza);
      }
      if(sum==-1){
        vrstica[i]=' ';
        continue;
      }
      sum=sum+overflow;
      overflow=Math.floor(sum/baza);
      sum=sum%baza;
      if(sum==-1)vrstica[i]=' ';
      else vrstica[i]=sum.toString(baza);
    }
    if(overflow)vrstica[0]=overflow.toString(baza);
    
    tabOut.deleteRow(1);
    tabOut.deleteRow(1);
    var row=tabOut.insertRow(1);
    for(i=0; i<tab[0].length; i++){
      var cell=row.insertCell(i);
      if(i==0)cell.innerHTML='+' +  vrstica[i];
      else cell.innerHTML=vrstica[i];
    }
    stCollapse++;
    stVidnih--; 
  });

  reset.addEventListener("click", function(event){
    helpSum.innerHTML="";
    tabOut.parentNode.insertBefore(no_overline, tabOut);
    tabOut.parentNode.insertBefore(underline, tabOut);
    kolikoKoll=0;
    next_step.style.display="block";
    for(var i=0; i<stVidnih; i++)tabOut.deleteRow(0);
    if(tabOut.rows[0])tabOut.deleteRow(0);
    prvo=document.getElementById("prvo_st").value;
    drugo=document.getElementById("drugo_st").value;
    lenPrvo=prvo.length;
    lenDrugo=drugo.length;
    stVidnihSum=lenDrugo+lenPrvo-1;
    
    var error=document.getElementById("error");
    baza=document.getElementById("modul").value;
    
    if(pravilenVhod(prvo, baza)==true && pravilenVhod(drugo, baza)==true){
      //izpisi mozne tiste
      error.innerHTML="";
    }else{
      error.innerHTML="Nepravilen vhod!";
      veljavenVhod=false;
      return;
    }
    veljavenVhod=true;
    
    var md=document.getElementById("izpisiModul");
    var sporocilo = "Izbral si bazo " + baza +". Rabiš lahko naslednja števila: " + stevila(baza);   
    md.innerHTML=sporocilo;

    prvo=parseInt(prvo, baza);
    drugo=parseInt(drugo, baza);
    
    stVidnih=0;
    stCollapse=0;
    
    tab=null;
    tab=new Array(lenDrugo+2);
    for(i=0; i<100; i++)tab[i]=new Array(lenDrugo+lenPrvo+2);
    
    for(i=0; i<tab.length; i++){
      for(var j=0; j<tab[0].length; j++)tab[i][j]=-1;
    }
    
    for(i=0; i<lenPrvo; i++)tab[0][i+1]=prvo.toString(baza).charAt(i);
    tab[0][lenPrvo+1]='x';
    for(i=0; i<drugo.toString(baza).length; i++)tab[0][i+lenPrvo+2]=drugo.toString(baza).charAt(i);
    
    for(i=0; i<lenDrugo; i++)
      reqMnozi(tab, i, baza, lenPrvo);
    setVisibleRow(tab, 0, tabOut, 0);
    sum(tab, baza, lenDrugo+lenPrvo-1, lenDrugo+1);
    stVidnih=1;
  });
  
  function printHelpSum(num){
    if(stVidnih==2){
      helpSum.innerHTML="";
      return;
    }
    var sporocilo="overflow = " + tab[0][num]+"</br>";
    sporocilo=sporocilo+" ("+ tab[0][num] +") ";
    var sum=tab[0][num];
    for(var i=1; i<stVidnih; i++){
      var data=tabOut.rows[i].cells[num].innerHTML;
      if(data.charAt(0)=='+')data=data.substr(1);
      if(data==' ')continue;
      sum=sum+parseInt(data, baza);
      if(sporocilo.length==0 && data!=' ')sporocilo=sporocilo + data;
      else if(data!=' ')sporocilo=sporocilo+' + '+data;
    }
    console.log(sum);
    sporocilo=sporocilo+" = " + sum+"</br>";
    sporocilo=sporocilo+sum+" % "+baza+" = " + (sum%baza)+"</br>";
    sporocilo=sporocilo+sum+" / "+baza+" = " + (Math.floor(sum/baza))+"</br>";
    if(sum!=0 || num!=0)helpSum.innerHTML=sporocilo;
  }
  
  function sum(tab, modul, first, visina){
    var add=0;
    for(var i=first; i>=0; i--){
      tab[0][i]=add;
      var colSum=add;
      for(var j=1; j<visina; j++){
        var data=parseInt(tab[j][i], modul);
        if(data!=-1)colSum+=data;
      }
      tab[visina][i]=(colSum%modul).toString(modul);
      add=Math.floor(colSum/modul);
    }
  }
  
  next_step.addEventListener("click", function(event){
    if(veljavenVhod==false)return;
  	var stSum=lenDrugo+1;
    if(stVidnih+kolikoKoll<=lenDrugo){ //nismo se izpisali vseh vrstic
      var nRow=stVidnih+kolikoKoll;
      setVisibleRow(tab, nRow, tabOut, stVidnih);
      stVidnih++;
      tabOut.rows[0].cells[lenPrvo+nRow].style.color="black";
      tabOut.rows[0].cells[lenPrvo+nRow+1].style.color="red";
      if(stVidnih==3 && kolikoKoll==0)stCollapse=3;
    }else if(stVidnihSum==lenDrugo+lenPrvo-1){ //smo izpisali vse vrstice razen zadnjo (kumulatino). Nastavimo zacetnne vrednositi te vrstice na ' '.
      tabOut.rows[0].cells[lenPrvo+stVidnih+kolikoKoll].style.color="black";
      tabOut.rows[stVidnih-1].style.color="black";
      var vrstica=tabOut.insertRow(stVidnih);
      for(var i=0; i<lenDrugo+lenPrvo; i++){
        var cell=vrstica.insertCell(i);
        cell.innerHTML=" ";
        cell.style.textAlign="right";
      }
      printHelpSum(stVidnihSum);
      tabOut.parentNode.insertBefore(overline, tabOut);
      tabOut.rows[stVidnih].cells[stVidnihSum].innerHTML=tab[stSum][stVidnihSum];
      setColor(stVidnihSum, "red");
      stVidnihSum--;
    }else if(stVidnihSum>=0){ //zaporedno dodajamo rezultat v celice zadnje vrstice
      printHelpSum(stVidnihSum);
      if(stVidnihSum==0 && tab[stSum][stVidnihSum]==0){
        setColor(stVidnihSum+1, "black");
      }else{
        tabOut.rows[stVidnih].cells[stVidnihSum].innerHTML=tab[stSum][stVidnihSum];
        setColor(stVidnihSum, "red");
        setColor(stVidnihSum+1, "black");
      }
      stVidnihSum--;
    }else{
      //end of job. Do nothing
      setColor(0, "black");
      helpSum.innerHTML="";
    }
  });
  
  function setColor(st, col){
    for(var i=1; i<tabOut.rows.length; i++){
      tabOut.rows[i].cells[st].style.color=col;
    }
  }
  
  function reqMnozi(tab, n, modul, lenNum1){
    var add=0;
    var coeff=parseInt(tab[0][lenNum1+n+2], modul);
    var ind=lenNum1+n;
    for(var i=lenNum1; i>0; i--, ind--){
      var c=coeff*parseInt(tab[0][i], modul)+add;
      var vr=c%modul;
      tab[n+1][ind]=vr.toString(modul);
      add=Math.floor(c/modul);
    }
    if(add)tab[n+1][ind]=add.toString(modul);
  }
    
  function setVisibleRow(tab, nRow, tabOut, nInTab){
    var row=tabOut.insertRow(nInTab);
    for(var i=0; i<tab[0].length; i++){
      var cell=row.insertCell(i);
      var data=tab[nRow][i];
      if(data!=-1)cell.innerHTML=data;
      else cell.innerHTML=" ";
      if(i==0 && nRow!=0)cell.innerHTML='+'+cell.innerHTML;
      else if(i==0)cell.innerHTML=' '+cell.innerHTML;
    }
    row.style.color="red";
    if(nRow==0)row.style.color="black";
    else tabOut.rows[stVidnih-1].style.color="black";
  }
  
  function stevila(n){
	  var rez="0";
	  for(var i=1; i<10 && i<n; i++){
		  rez=rez+", " + i;
	  }
	  if(n<10)return rez+".";
	  if(n>=11)rez=rez + ", a";
	  if(n>=12)rez=rez + ", b";
	  if(n>=13)rez=rez + ", c";
	  if(n>=14)rez=rez + ", d";
	  if(n>=15)rez=rez + ", e";
	  if(n>=16)rez=rez + ", f";
	  return rez+".";
  }
  
  function pravilenVhod(st, b){
    for(var i=0; i<st.length; i++){
      if(isNaN(parseInt(st.charAt(i), baza)) && parseInt(st.charAt(i), baza)!=0)return false;
    }
    return true;
  }
  
  var no_overline = document.createElement('style');
  no_overline.innerHTML='table tr:last-child td {border-top: 0; }';  
  
  var overline = document.createElement('style');
  overline.innerHTML='table tr:last-child td {border-top: 1px solid black; }';
  
  var underline=document.createElement('style');
  underline.innerHTML='table tr:first-child td {border-top: 0; }';
});