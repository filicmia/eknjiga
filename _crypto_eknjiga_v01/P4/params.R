#
# Nalaganje knjižnic
#
r = getOption("repos")
r["CRAN"] = "http://cran.us.r-project.org"
options(repos = r)

libs = c("knitr")
for (lib in libs) {
  if (!is.element(lib, .packages(all.available = TRUE)) ) { install.packages(lib) }
  suppressPackageStartupMessages(suppressWarnings({ library(lib, character.only = TRUE) }))
}
rm(list = ls())


#
# Izbriši RDS datoteko
#
if (file.exists("crypto.rds")) unlink("crypto.rds")


Sys.setlocale("LC_ALL", "sl_SI.UTF-8")


#
# Privzete nastavitve knitr generiranja vsebine
#
opts_chunk$set(echo = FALSE)
opts_chunk$set(fig.align = "center")


# Prikaži oz. skrij rešitve naloge
resitve <- TRUE