# 4. Racunala (nove dobe)

## Pregled
![](img/pregled.jpg)

## Literatura

1. [Modularna aritmetika](https://en.wikipedia.org/wiki/Modular_arithmetic) 
2. [Elipticne krivulje](https://en.wikipedia.org/wiki/Elliptic_curve)
3. [Grupe](https://en.wikipedia.org/wiki/Group_(mathematics))
4. [Koncni obsegi](https://en.wikipedia.org/wiki/Finite_field)
5. [Presek - Računala nove dobe 1](http://lkrv.fri.uni-lj.si/popularizacija/presek/racunala.nove.dobe.1.pdf)
6. [Presek - Računala nove dobe 2](http://lkrv.fri.uni-lj.si/popularizacija/presek/racunala.nove.dobe.2.pdf)
7. [Teta kriptografija v kratkih hlacah](https://www.dropbox.com/s/foasurrp2bzdbm5/k1.pdf)
8. [Ekonomija baze](https://en.wikipedia.org/wiki/Radix_economy)
9. [Kitajska metoda](https://en.wikipedia.org/wiki/Lattice_multiplication)
10. [Ruska metoda](https://en.wikipedia.org/wiki/Ancient_Egyptian_multiplication#Russian%20peasant%20multiplication)
11. [Cayleyeve tabele](https://en.wikipedia.org/wiki/Cayley_table)
12. [ECC]