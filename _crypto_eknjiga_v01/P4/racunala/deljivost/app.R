library(shiny)


sourceDir <- getSrcDirectory(function(dummy) {dummy})
setwd(sourceDir)


deljitelji <- 1:11
opis <- c(
  'Brez pogoja.',
  'Zadja števka je soda.',
  'Vsota števk je deljiva s 3.',
  'Zadnji dve števki sta deljivi s 4.',
  'Zadnja števka je 5 ali 0.',
  'Deljivo z 2 in 3.',
  'Alternirajoča vsota treh zaporednih števk od zadaj je deliva s 7.',
  'Zadje tri števke so deljive z 8.',
  'Vsota števk je deljiva z 9.',
  'Zadja števka je 0.',
  'Alternirajoča vsota števk je deliva s 11.'
)

div1 <- function(N) {
  return(sprintf('JE. %d = 1 × %d', N, N))
}

div2 <- function(N) {
  lastd <- N %% 10
  
  if (N %% 2 == 0) {
    return(sprintf('JE. %d je sodo.', lastd))
  }
  return(sprintf('NI. %d je liho.', lastd))
}

div3 <- function(N) {
  digits <- as.numeric(unlist(strsplit(as.character(N), "")))
  strsum <- paste(digits, collapse = ' + ')
  sum <- sum(digits)
  
  
  if (N %% 3 == 0) {
    return(sprintf("JE. %s = %d = 3 × %d", strsum, sum, floor(sum / 3)))
  }
  return(sprintf("NI. %s = %d = 3 × %d + %d", strsum, sum, floor(sum / 3), sum %%
                   3))
}

div4 <- function(N) {
  lasttwo <- N %% 100
  
  if (N %% 4 == 0) {
    return(sprintf("JE. %d = 4 × %d", lasttwo, floor(lasttwo / 4)))
  }
  return(sprintf("NI. %d = 4 × %d + %d", lasttwo, floor(lasttwo / 4), lasttwo %%
                   4))
}

div5 <- function(N) {
  last <- N %% 10
  
  if (N %% 5 == 0) {
    return(sprintf("JE. Zadnja števka je %d.", last))
  }
  return(sprintf("NI. Zadnja števka je %d.", last))
}

div6 <- function(N) {
  if (N %% 6 == 0) {
    return("JE. Deljivo s 2 in 3.")
  }
  if (N %% 2 != 0) {
    return("NI. Ni deljivo s 2.")
  }
  return("NI. Ni deljivo s 3.")
}

div7 <- function(N) {
  digits <- as.numeric(unlist(strsplit(as.character(N), "")))
  strsum <- c()
  blocks <- c()
  alter <- TRUE
  digits <- c('0', '0', digits)
  nd <- length(digits)
  for (i in 0:(ceiling((nd - 2) / 3) - 1)) {
    blk <- as.numeric(paste(digits[(nd - i * 3 - 2):(nd - i * 3)], collapse = ''))
    blocks <- c(blocks, blk)
    if (alter) {
      alter <- !alter
      strsum <- c(strsum, blk, ' - ')
    } else{
      alter <- !alter
      strsum <- c(strsum, blk, ' + ')
    }
  }
  strsum <- paste(strsum[1:(length(strsum) - 1)], collapse = '')
  #print(strsum)
  
  sum <- sum(blocks * (-1) ^ seq(0, length(blocks) - 1))
  
  
  if (N %% 7 == 0) {
    return(sprintf("JE. %s = %d = 7 × %d", strsum, sum, floor(sum / 7)))
  }
  return(sprintf("NI. %s = %d = 7 × %d + %d", strsum, sum, floor(sum / 7), sum %%
                   7))
}

div8 <- function(N) {
  lastthree <- N %% 1000
  if (N %% 8 == 0) {
    return(sprintf("JE. %d = 8 × %d", lastthree, floor(lastthree / 8)))
  }
  return(sprintf(
    "NI. %d = 8 × %d + %d",
    lastthree,
    floor(lastthree / 8),
    lastthree %% 8
  ))
}

div9 <- function(N) {
  digits <- as.numeric(unlist(strsplit(as.character(N), "")))
  strsum <- paste(digits, collapse = ' + ')
  sum <- sum(digits)
  
  
  if (N %% 9 == 0) {
    return(sprintf("JE. %s = %d = 9 × %d", strsum, sum, floor(sum / 9)))
  }
  return(sprintf("NI. %s = %d = 9 × %d + %d", strsum, sum, floor(sum / 9), sum %%
                   9))
}

div10 <- function(N) {
  last <- N %% 10
  if (N %% 10 == 0) {
    return(sprintf("JE. Zadnja števka je 0."))
  }
  return(sprintf("NI. Zadnja števka je %d.", last))
}

div11 <- function(N) {
  digits <- rev(as.numeric(unlist(strsplit(as.character(N), ""))))
  strsum <- c()
  alter <- TRUE
  for (d in digits) {
    if (alter) {
      alter <- !alter
      strsum <- c(strsum, d, ' - ')
    } else{
      alter <- !alter
      strsum <- c(strsum, d, ' + ')
    }
  }
  strsum <- paste(strsum[1:(length(strsum) - 1)], collapse = '')
  #print(strsum)
  sum <- sum(digits * (-1) ^ seq(0, length(digits) - 1))
  
  
  if (N %% 11 == 0) {
    return(sprintf("JE. %s = %d = 11 × %d", strsum, sum, floor(sum / 11)))
  }
  return(sprintf("NI. %s = %d = 11 × %d + %d", strsum, sum, floor(sum /
                                                                    11), sum %% 11))
}


ui <- fluidPage(
  tags$head(tags$script(src = "dijeljivost.js"),
  tags$link(rel = "stylesheet", type = "text/css", href = "dijeljivost.css"))
    ,fluidRow(column(
  8, textInput('stevilo', 'Vnesi število:', value = '12345')
)),
fluidRow(id='djeljitelji',column(12, tableOutput(
  'deljitelji_tabela'
))))

`%then%` <- shiny:::`%OR%`

server <- function(input, output) {
  output$deljitelji_tabela <- renderTable({
    stevilo <- strtoi(input$stevilo, 10L)
    validate(
      need(input$stevilo, '') %then%
        need(!is.na(stevilo), 'Zapis števila ni pravi.') %then%
        need(stevilo >= 0, 'Število ni pozitivno.')
    )
    izracun <-
      c(
        div1(stevilo),
        div2(stevilo),
        div3(stevilo),
        div4(stevilo),
        div5(stevilo),
        div6(stevilo),
        div7(stevilo),
        div8(stevilo),
        div9(stevilo),
        div10(stevilo),
        div11(stevilo)
      )
    #print(izracun)
    ostanek <- stevilo %% deljitelji
    deljitelji_tabela <-
      matrix(c(deljitelji, opis, izracun, ostanek), ncol = 4)
    colnames(deljitelji_tabela) <-
      c('deljitelj',
        'pogoj za deljivost',
        'veljavnost pogoja',
        'ostanek')
    deljitelji_tabela
  }, rownames = FALSE, digits = 0, bordered = TRUE, align = 'l')
}


# Run the application
shinyApp(ui = ui, server = server)
