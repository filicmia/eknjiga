# Zgoscevalne funkciije
<script src='./js-libs/iframeResizer.min.js'></script>

## Uvod {-}

Kot veliko protokolov in nasplošno tehnik, ki jih uporabljamo v računalništvu, tudi zgoščevalne funkcije izhajajo iz našega vsakodnevnega življenja. Naprimer: zjutraj greš z družino na zajtrk. Vsak član družine naroči natakarju, kaj bo jedel in pil, na koncu pa pridni natakar, ki hoče biti siguren, da se ni zmotil pri zapisovanju naročila, vpraša: "Torej skupaj boste 2 kavi in 3 rogljičke, je tako?"
Komaj opisan postopek, ki ga je uporabil natakar, je res zelo primitiven primer zgoščevalne funkcije. Kaj je v bistvu naredil? Vzel je dolgo sporočilo, kjer je vsaki član družine naročil nekaj zase in ga predelal v krajšega, ki služi temu, da preveri, ali je med prenosom informacije prišlo do napake. 

Bolj formalno povedano:

   * Zgoščevalna funkcija je katerakoli funkcija, ki jo lahko uporabimo, z namenom da vhodni podatek poljubne velikosti preslikamo v nek podatek vnaprej definirane velikosti.


## Primerov 

Take funkcije so vsepovsod okoli nas, poglejmo si nekaj primerov:

### ISBN

```{r, echo = FALSE}
knitr::include_app("https://mathtravel.shinyapps.io/ISBN")
```
ISBN (International Standart Book Number) je desetmestna  (ali trinajstmestna) številka, ki enolično definira neko knjigo. Vsaka knjiga tiskana po letu 1970 ima svojo ISBN številko. Prvih 9 oz 12 cifer označuje naslov knjige, avtorja, založbo in jezik, v katerem je napisana, zadnja cifra pa je zgoščena vrednost vodilnih cifer. Zato, da bi izračunali to zgoščeno vrednost, vsako cifro pomnožimo z ustreznim faktorjem, vsoto pa delimo s številom 11. 

Natančen postopek samega izračuna lahko preveriš [tukaj](https://www.geeksforgeeks.org/program-check-isbn/), sam pa lahko vzameš knjigo, ki ti je najbolj blizu, pa poskusiš, če ta postopek deluje ali ne!

### EMŠO

```{r, echo = FALSE}
knitr::include_app("https://mathtravel.shinyapps.io/emso")
```
Na hrbtni strani našega osebnega dokumenta je zapisana številka EMŠO (Enotna Matična Številka Občana). Njena sestava je precej preprosta: datum rojstva določa prvih sedem cifer, osma in deveta cifra je pri nas vedno 50 (kar označuje rojstvo v Sloveniji), sledijo še tri cifre, ki določajo zaporedno številko rojstva (pri tem se razlikuje med rojstvi otrok moškega in ženskega spola). Sledi še [zadnja cifra](https://sl.wikipedia.org/wiki/Enotna_mati%C4%8Dna_%C5%A1tevilka_ob%C4%8Dana), ki je v bistvu zgoščena vrednost vseh ostalih. 
Tukaj lahko poskusiš generirati svojo EMŠO številko, pa tudi tisto izmišljenih oseb. Poskusi pa odgovoriti na naslednja vprašanja:
 
  1) Kako se spremeni EMŠO, če spremeniš ime?
 
  2) Ali lahko imata dva občana isto EMŠO številko?
 
  3) Kaj lahko sklepaš o imetniku dane EMŠO številke, če o njem veš samo EMŠO?

### Številke plačilnih kartic

```{r, echo = FALSE}
knitr::include_app("https://mathtravel.shinyapps.io/stevilke-placilnih-kartica")
```
Tudi zadnja števka naših plačilnih kartic je zgoščena funkcija prvih petnajstih. Izračunamo jo s tako imenovanim Luthovim algoritmom, ki ima podoben postopek kot zgornja dva primera: ustrezno pomnožimo cifre kartice, jih seštejemo, na koncu pa izvedemo še neko celoštevilsko deljenje. 
Na spletu najdemo več takih aplikacij, ki ponaredijo izmišljene kartice (v bistvu izberejo skoraj naključno prvih 15 cifer in izračunajo na koncu še kontrolno vsoto).Postopek, s katerim bi lahko trivialno preverili, ali so take kartice resnične ali ne, ne obstaja; to pa omogoča razna protizakonita obnašanja (na primer lahko vsaki mesec [generiramo izmišljeno kartico](https://www.getcreditcardnumbers.com/) in tako koristimo "brezplačni mesec" na raznih spletnih straneh kot "Netflix". Ponovimo pa da je tako obnašanje protizakonito). Nekatere spletne strani odvzamejo iz računa 1 cent in ga takoj vrnejo z namenom preverjanja pristnosti kartice. Proti takemu sistemu za preverjanje kartic seveda spodnji postopek ni odporen. 
Tudi s spodnjim pripomočkom lahko generiramo izmišljene kartice: vnesi prvih 15 cifer, dobil pa boš veljavno številko kartice! 

### Zaključek
Iz zgoraj navedenih primerov bi moralo biti jasno, da osnovni podatki enolično določajo zgoščeno vrednost, obratno pa ne velja. Na primer, če se navežemo na zgornji primer ISBN-ja: obstaja namreč več milijonov knjig, vsaka ima enolično določeno ISBN kodo, z druge strani pa obstaja samo 11 zgoščenih vrednosti za podane osnovne podatke. 

Podoben razmislek lahko uporabimo v računalništvu, tudi ko nas vsebina in nasploh pomen podatkov, ki prenašamo, sploh ne zanima. Naprimer, da želimo sporočilo dolgo 20 bitov zgostiti v vrednost dolgo 5 bitov. V takem primeru, možnih 1048576 (2<sup>20</sup>) vhodnih vrednosti bo zgoščevalna funkcija razdelila v skupine, ki bodo imele isto zgoščeno vrednost; v povprečju bodo te skupine velike 32768 (2<sup>15</sup>), osnovna želja pa je seveda, da so skupine čim bolj enakomerne. Takemu dogodku, kjer se dva ali več vhodnih podatkov preslikata v isto zgoščeno vrednost, pravimo <strong>kolizija</strong> (ang. collision), temu pa se seveda želimo izogniti.

## Lastnosti

Osnovne lastnosti, ki želimo, da jih ima zgoščevalna funkcija, so:

1) Enosmernost (one-way): če imamo podano zgoščeno vrednost h, želimo, da je čim težje dobiti neko originalno sporočilo m (le-temu pravimo čistopis), ki ga je generiralo. Dejstvo, da je h zgoščena vrednost čistopisa m, zgoščenega s funkcijo H, označujemo h=H(m);

2)	Enostavnost: želimo, da je izračun zgoščene vrednosti čim bolj enostaven;

3)	Odpornost na kolizije (collision resistant): želimo, da je čim težje dobiti dva čistopisa m<sub>1</sub> in m<sub>2</sub>, ki se preslikata v isto zgoščeno vrednost, tj H(m<sub>1</sub>)=H(m<sub>2</sub>).

Iz zgoraj opisanih lastnosti sledi, da se mora zgoščevalna funkcija razpršiti čim bolj enakomerno po prostoru, ki ga ima na razpolago. Zaradi teh lastnosti imajo zgoščevalne funkcije veliko možnih primerkov uporabe. V računalništvu jih najdemo, med drugim, v podatkovnih bazah, podatkovnih strukturah in nenazadnje v računalniških komunikacijah, kjer jih uporabljamo zato, da bi imeli čim bolj varno in zanesljivo komunikacijo, v nadaljevanju pa se bomo omejili samo na ta zadnji primer uporabe. 

### Zgoščevalne funkcije v računalniških komunikacijah 
Ker smo na spletu podvrženi [raznim nevarnostim](https://www.spyzie.com/monitor/top-5-dangers-of-using-the-internet.html), uporabljamo razne protokole (glej RSA, SSL, DES, ...), zato da bi zagotovili:
1) Integriteto (ang. integrity): želimo preprečiti možnost, da nekdo spremeni podatke, ki si jih dve osebi izmenjujeta, ne da bi le-ti zaznali spremembe v sporočilu;

2) Avtentikacijo (ang. authentication): želimo biti zmožni dokazati identiteto svojega sogovornika. Ta lastnost je še posebej pomembna, ko preko spleta pošiljamo osebne podatke;

3) Onemogočanje zanikanja (ang. non repudiation): želimo dokazati, da je neka oseba res bila tista, ki je poslala določeno sporočilo.

V osnovi so v dobri meri te lastnosti zagotovljene z uporabo zgoščevalnih funkcij in enkripcije čistopisa. Naprimer, ko želimo sporočilo m poslati preko interneta: sporočilu dodamo njegovo zgoščeno vrednost H(m), prejemnik sporočila pa bo prejel sporočilo m', ki je lahko enako sporočilu, ki smo poslali, z druge strani pa se je lahko nekdo vrinil v komunikacijo in spremenil čistopis m in ga nadomestil z m'. Prejemnik bo izračunal zgoščeno vrednost H(m') in jo primerjal z H(m), ki je prejel. V primeru, da sta enaki, bo vedel, da med komunikacijo sporočilo m ni bilo spremenjeno. Če nista, bo vedel, da je nekdo sporočilo spremenil. Seveda je dejanski postopek bistveno kompleksnejši, saj v primeru, da napadalec izve za zgoščevalno funkcijo, ki jo sogovornika uporabljata, lahko nadomesti tudi zgoščeno vrednost. Zato, da se to ne bi zgodilo, se poleg čistopisa zgosti še neki tajni podatek, ki ga poznata samo pošiljatelj in prejemnik.

### Hashiranje gesel
Drugo področje, kjer uporabljamo zgoščevalne funkcije za zagotavljanje varnosti, je v hranjenju gesel v serverjih. Osnovna ideja je, da v primeru, ko nekdo uspe iz kakršnegakoli razloga dobiti dostop do računalnika, kjer so shranjena gesla, le-ta ne uspe izluščiti gesel uporabnikov. Denimo konkreten primer: Facebook ima več kot dve milijardi uporabnikov. Vsak,ki želi vstopiti v svoj račun, mora dokazati svojo identiteto s tem, da vnese svoj e-mail in neko geslo. E-mail je v večini primerov javen, z druge strani pa je geslo nekaj, kar pozna samo uporabnik. Le-ta seveda hoče, da ostane njegovo geslo tajno (še posebej če rabi isto geslo za več spletnih strani). Recimo pa primer, da bi Facebook hranil gesla v čistopisu (npr geslo uporabnika je '12345' in je na serverju geslo shranjeno kot '12345'). Ko neki uporabnik želi vstopiti, sistem preveri, ali je vnešeno geslo enako tistemu, ki je hranjeno v serverju. Taki sistem deluje v redu, dokler napadalec ne uspe vstopiti v računalnik, kjer so hranjena gesla in si tako brez težav prilasti gesla vseh uporabnikov. Seveda se skušamo izogibati takemu sistemu, žal pa je še vedno veliko [takih spletnih strežnikov](http://plaintextoffenders.com/about/), ki hranijo gesla v čistopisu; nenazadnje nismo omenili takega velikana kot je Facebook zaman: ravno v prvih mesecih leta 2019 smo izvedeli, da je Facebook hranil gesla stotin milijonov uporabnikov v čistopisu, lastniki gesel pa so seveda bili v nevarnosti.

Drugi način hranjenja gesel je z zgoščeno vrednostjo. V spletnih strežnikih je shranjena zgoščena vrednost gesla, tako da tudi če bi napadalec imel dostop do strežnika, ne bi uspel izluščiti gesla uporabnikov. Ta tehnika je sicer boljša kot prešnja, vendar ima tudi ta nekaj pomanjkljivosti: najhujša je, da se ista gesla slikajo v isto zgoščeno vrednost. To lahko ustvari težave, saj v primeru, da ima veliko uporabnikov isto geslo in se na kakšen način uspemo polastiti enega od teh gesel, potem bomo vedeli gesla vseh uporabnikov, ki imajo isto zgoščeno vrednost gesla (pri tem predpostavimo, da je verjetnost, da pride do kolizije, skoraj ničelna). Temu služijo [mavrične tabele](https://en.wikipedia.org/wiki/Rainbow_table), ki so vnaprej sestavljene tabele čistopisov in njihovih zgoščenih vrednosti, v odvisnosti od zgoščevalne funkcije. Take tabele izredno pohitrijo brute-force napade. Zaradi takega pristopa je [Adobe](https://www.theguardian.com/technology/2013/nov/07/adobe-password-leak-can-check) imel kar nekaj težav leta 2013.

Danes najučinkovitejša strategija je hranjenje gesel z zgoščenimi funkcijami, ki pa se zgostijo skupaj z naključnim podatkom. To zagotovi, da se bodo ista gesla slikala v različne zgoščene vrednosti.

Ker pa varnosti ni nikoli preveč, se lahko uporablja kombinacija zgornjih pristopov in enkripcije. Tako strategijo uporablja na primer [Dropbox](https://blogs.dropbox.com/tech/2016/09/how-dropbox-securely-stores-your-passwords/). 

## Dodatne interakcije

<div class='todo'>
Tekst okoli vsake interakciije
</div>

### Barkode
<iframe src="./P2/barKode/BarCodeV2.html" id = 'barCode-iframe'></iframe>

### Hash funkcije
<iframe src="./P2/hash_funkcije/hash_funkcije.html" id = 'hash-funkcije-iframe'></iframe>

### Rojstni dnevi
```{r, echo = FALSE}
knitr::include_app("https://mathtravel.shinyapps.io/rojstni-dnevi")
```
<script>
  iFrameResize({ log: true, warningTimeout: 10000}, '#barCode-iframe, #hash-funkcije-iframe')
</script>