---
title: "Graf elipticnih krivulj"
author: "Klemen Klanjscek"
date: "3 april 2019"
output: html_document
runtime: shiny
resource_files:
- FFPlot/app.R
---

```{r setup, include=FALSE}

```

### Interaktivni grah elipticne krivulje $y^2= x^3 + ax^2+b$ v $\mathbb{Z}_q$
```{r, echo = FALSE}
shinyAppDir("FFPlot/",
  options = list(width = "100%", height = "800")
)
```
