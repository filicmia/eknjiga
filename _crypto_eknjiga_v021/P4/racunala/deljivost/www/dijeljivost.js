// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true, subtree: true };

//rows of the djeljitelji tabela.
var rows, deljitelji_tabela, index_ostatek = 3; //the column in the table

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
    for(var mutation of mutationsList) {
        if (mutation.type == 'childList') {
            console.log('A child node has been added or removed.');
        }
        else if (mutation.type == 'attributes') {
            console.log('The ' + mutation.attributeName + ' attribute was modified.');
        }
    }
    rows = deljitelji_tabela.querySelectorAll("tr");
    
    var row;
    for (i = 0, len = rows.length; i < len; i++) { 
      var cols = rows[i].querySelectorAll('td');
      if(cols.length > 0){
        if(cols[index_ostatek].innerHTML == 0){
          //http://colorsafe.co/
          rows[i].style.background = '#3D9970';
        } else {
          rows[i].style.background = '#e46469';
        }
        rows[i].style.color = '#004055'
      }
    }
    
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);

window.addEventListener("load", function(event) {
  
    deljitelji_tabela = document.querySelector("div#deljitelji_tabela");
    //alert(deljitelji_tabela.innerHTML);
    rows = deljitelji_tabela.querySelectorAll("#table");
    
    // Start observing the target node for configured mutations
    observer.observe(deljitelji_tabela, config);
  
});