# Asymmetric (key) cryptography. 

```{r filtriranjeVsebine}
source('../P/params.R')
```
Throughout this book, we have already encountered a lot symmetric cryptosystems, i.e. Ceaser chiper <link to the chapter>, Vigenare chiper <link to the chapter>, One Time Pad <link to the chapter> etc. <br/>

We call those symmetric cryptosystems as they use the same secret parameter - key for encryption and decryption. 
```{r}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/privateCrypto.png"))
```

NB. Taking the example of Eve sending a secure message to Bob, both of them need to have the same key in order to encrypt and decrypt the messages that they may exchange with each other.

On the contrary, asymmetric cryptosystem uses not one key, but a pair of keys: a private key and a public key. You might ask: 

> Why do we need two keys?

In asymmetric cryptography, one key is used to encrypt the data, the public key, and the other to decrypt the encrypted message (e.g. chipertext), the private key.

When encrypting the message using, let’s say Bob's public key (continuing the example from the introduction), that same message can  be decrypted only using Bob's private key.

Moreover, someone's, i.e. Bob's, **private key**, as the name states, must be kept private to that someone (Bob). It’s the only key that can decrypt any messaged that was encrypted with i.e. Bob's public key. 

**Public keys** as, yet again, the name states, are public and thus no security is required because of it should publicly available and can be passed over the internet. The public key is used to encrypt a message that can only be decrypted using its private counterpart.

For a better understanding what is going on when encrypting with asymetric cryyptosystem, take look at the pictture above.
```{r}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/AliceBobpng_asymmetric.png"))
```

On a picture above, we can see that Alice uses key $K$ to encrypt the message, which then Bob decrypts with a different key, $K'$. $K'$ is his private key, and $K$ is $K'$'s public counterpart.

At this point, you should:

1. have a feeling what cryptosystems are,
2. be aware of the crucial difference between symmetric and asymmetric cryptography (encryption),
3. know at least two symmetric cryptosystems.

What about asymmetric cryptosystems? Are there some? <br/>
Ofcourse. <br/>
Let us present some.

## Our first asymmetric cryptosystem: RSA

RSA cryptosystem was invented by Ron Rivest, Adi Shamir and Leonard Adleman in 1978.
In practice RSA is rather slow so it’s hardly used to encrypt data , more frequently it is used to encrypt and pass around symmetric keys which actually deal with encryption, at faster rate.

### How does it work?
To explain the RSA we will use rather small numbers, but keep in mind that the real value of most of $\mod(p)$ based algorithms happens when huge primary numbers are used.

The first part we’ll show how the encryption and decryption functions work, and then we explain why. Let us start.

```{r}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/rsa1.png"))
```

We’ve got a message ('HELLO') , and we’ve picked two tuples with two numbers each (we will explain have picked those a bit later). Obviously there’s no arithmetic operation we can perform with strings, so the message has to be converted into something. Let’s say 'HELLO' converts, using some conversion algorithm, to '2'.

Normally, a lot of different techniques are used to unify the message and padding, with the format of the keys (usually a bit string).

Fantastic, now we have everything we need. Let’s run the message through the functions that will in theory encrypt our message for us:

```{r}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/rsa2mini.png"))
```

So there we have the basics of the RSA algorithm , and how the trapped function is laid out.

The interesting bit is how we come about those numbers, and how (5,14) is related to (11,14). 
Let us start.

### The details of picking the Decryption/Encryption pair

Now we present the steps of the RSA algorithm set up phase :)

1. Frst, pick two prime numbers. For example, 
```
P = 2 and Q = 7
```
2. Secondly, multiply `P` and `Q`. The results becomes modulus of the RSA encryption.
```
N = P * Q = 14
```
3. Thirdly, make a list between 1 and $N$ and remove numbers that have common factors with `N`:
```{r}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/rsaInverses.png"))
```

Calculate the length `L = LengthNCF(14)` of the resultng list. 
There’s an easy way to calculate that and that is:
`( Q - 1 ) * ( P - 1) = L` <br/>
We calculate `L = (7 - 1 ) * ( 2 -1 ) = 6`.

4. Now is time to pick the encryption key. In the example it was (5,14), and we know noew, that 14 is the modulus.<br/>
When pickng the encryption key, there are few rules that must not be broken:
  * The encryption key need to be between 1 and `L`, i.e. from `[2,3,4,5]`
  * The encryption key need to be coprime with `L` (6) and the Modulus (14).
In our case, there is only one possibility for encryption (publc) key, and is 5.
So there we came to a conclusion of why the public key is equal to (5,14).
5. Lastly, we pick the Decryption part, private key. In the example $(11,14)$ has been picked. Again 14 is the modulus but what about 11? </br>
Let us denote it wth `D`.
```{r}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/rsa4.png"))
```
It iis because, the Decryptor(11) (private key) multiplied by the Encryptor(5) (publc key) modulus the length `L` has to be equals to 1!
```
D * E % LNCF(N) = 1
11 * 5 % 6 = 1
```
To get `D`, one can make a list of all numbers from 1 to `T`, i.e. 50 (for now, it needs to be at least `N`) and, filter the ones that when multiplied by E and moduled by `L` are equal to 1. <br/>
When setting `T = 50`, we get the followng:
```{r}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/rsa5.png"))
```
Let’s see if gotten numbers can successfully decrypt the message :). 

Remember the encrypted message is `4`,and the decrypted message is `2`.
Thus, we should get something like this:
```
4 ** D % 14 = 2 <---Decrypted message
```
So let’s do a little list comprehension to see if the rule works:
```{r}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/rsa6.png"))
```

And it worked! Well done. 

You see how applying the function to all the Decryption keys that follow the rule `(D * E % L = 1)` , decrypted the message successfully.

We hope this didn’t get too complex :).

#### Remarks

For encrypting and decrypting the messages, we need toknow how to calculate the power of
a number in modulus arithmetic. We showed how is it done in previous chapter <link>. We have also provided the application that helps us calculate those <link>.

In step 4. we actually search for all numbers that are coprime with the `L`. We can do that usiing Eratosten seed from previous chapter <link>.

In step five, we actually look for the invers of the `E` modulo `N`, multiplicative inverse. Thus, we can utilize the interction from the previous chapter <link>.

In practice `E` is selected to be small so that Encryption is fast.

**Warning** <br/>
Do not use this or any other algorithm naively!

### Security of RSA

Detailed security analysis is out of the scope of the reading, but we find it crucial to mention existance of several security holes that should be keptt in mind in instantiations.

#### Possible security holes:

1. There is a need to use `safe` primes `P` and `Q`.  In particular `P-1` and `Q-1` should have large prime factors,
2. `P` and `Q` should not have the same number of digits,  
<!--Then one can effectevly use a middle attack starting at sqrt(n).-->
3. $E$ cannot be too small,
4. Don't use same $N$ for different $E$’s,
5. Always `pad`.

### RSA in the “Real World”

RSA Encryption is a part of many standards, i.e. PKCS, ITU X.509, (ANSI X9.31, IEEE P1363
Furthermore, it is used by: SSL, PEM, PGP, Entrust,etc.

The standards specify many details on the implementation, e.g.

1. $E$ should be selected to be small, but not too small,
2. 'multi prime' versions make use of `N = PQR` etc. This makes it cheaper to decode especially in parallel using Chinese remainder theorem <link>.