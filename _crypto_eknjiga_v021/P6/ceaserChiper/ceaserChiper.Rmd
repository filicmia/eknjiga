---
title: "Ceaser Chiper"
resource_files:
- ceaserAttack/app.R
- ceaserAttack/ceaserAttack.html
- ceaserAttack/www/ceaserAttack.js
- www-shared/ceaserChiper.js
- www-shared/ceaserStyle.css
- ceaserDecrypt/www/ceaserDecrypt.js
- ceaserDecrypt/app.R
- ceaserEncrypt/app.R
- ceaserEncrypt/ceaserEncrypt.html
- ceaserEncrypt/www/ceaserEncrypt.js
- ceaserDecrypt/ceaserDecrypt.html
runtime: shiny
output:
  flexdashboard::flex_dashboard:
    orientation: rows
---


```{r setup, include=FALSE}
source('params.R')
```

Row {.tabset .tabset-fade}
-------------------------------------
    
### Encryption
    
```{r, echo = FALSE}
shinyAppDir("ceaserEncrypt/",
  options = list(width = "100%")
)
```
   
### Decryption

```{r, echo = FALSE}
shinyAppDir("ceaserDecrypt/",
  options = list(width = "100%")
)
```
 
### Attack
    
```{r, echo = FALSE}
shinyAppDir("ceaserAttack/",
  options = list(width = "100%")
)
```