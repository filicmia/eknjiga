library(shiny)

sourceDir <- getSrcDirectory(function(dummy) {dummy})
setwd(sourceDir)

ui <- fluidPage(
  tags$head(tags$script(src = "ceaserDecrypt.js"),
            tags$link(rel = "stylesheet", type = "text/css", href = "www/ceaserStyle.css")),
  mainPanel(
    includeHTML("ceaserDecrypt.html")
  )
)

server <- function(input, output, session) {
}

#v params.R
#addResourcePath('www', directoryPath = '../www-shared')
#addResourcePath('js', directoryPath = '../www-shared') 
shinyApp(ui, server)