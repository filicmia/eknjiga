var alphabet = " abcdefghijklmnopqrstuvwxyz";

// Function creates and returns new HTML title.
function new_title(type, text) {
  var title = document.createElement(type);
  title.innerHTML = text;
  return title;
}

// Function creates and returns new HTML input field.
function new_input(type, name) {
  var input = document.createElement('input');
  input.setAttribute("type", type);
  //input.setAttribute("name", name);
  input.id = name;
  return input;
}

// Function creates and returns new HTML button.
function new_button(value, onclick) {
  var button = document.createElement("input");
  button.type = "button";
  button.value = value;
  button.onclick = onclick;
  return button;
}

// Function that creates new div representing one person in communication.
function create_new_person(name, id) {
  var alice = document.createElement('div');
  alice.setAttribute("id", id);
  alice.setAttribute("class", "user");
  
  // Append all elements.
  alice.appendChild(new_title("h2", name));
  alice.appendChild(new_title("h3", "Key:"));
  alice.appendChild(new_input("text", id+"_key_input"));
  alice.appendChild(new_title("h3", "Message:"));
  alice.appendChild(new_input("text", id+"_message_input"));
  alice.appendChild(new_button("Generate & Encrypt", function(){generate_encrypt(id);}));
  alice.appendChild(new_title("h3", "Ciphertext:"));
  alice.appendChild(new_input("text", id+"_ciphertext_input"));
  alice.appendChild(new_button("Decrypt", function(){decrypt(id);}));
  alice.appendChild(new_button("Send", function(){send(id);}));
  
  return alice;
}

// Function generates key of length len.
function generate_key(len) {
  var output = "";
  var n = alphabet.length;
  for(var i=0; i<len; i++) {
    var r = Math.floor(Math.random() * n);
    output += alphabet.charAt(r);
  }
  return output;
}

// Function is called when button Generate & Encrypt is clicked.
// It generates output new key and encrypt the message.
function generate_encrypt(id) {
  var plaintext = document.getElementById(id+"_message_input").value;
  var n = plaintext.length;
  if(n == '0') {
    window.alert("Message is not set.");
    return;
  }
  var key = generate_key(n);
  document.getElementById(id+"_key_input").value = key;
  var ciphertext = "";
  for(var i=0; i<n; i++) {
    var index = (alphabet.indexOf(plaintext.charAt(i)) + alphabet.indexOf(key.charAt(i))) % alphabet.length;
    ciphertext += alphabet.charAt(index);
  }
  document.getElementById(id+"_ciphertext_input").value = ciphertext;
}

// Function for decryption of cyphertext.
function decrypt(id) {
  var ciphertext = document.getElementById(id+"_ciphertext_input").value;
  var n = ciphertext.length;
  if(n == '0') {
    window.alert("Ciphertext is not set.");
  }
  var key = document.getElementById(id+"_key_input").value;
  if(key.length < n) {
    window.alert("Key is too short!");
    return;
  }
  var plaintext = "";
  for(var i=0; i<n; i++) {
    var index = (alphabet.indexOf(ciphertext.charAt(i)) - alphabet.indexOf(key.charAt(i)) + alphabet.length) % alphabet.length;
    plaintext += alphabet.charAt(index);
  }
  document.getElementById(id+"_message_input").value = plaintext;
}

// It sends ciphertext from one person to another.
function send(from) {
  var to = "alice";
  if(from == "alice") to = "bob";
  var ciphertext = document.getElementById(from+"_ciphertext_input").value;
  document.getElementById(to+"_ciphertext_input").value = ciphertext;
}

window.addEventListener("load", function(event) {
  var wrapper = document.getElementById('cchiperWrapper');
  wrapper.appendChild(new_title("h1", "One Time Pad"));
  
  wrapper.appendChild(create_new_person("Alice", "alice"));
  wrapper.appendChild(create_new_person("Bob", "bob"));
});
