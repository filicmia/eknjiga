--- 
title: "Ebook crypto 2018/2019"
author: "LKRV - FRI, UNI LJ"
date: "`r format(Sys.Date(), '%A, %d. %B, %Y', tz='Europe/Zagreb')`"
site: bookdown::bookdown_site
output: bookdown::gitbook
documentclass: book
bibliography: [bibliography.bib]
biblio-style: apalike
link-citations: yes
description: "Crypto ebook 2018/2019"
---

```{r filtriranjeVsebine}
source('params.R')
```

#Uvod {#uvod}

# Uvod
Večina bralcev se je najbrž že spoznala z besedo kriptografija, ne ve pa točno, kaj ta znanost je. Na kratko povedano, gre za znanost o skrivnostih, tj. skrivnem pisanju in skrivnem dostopu do svojih dokumentov. Povedano natančneje, kriptografija poskuša omogočiti zasebnost. Glavni cilji kriptografije so  zaupnost, avtentičnost, celovitost in preprečevanje zlorab.

V splošnem obstajata dva glavna načina, tj. kriptografska protokola, ki omogočata skrivno komunikacijo:

1. kriptografija javnega ključa,
2. simetrična kriptografija.

Glavna razlika med obema je v njunem načinu šifriranja in dešifriranja.

Razložimo najprej, kaj si lahko predstavljamo pod pojmom kriptosistem. 

```{r}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/AliceBobpng_init.png"))
```

Zgornja slika je osnovni primer kriptografije. Predstavlja komunikacijo dveh oseb. Recimo jima Alice in Bob. Alice in Bob želita komunicirati med seboj (ker si imata toliko za povedati). Zaradi določenih razlogov želita, da njuna 
komunikacija ostane skrivna. V ta namen uporabljata neke vrste kriptografijo. Da pa lahko uporabljata kriptografijo, pred začetkom komunikacije
dorečeta nekatere stvari. To so:

(1) algoritem, ki bo naredil vsako sporočilo med njima skrivno (šifrirni algoritem oz. šifrirno funkcijo),
(2) algoritem za dešifriranje (dešifrirna funkcija), ki bo omogočila tretji osebi brati zašifrirano sporočilo,
(3) vrsta skrivnostih ključev
(4) kakšne vrste sporočil bosta izmenjevala prek komunikacijskega kanala
   (a) domena šifrirne funkcije (kodomena dešifrirne funkcije)
   (b) kodomena šifrirne funkcije (domena dešifrirne funkcije)

> We can call it environment set up.

Postopek, s katerim naredimo sporočilo skrivno, imenujemo šifriranje. Zašifrirano sporočilo imenujemo šifrirni tekst. 
Postopek, s katerim naredimo sporočilo spet berljivo, imenujemo dešifriranje. Dešifrirano sporočilo je enako začetnemu tekstu, ki ga imenujemo osnovni tekst.

Ko je okolje za pogovor postavljeno, Alice in Bob lahko začneta pogovor.

1. Kaj se zgodi, ko Alice želi poslati sporočilo Bobu? 

Ko Alice želi poslati sporočilo $M$ Bobu, najprej zašifrira sporočilo $M$ in ga potem pošlje Bobu prek sporočilnega kanala (kot to naredi https protokol).
Ko Bob prejme sporočilo, ga mora najprej dešifrirati, da ga lahko prebere. Zašifriran tekst, ki ga dobi od Alice, ne sme biti povezan z začetnim sporočilom, saj nočemo, da lahko kdorkoli, ki je povezan s komunikacijskim kanalom, prebere to začetno sporočilo $M$.

Primer:
Poskusimo uporabiti točno določen simetrični kriptosistem. To pomeni, da bosta šifrirni in dešifrirni ključ enaka ali pa povezana.

Vpleteni osebi sta kot običajno Alice in Bob. Preden se komunikacija začne, se na kavi dogovorita o naslednjih vprašanjih:

(1) Katero sporočilo želita pošiljati? :Angleške besede, ki imajo 26 črk (opustili bomo prazna mesta kot v tem primeru)
(2) Kakšne vrste zašifrirano sporočilo želimo dobiti za komunikacijo? :Angleške besede, zato imamo 26 črk.

(3) Kaj bo šifrirna funkcija?

```{R, echo = T, tidy=FALSE, eval=FALSE}
Enc(m, x):
	for each letter in m:
			letter = letter + x;											
```
(4) Kaj bo dešifrirna funkcija?

```{R, echo = T, tidy=FALSE, eval=FALSE}
Dec(c, x):
	for each letter in c:
			letter = letter - x;										
```

Povedano drugače, če imamo abecedo $a, b, c, …, z$  v tabeli `a`, dekodiramo posamično črko s pravilom:

```{R, echo = T, tidy=FALSE, eval=FALSE}
letter = a[index(a==letter) + x]
```

Ta kriptografski protokol (**= dogovor o toku sporočila med dvema stranema**)
uporablja zamenjalko za šifriranje in dešifriranje sporočil.

**Zamenjalka** *kodira/šifrira* in *dekodira/dešifrira* sporočilo črko (oz. znak originalnega sporočila) po črko. \
**Bločna šifra** *kodira/šifrira* in *dekodira/dešifrira* sporočilo blok (oz. zaporedje originalnega sporočila) za blokom. \

> NB. Encryption and decryption algorithms are indeed functions and they accept one or more cryptographic parameters (beside message to be encrypted) which usually specify cryptosystem’s strength.
In here, we have only one such a parameter: x.

Primer.
Definirajmo poseben primer kriptosistema z določitvijo (skrivnega) parametra
`x`, npr. `x = 3`. \
Uporabimo to na sporočilu

<div class = "blue pre">

če je imel za povedati kar koli skrivnega, je napisal v šifrah, tj., s spreminjanjem zaporedja črk v abecedi, v kateri nobene besede ni moč ugotoviti.

</div>

<script src="./P6/ceaserChiper/www-shared/ceaserChiper.js"></script>
<script src="./P6/ceaserChiper/ceaserEncrypt/www/ceaserEncrypt.js"></script>
<link rel="stylesheet" type="text/css" href="./P6/ceaserChiper/www-shared/ceaserStyle.css">

<div id='ceaserEncrypt'> Add Chiper!</div>

Rezultat mora biti:

<div class = "pre">

li kh kdg dqbwklqj frqilghqwldo wr vdb, kh zurwh lw lq flskhu, wkdw lv, eb vr fkdqjlqj wkh rughu ri wkh ohwwhuv ri wkh doskdehw, wkdw qrw d zrug frxog eh pdgh rxw.

</div>

Preverimo lahko tudi, če je rezultat dešifriranja enak začetnemu sporočilu.

<script src="./P6/ceaserChiper/ceaserDecrypt/www/ceaserDecrypt.js"></script>
<div id='ceaserDecrypt'></div>


The adjective symmetric is used to denote the symmetry in encryption - description process, to emphasise the usage of the same crypto parameter (or one s.t. encryption parameter can be deduced from the decryption
parametter and vice versa).  

```{r}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/AliceBobpng.png"))
```
On the contrast, asymmetric cryptosystem is quite the opposite to the symmetric cryptosystem as it uses not one key, 
but a pair of keys: a private one and a public one.

V komunikacijskih kanalih, kjer je cilj zagotoviti skrivnost komunikacije:

(1) protokol,
(2) šifrirano sporočilo in
(3) šifrirna funkcija

*niso* skrivni.

Edina stvar, ki omogoča kriptosistem varen in uporaben, je modra uporaba kriptografskih parametrov, tj. skrivnih ključev!

<div class='red'>
HERE ENDS THE ESSAY ON THE TOPIC: Introduction to cryptography.
</div>

**Kriptografija** je algoritem oz. tehnika, ki ohranja skrivnost, omogoča avtentikacijo, celovitost in prepoznavanje nepoštenih ljudi. [@stinson2018cryptography].

Kriptografski protokol ima za **cilj**, da ne sporoči več, kot je vidno v šifriranem sporočilu.


<!--https://stackoverflow.com/questions/14667770/how-to-call-a-javascript-function-when-iframe-finished-loading-->
<!--iframe id ='myframe' src='"./P6/ceaserChiper/ceaserDecrypt/ceaserDecrypt.html"' onload="onLoadHandler();"></iframe-->

