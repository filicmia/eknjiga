# Attacks {#napadi}

A **cryptographic attack** can be an attack against:

1. cryptographic protocol,
2. cryptographic algorithms,
3. techniques used to implement 1. or 2.

## Attacks against cryptographic protocol

Let get back to our `Ceaser chiper` from the introduction. \
There, we were checking the correctness of our encryption system. What ahout braking it?\
To break it, the only thing we need is to reveal the secret parameter: `x`.

One way to do it is by trying to guess it. we can do that by explicitely stating what `x` is, or by changing one letter of the chiper with another, then shift all the others accordingly (try the snippet bellow).

<script src="./P6/ceaserChiper/www-shared/ceaserChiper.js"></script>
<script src="./P6/ceaserChiper/ceaserAttack/www/ceaserAttack.js"></script>
<link rel="stylesheet" type="text/css" href="./P6/ceaserChiper/www-shared/ceaserStyle.css">

<div id='ceaserAttack'> Add Chiper!</div>

When trying all posibilities for `x`, we call this attack **brute force** attack.