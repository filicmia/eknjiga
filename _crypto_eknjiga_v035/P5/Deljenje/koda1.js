window.addEventListener('load', function(){
	console.log("Welcome");
	var tab=document.getElementById("tab");
	var reset=document.getElementById("reset");
	var a, b, n, id;
	var working=false;
	var err=document.getElementById("err");

	reset.addEventListener('click', function(){
		//preveri pravilnost vhodnih podatkov
		/*TO-DO*/

		//vstavi cikel, ce pritisnemo reset
		if(working==true){
			clearInterval(id)
			working=false;
			return;
		}
		document.getElementById("rezultat").innerHTML="";
		err.innerHTML="";
		working=true;
		while(tab.rows[0])tab.deleteRow(0);

		//vhodni podatki
		a=document.getElementById("a").value;
		b=document.getElementById("b").value;
		if(document.getElementById("tabrez").rows[0])document.getElementById("tabrez").deleteRow(0);
		var len_a=a.length;
		var len_b=b.length;
		//preverim pravilnost podatkov
		n=parseInt(document.getElementById("n").value, 10);
		if(isNaN(parseInt(a, n)) || isNaN(parseInt(b, n))){
			err.innerHTML="Nepravilen vhod";
			return;
		}

		var b_desetisko=parseInt(b, n);
		var speed=document.getElementById("speed").value;
		speed=parseInt(speed, 10);
		speed=speed*500;
		//napisemo prvo vrstico
		createRow(0, len_a+len_b+10);//popravi tuki
		insertIntoTab(0, 0,  a);
		tab.rows[0].cells[len_a].innerHTML=":";
		insertIntoTab(0, len_a+1, b);
		tab.rows[0].cells[len_a+len_b+1].innerHTML="=";
		tab.rows[0].cells[0].style.width='20px';

		var indeksDeljenje=len_a+len_b+2; //cifro rezultat deljenja napisemo v tab[0][indDeljenje]
		var indeksVrstica=1; //v katero vrstico pisemo
		var dividend=0; //desetiska predstavitev stevila ki trenutno delimo
		var indNovaCifra;
		var postaviNaNic=true; //
		var stop=false; // ko je stop=true bomo koncali	
		var trenutniZmnozek=0;
		var dodajNovo=false;
		var napisiMinus=false; //ko je postavljena na true, napisemo minus na spodnje stevilo
		id=setInterval(function(){
			if(indNovaCifra==a.length && stop==true){
				//pobarvamo prvo vrstico
				for(var i=0; i<a.length; i++)tab.rows[0].cells[i].style.backgroundColor='green';
				for(var i=a.length+1; i<=a.length+b.length; i++)tab.rows[0].cells[i].style.backgroundColor='blue';
				for(var i=a.length+b.length+2; i<indeksDeljenje; i++)tab.rows[0].cells[i].style.backgroundColor='orange';
				for(var i=0; i<len_a+1; i++){
					if(!isNaN(parseInt(tab.rows[indeksVrstica].cells[i].innerHTML, n))){
						tab.rows[indeksVrstica].cells[i].style.backgroundColor='red';
					}
				}
				//napisemo rezultat
				document.getElementById("rezultat").innerHTML="Torej velja:";
				var rez=document.getElementById("tabrez");
				var row=rez.insertRow(0);
				var i;
				for(i=0; i<indeksDeljenje; i++){
					var cell=row.insertCell(i);
					cell.innerHTML=tab.rows[0].cells[i].innerHTML;
					if(i==a.length)cell.innerHTML=" = ";
					else if(tab.rows[0].cells[i].innerHTML=="=")cell.innerHTML=" * ";
					if(i<a.length)cell.style.backgroundColor='green';
					else if(i<=a.length+b.length && i!=a.length)cell.style.backgroundColor='blue';
					else if(i!=a.length && i!=a.length+b.length+1)cell.style.backgroundColor='orange';
				}
				var cell=row.insertCell(i);
				cell.innerHTML=" + ";
				i++;
				for(var j=0; j<a.length+1; j++){
					if(!isNaN(parseInt(tab.rows[indeksVrstica].cells[j].innerHTML, n))){
						var cell=row.insertCell(i);
						cell.innerHTML=tab.rows[indeksVrstica].cells[j].innerHTML;
						i++;
						cell.style.backgroundColor='red';
					}	
				}
				working=false;
				clearInterval(id);
				return;
			}
			if(dividend<b_desetisko && indeksVrstica==1){
				for(var i=1; ; i++){
					if(i==a.length+1){
						//deljenje da 0
						createRow(1, a.length+1);
						tab.rows[0].cells[indeksDeljenje].innerHTML=0;
						for(var j=0; j<a.length; j++){
							tab.rows[1].cells[j].innerHTML=a.charAt(j);
						}
						stop=true;
						indNovaCifra=a.length;
						indeksDeljenje++;
						return;
					}
					dividend=a.substring(0, i);
					dividend=parseInt(dividend, n);
					if(dividend>=b_desetisko){
						indNovaCifra=i;
						break;
					}
				}
			}

			if(dodajNovo==true){
				//dodamo se zgornjo cifro v trenutni zmnozek
				indNovaCifra++;
				dividend=dividend*n+parseInt(a.charAt(indNovaCifra-1), 10);
				dodajNovo=false;
				insertNumber(dividend, n, indeksVrstica, indNovaCifra);
			}else if(postaviNaNic==true){
				//dodamo novo vrstico, ki se bo povecala. trenutniZmnozek je 0
				if(indeksVrstica>1)indeksVrstica++;
				createRow(indeksVrstica, len_a+len_b+10);
				tab.rows[0].cells[indeksDeljenje].innerHTML=0;
				insertNumber(0, n, indeksVrstica, indNovaCifra);
				postaviNaNic=false;
			}else if(dividend>=trenutniZmnozek+b_desetisko){
				//trenutniZmnozek povacamo za b_desetisko
				tab.rows[0].cells[indeksDeljenje].innerHTML=parseInt(tab.rows[0].cells[indeksDeljenje].innerHTML, 10)+1;
				trenutniZmnozek=trenutniZmnozek+b_desetisko;
				insertNumber(trenutniZmnozek, n, indeksVrstica, indNovaCifra);
				if(trenutniZmnozek+b_desetisko>dividend)napisiMinus=true;
			}else if(napisiMinus==true || (trenutniZmnozek==0 && tab.rows[indeksVrstica].cells[0].innerHTML.charAt(0)!='-')){
				//zapisemo minuc
				console.log(indeksVrstica+"  ");
				tab.rows[indeksVrstica].cells[0].innerHTML='-'+tab.rows[indeksVrstica].cells[0].innerHTML;
				napisiMinus=false;
			}else{
				//ne moramo vec povecati trenutnega zmnozka 
				//prepisemo razliko presnjih dveh vrstic
				tab.rows[indeksVrstica].cells[0].innerHTML=tab.rows[indeksVrstica].cells[0].innerHTML.substring(1);
				indeksVrstica++;
				indeksDeljenje++;
				createRow(indeksVrstica, len_b+len_a+10);
				dividend=dividend-trenutniZmnozek;
				insertNumber(dividend, n, indeksVrstica, indNovaCifra);
				dodajNovo=true;
				postaviNaNic=true;
				trenutniZmnozek=0;
				if(indNovaCifra==a.length)stop=true;
			}
		}, speed);
	});

	function insertNumber(num, b, i, j){
		j--;
		var numberString=num.toString(b);
		var len=numberString.length;
		var ind=len-1;
		for(var c=j; c>j-len; c--){
			tab.rows[i].cells[c].innerHTML=numberString.charAt(ind);
			ind--;
		}
	}

	function insertIntoTab(i, j, str){
		for(var s=0; s<str.length; s++){
			tab.rows[i].cells[j+s].innerHTML=str.charAt(s);
		}
		return;
	}

	function createRow(i, n){
		var row=tab.insertRow(i);
		for(var j=0; j<n; j++){
			row.insertCell(j);
		}
	}

	function fillEmpty(row, start,  end){
		for(var i=start; i<=end; i++){
			tab.rows[row].cells[i].innerHTML=" ";
		}
	}
});