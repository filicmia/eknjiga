window.addEventListener('load', function() {
	console.log("Welcome");
	var tab=document.getElementById("tab");

	document.getElementById("reset").addEventListener('click', function(){
		document.getElementById("err").innerHTML="";
		while(tab.rows[0]){
			tab.deleteRow(0);
		}
		var a=document.getElementById("a").value;
		a=parseInt(a, 10);
		var b=document.getElementById("b").value;
		b=parseInt(b, 10);

		if(b>a){
			document.getElementById("err").innerHTML="Stevila, ki si vnesel, niso pravilna. V aritmetiki ure, kjer so vsa stevila v modulu n, morajo biti vse vrednost med 0 in n (0 vkljucen, n izkljucen).";
		}
		var krat="<span>&#183;</span>";

		//prva vrstica
		var row=tab.insertRow(0);
		var cell=row.insertCell(0);
		cell.innerHTML='index i';
		cell.style.width="50px";
		cell=row.insertCell(1);
		cell.innerHTML="kvocijent";
		cell.style.width="150px";
		cell=row.insertCell(2);
		cell.innerHTML="Ostanek";
		cell.style.width="150px";
		cell=row.insertCell(3);
		cell.innerHTML="s<sub>i</sub>";
		cell.style.width="150px";
		cell=row.insertCell(4);
		cell.innerHTML="t<sub>i</sub>";
		cell.style.width="150px";

		//Druga vrstica
		row=tab.insertRow(1);
		cell=row.insertCell(0);
		cell.innerHTML='0';
		cell=row.insertCell(1);
		cell.innerHTML="  ";
		cell=row.insertCell(2);
		cell.innerHTML=a.toString(10);
		cell=row.insertCell(3);
		cell.innerHTML="1";
		cell=row.insertCell(4);
		cell.innerHTML="0";

		//tretja vrstica
		row=tab.insertRow(2);
		cell=row.insertCell(0);
		cell.innerHTML='1';
		cell=row.insertCell(1);
		cell.innerHTML="  ";
		cell=row.insertCell(2);
		cell.innerHTML=b.toString(10);
		cell=row.insertCell(3);
		cell.innerHTML="0";
		cell=row.insertCell(4);
		cell.innerHTML="1";

		//zanka za vse ostale vrstice
		var ind=3;
		var stop=false;
		while(true){
			row=tab.insertRow(ind);
			cell=row.insertCell(0);
			cell.innerHTML=(ind-1).toString(10);
			
			cell=row.insertCell(1);
			var vr1=vrednost(tab.rows[ind-2].cells[2].innerHTML);
			var vr2=vrednost(tab.rows[ind-1].cells[2].innerHTML);
			console.log(vr1+" "+vr2);
			var kolicnik=Math.floor(vr1/vr2);
			cell.innerHTML=vr1.toString(10)+" / "+vr2.toString(10)+" = "+kolicnik.toString(10);
			
			cell=row.insertCell(2);
			cell.innerHTML=vr1.toString(10)+" - "+kolicnik.toString(10)+" "+krat+" "+vr2.toString(10)+" = "+(vr1%vr2).toString(10);
			if(vr1%vr2==0)stop=true;
			
			cell=row.insertCell(3);
			vr1=vrednost(tab.rows[ind-2].cells[3].innerHTML);
			vr2=vrednost(tab.rows[ind-1].cells[3].innerHTML);
			var rez=vr1-kolicnik*vr2;
			cell.innerHTML=vr1.toString(10)+" - "+kolicnik.toString(10)+" "+krat+" "+vr2.toString(10)+" = "+rez.toString(10);

			cell=row.insertCell(4);
			vr1=vrednost(tab.rows[ind-2].cells[4].innerHTML);
			vr2=vrednost(tab.rows[ind-1].cells[4].innerHTML);
			var rez=vr1-kolicnik*vr2;
			cell.innerHTML=vr1.toString(10)+" - "+kolicnik.toString(10)+" "+krat+" "+vr2.toString(10)+" = "+rez.toString(10);
			ind++;

			if(stop==true)break;
		}
		var rezultat=document.getElementById("rezultat");
		var str="Števila si nista tuja, torej ne obstaja ....";
		var predzadnji=vrednost(tab.rows[ind-2].cells[2].innerHTML);
		console.log("pred "+predzadnji);
		if(parseInt(predzadnji, 10)==1){
			ind--;
			var str="Velja torej zveza: </br> "+vrednost(tab.rows[ind-1].cells[3].innerHTML)+" "+krat+" "+a.toString(10);
			var temp=vrednost(tab.rows[ind-1].cells[4].innerHTML);
			temp=parseInt(temp, 10);
			console.log(temp);
			if(temp>0){
				str=str+" + "+vrednost(temp.toString(10))+" "+krat+" "+b.toString(10);
			}else{
				str=str+" - "+vrednost(temp.toString(10).substring(1))+" "+krat+" "+b.toString(10);
			}
			str=str+" = 1 </br>Iz tega zakljucimo da je "+temp.toString(10);
			if(temp<0)str=str+"(= "+a+" " +temp.toString(10)+" = "+ (parseInt(a)+temp).toString(10) +") ";
			str=str+" inverzni element stevila "+b +" v modulu " + a;
		}else{

		}
		rezultat.innerHTML=str;
	});

	function vrednost(izraz){
		for(var i=0; izraz.charAt(i)!='=' && i<izraz.length; i++){}
		if(i==izraz.length)return parseInt(izraz, 10);
		i+=2;
		izraz=izraz.substring(i);
		return parseInt(izraz, 10);
	}
});