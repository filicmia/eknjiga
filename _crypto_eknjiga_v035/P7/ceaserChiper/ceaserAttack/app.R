library(shiny)

sourceDir <- getSrcDirectory(function(dummy) {dummy})
setwd(sourceDir)

ui <- fluidPage(
  tags$head(tags$script(src = "ceaserAttack.js"),
            tags$link(rel = "stylesheet", type = "text/css", href = "www/ceaserStyle.css")),
            tags$body(
    tags$div(id='ceaserAttack'))
)

server <- function(input, output, session) {
}

#v params.R
#addResourcePath('www', directoryPath = '../www-shared')
#addResourcePath('js', directoryPath = '../www-shared')
shinyApp(ui, server)