#
# Nalaganje knjižnic
#
r = getOption("repos")
r["CRAN"] = "http://cran.us.r-project.org"
options(repos = r)

libs = c("knitr")
for (lib in libs) {
  if (!is.element(lib, .packages(all.available = TRUE)) ) { install.packages(lib) }
  suppressPackageStartupMessages(suppressWarnings({ library(lib, character.only = TRUE) }))
}
rm(list = ls())
knitr::opts_knit$set(self.contained = FALSE)


# see https://bookdown.org/yihui/bookdown for the full documentation
#if (file.exists("main.Rmd")) bookdown::render_book("main.Rmd")


Sys.setlocale("LC_ALL", "sl_SI.UTF-8")


#
# Privzete nastavitve knitr generiranja vsebine
#
opts_chunk$set(echo = FALSE)
opts_chunk$set(fig.align = "center")

addResourcePath('www', directoryPath = 'www-shared')
addResourcePath('js', directoryPath = 'www-shared')
addResourcePath('protokolGameHtml', directoryPath = 'ceaserAttack')