# Gesla in Pini (Metod)

```{r filtriranjeVsebine, echo=F}
source('../P/params.R')
```

## Gesla

Geslo je beseda ali znakovni niz, ki se uporablja za uporabnikov dokaz
identitete ali dostopno soglasje za pridobitev dostopa do virov.
```{r out.width=200,echo=FALSE}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/imgs/imagesh.png"))
```

Uporaba gesel je starodavna. V preteklosti so stražarji v tiste, ki so
želeli vstopiti v varovano območje, merili z orožjem, dokler le-ti
niso povedali pravilnega gesla.
```{r out.width=130,echo=FALSE}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/imgs/handPin.jpg"))
```
```{r out.width=200,echo=FALSE}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/imgs/password_img.png"))
```

V sodobnem času so uporabniška imena in gesla običajen način
identifikacije za dostop do zaščitenih računalniških operacijskih
sistemov, mobilnih telefonov, TV sprejemnikov, ipd. Tipičen uporabnik
računalnika ima gesla za veliko različnih namenov: prijavljanje v
račune, pridobivanje e-pošte, dostop do aplikacij, baz podatkov,
omrežij, spletnih mest in celo branje jutranjega časopisa na
spletu. Kljub imenu ni potrebno, da so gesla dejanske besede; gesla,
ki niso dejanske besede, je morda težje uganiti, kar je zaželena
lastnost. Beseda geslo se včasih uporablja, kadar so skrivne informacije
samo številčne, kot je osebna identifikacijska številka (PIN), ki se
običajno uporablja za dostop do bankomatov. Gesla so na splošno dovolj
kratka, da si jih je mogoče enostavno zapomniti in vnesti, čeprav so
lahko daljša in bolj zapletena, če želi uporabnik biti varnejši.
Večina organizacij določi pravilnik o geslih, ki določa zahteve za
sestavo in uporabo gesel, ki običajno narekujejo minimalno dolžino,
zahtevane kategorije (npr. velike in male črke, številke in posebne
znake), prepovedane elemente (npr. lastno ime, datum rojstva, naslov
, telefonska številka). Nekatere vlade imajo nacionalne okvire za
preverjanje pristnosti, ki opredeljujejo zahteve za overjanje uporabnikov
vladnim službam, vključno z zahtevami za gesla.


<!-- Lahko si preveriš moč gesla spodaj. -->

<!-- #id = 'password-test-iframe'></iframe> -->


“Zxcvbn” je ocenjevalnik moči gesla, ki ga navdihujejo krekerji. S primerjanjem vzorcev in konzervativno oceno prepozna in oceni 30 000 popularnih gesel, imen in priimkov po podatkih iz popisa v ZDA, priljubljenih angleških besed iz Wikipedije in ameriške televizije in filmov, ter drugih običajnih vzorcev, kot so datumi, ponavljanja (aaa), zaporedja (abcd),... Uporabniku hitro in enostavno poda informacije o vpisanem geslu.

Podano imaš tabelo, v kateri so izračunane moči gesel po algoritmu knjižnjice “Zxcvbn”.
Oglej si gesla v tabeli in premisli, kateri bi bil najboljši in zakaj. Poleg grafičnega prikaza moči imaš tudi dodatne informacije o izbranem geslu, s katerimi lažje razumeš kriterij po čemer se določa moč gesla.





<!-- width=1000px height=300px -->
<iframe src="./P1/password_test/password_test.html" id ='password_test_iframe'></iframe>


<br>

Tu imaš možnost preverjanja poljubnih gesel. Brez skrbi, spletna stran ne beleži tvojega vnosa :)

<!-- width=1000px height=300px  -->

<iframe src="./P1/password_test_insert_style/password_test_insert_style.html" id='password_test_insert_style_iframe'></iframe>



### Izbira

Lažje je, da si lastnik zapomni geslo, kar pomeni, da bo napadalcu
težje uganiti. Vendar pa gesla, ki si jih je težko zapomniti, lahko
tudi zmanjšajo varnost sistema, ker si bodo uporabniki morali nekam
shraniti geslo, hkrati pa bodo morali pogosto ponastaviti gesla. Velja
pa tudi možnost ponovne uporabe istega gesla. Podobno vplivajo strožje
zahteve na moč gesla, npr. "Imajo mešanico velikih in malih črk in
številk" ali "se spreminjajo mesečno". Drugi trdijo, da daljša gesla
zagotavljajo večjo varnost, kot krajša gesla z najrazličnejšimi znaki.

Združevanje dveh ali več nepovezanih besed in spreminjanje nekaterih
črk v posebne znake ali številke je dober način, toda enobesednega
gesla v slovarju ni. Še en dober način je imeti osebno zasnovan
algoritem za ustvarjanje nejasnih gesel.

Zahteva uporabnikom, da uporabijo "črke in številke", bo pogosto
vodila do enostavnih zamenjav, kot so "E" → "3" in "I" → "1", to
so zamenjave, ki jih napadalci dobro poznajo. Podobno vnašanje gesla
z eno vrstico tipkovnice je navaden trik, ki ga napadalci poznajo.

Leta 2013 je Google izdal seznam najpogostejših nezanesljivih tipov
gesel, katere je preveč preprosto uganiti (zlasti po raziskovanju
posameznika na socialnih medijih):

1. Ime hišnega ljubljenčka, otroka, družinskega člana ali druge pomembne osebe
2. Datumi, obletnice in rojstni dnevi
3. Rojstni kraj
4. Ime najljubšega dopusta
5. Nekaj v zvezi z najljubšo športno ekipo
6. Beseda "geslo"



Na voljo imaš 2 različni implementaciji sestavljanja gesel. 
Prva (levo) ti poleg standardnih načinov generiranja gesel omogoča 
tudi ročni vnos besed, ki se bodo naključno uporabila v procesu sestavljanja 
priporočenih gesel. Druga implementacija (desno) pa je bolj specifična na 
izbiri znakov, dolžine ali entropije za generiranje gesel. Preizkusi obe. 
Za testiranje lahko generirana gesla skopiraš in preveriš v 
zgornji aplikaciji preverjanja moči gesel.


<iframe src="./P1/passphrase_generator_BREZ_entropije/passphrase_generator.html" id='passphrase_generator_Be_iframe'></iframe>


<iframe src="./P1/passphrase_generator_z_entropijo/passphrase_generator.html" id='passphrase_generator_Ze_iframe'></iframe>


### Dejavniki varnosti sistema gesel

Varnost sistema, zaščitenega z geslom, je odvisna od večih
dejavnikov. Celoten sistem mora biti zasnovan za zanesljivo varnost,
z zaščito pred računalniškimi virusi , napadi “človeka v
sredini” in podobno. Vprašanja v zvezi s fizično varnostjo so prav
tako zaskrbljujoča, od vohunjenja do bolj sofisticiranih fizičnih
groženj, kot so video kamere in snifferji s tipkovnico. Gesla je treba
izbrati tako, da jih napadalec težko ugiba in težko odkrije (uporaba
sistemov samodejnih napadov).

Danes je običajna praksa, da računalniški sistemi skrijejo gesla,
ko so vnesena. Namen tega ukrepa je preprečiti, da bi drugi opazovalci
prebrali geslo; vendar pa nekateri trdijo, da lahko ta praksa privede
do napak in stresa ter spodbudi uporabnike k izbiri šibkih gesel. Poleg
tega morajo imeti uporabniki možnost, da prikažejo ali skrijejo gesla,
ko jih vnašajo. Nekatera specifična vprašanja upravljanja z gesli,
ki jih je treba upoštevati pri razmišljanju in izbiri gesla so:.

1) Stopnja, pri kateri lahko napadalec poskusi uganiti gesla.
2) Hitrost, s katero lahko napadalec predloži uganjena gesla sistemu,
je ključni dejavnik pri določanju varnosti sistema. Nekateri sistemi
nalagajo časovni presledek nekaj sekund po majhnem številu (npr. treh)
neuspelih poskusih vnosa gesla. V odsotnosti drugih ranljivosti so
lahko takšni sistemi učinkovito zavarovani s sorazmerno enostavnimi
gesli. Gesla, ki se uporabljajo za ustvarjanje kriptografskih ključev
(npr. za šifriranje diska ali za zaščito brezžičnega omrežja), so
lahko prav tako izpostavljena visokim stopnjam ugibanja. Seznami skupnih
gesel so dostopni in napadi so zelo učinkoviti. Varnost je v takih
situacijah odvisna od uporabe gesel ali gesel z zadostno zahtevnostjo,
zaradi česar je napad nemočen. Nekateri sistemi, kot sta npr. PGP
in Wi-Fi WPA , uporabljajo geslo, ki zahteva intenzivno računanje,
da upočasni takšne napade.


<!-- width=500px height=500px -->



Shranjevanje gesel je prav tako pomembna lastnost moči gesla. Dobro geslo, shranjeno v navadnem tekstu je praktično neuporabno. Zato se gesla kodira z zgoščevalnimi funkcijami (podrobneje opisane v 2. poglavju). Eno izmed zgoščevalnih kodiranj je MD5.

#### MD5

MD5 (Message-Digest algorithm 5) je znan kot pogosto uporabljena kodirna funkcija s 128-bitnim izhodom. Po internetnem standardu (RFC 1321) je bil MD5 priznan in uporabljen v velikem številu aplikacij za izboljšanje varnosti. Pogosto se uporablja tudi za preverjanje datotek.

MD5 si je zamislil Ronald Rivest leta 1991 z namenom, da bi zamenjal zgodnejšo funkcijo MD4. Leta 1996 so našli napako v zasnovi algoritma MD5. Kljub temu, da to ni bila velika napaka, so strokovnjaki za kodiranje začeli priporočati uporabo drugih varnostnih algoritmov kot na primer SHA-1. Leta 2004 so odkrili dodatne pomanjkljivosti v algoritmu, nadaljnje odkrivanje napak pa je sledilo do leta 2007. Urad US-CERT je izjavil, da je »MD5 algoritem zlomljen in neprimeren za nadaljnjo uporabo«.

Za prikaz delovanja tega kodiranja vstavi poljubno geslo, dolgo največ 5 znakov za hitrejšo rešitev. Izpisal se bo geslu izračunan MD5 hash zapis, katerega bodo “delavci” (računalnik) skušali z “bruteforce” načinom vpisovanja gesel najti enak MD5 hash zapis. S številom delavcev lahko vplivaš na hitrost razbijanja gesla. Ta proces je računsko zahteven in bo tvoj računalnik zaposlil z veliko dela. Z bližnjico “Ctrl + Shift + Esc” lahko v Upravitelju opravil spremljaš uporabo procesorja. Kljub veliki zaposlitvi lahko vidiš, da je čas razbijanja zelo majhen, zato torej je kodiranje v MD5 algoritmu zelo neučinkovito.

<iframe src="./P1/md5-cracker/index.html" id='md5_cracker_iframe'></iframe>

### Omejitve števila ugibanj gesla

Alternativa omejevanju hitrosti, s katero lahko napadalec ugiba o geslu,
je omejiti skupno število ugibanj, ki jih je mogoče narediti. Geslo
je mogoče onemogočiti, kar zahteva ponastavitev, po majhnem številu
zaporednih slabih ugibanj (recimo 5); in uporabnik bo morda moral
spremeniti geslo po večjem kumulativnem številu slabih ugibanj
(npr. 30).S tem prepreči, da bi napadalec naredil poljubno veliko slabih
ugibanj. Napadalci lahko nasprotno uporabijo znanje o tem (ublažitev
izvajati DDoS) proti uporabniku z namernim zaklepanjem uporabnika
iz lastne naprave; to zavrnitev storitve lahko odpre napadalcu druge
možnosti za manipulacijo situacije v njihovo korist preko socialnega
inženiringa.

Viri: Password - [Wikipedia](https://en.wikipedia.org/wiki/Password)


## PINI

PIN (Personal Identification Number) ~ (osebna identifikacijska številka)
- je numerično ali alfanumerično geslo, ki se uporablja v postopku
avtentikacije uporabnika, ki dostopa do sistema.

Osebna identifikacijska številka je bila ključna pri izmenjavi zasebnih
podatkov med različnimi centri za obdelavo podatkov v računalniških
omrežjih za finančne institucije, vlade in podjetja. Bolj splošno
znana uporaba PIN številke je predvsem pri preverjanju pristnosti
imetnika bančne kartice.

### Zgodovina

PIN številka se je začela uporabljati ob prihodu bankomatov leta
1967, ko so banke iznašle učinkovitejši način dviga denarja svojih
strank. Prvi sistem bankomatov je bil sistem Barclays v Londonu leta
1967. Sprejemal je čeke s strojno berljivo enkripcijo, uporabnik pa je za
odobritev transakcije moral vpisati PIN številko. Podjetje Lloyds Bank je
leta 1972 izdalo prvo bančno kartico, ki je uporabljala PIN za varnost.

### Uporaba

V okviru finančne transakcije je ponavadi tako zasebna koda PIN, kot
tudi javni identifikator uporabnika potrebna za overjanje uporabnika
v sistemu. Po prejemu obeh sistem pošjle kodo PIN na podlagi ID-ja
uporabnika in ga primerja z vnešenim. Uporabniku je dovoljen dostop le,
če se vneseno število ujema s številko shranjeno v sistemu. Zato,
kljub imenu, PIN ne identificira uporabnika.

### Dolžina PIN

Mednarodni standard za upravljanje s PIN-om za finančne storitve,
ISO 9564-1, dovoljuje uporabo PIN-ov od štirih do dvanajstih številk,
vendar priporoča, da izdajatelj kartice zaradi razlogov uporabnosti ne
dodeli PIN-a, daljšega od šestih mest.
Izumitelj bankomata, John Shepherd-Barron, si je sprva zamislil
šestmestno številčno kodo, njegova žena pa si je lahko zapomnila le
štirimestno številko, ki je postala najpogosteje uporabljena dolžina,
čeprav banke v Švici in številnih drugih državah uporabljajo
šestmestno kodo PIN.


### Preverjanje PIN-a

Naravna koda PIN se ustvari s šifriranjem primarne številke računa
(PAN) s pomočjo šifrirnega ključa, izdelanega posebej za ta namen. Ta
tipka se včasih imenuje ključ generacije PIN (PGK). Ta koda PIN je
neposredno povezana s primarno številko računa. Za potrditev PIN-a
banka izdajatelju z zgornjo metodo regenerira PIN in to primerja z
vnešeno PIN kodo.
Naravni PIN-i se ne morejo izbrati, ker so izpeljani iz PAN-a. Če je
kartica ponovno izdana z novo PAN, mora biti ustvarjena nova koda PIN.
Naravni PIN-i omogočajo bankam, da ob izdaji PIN-a izdajajo opomnike PIN.

Naravni PIN + offset metoda 

Za omogočanje uporabniško izbranih kod PIN je možno shraniti
vrednost odmika PIN. Odmik se ugotovi z odštevanjem naravnega PIN-a od
uporabniško izbranega PIN-a po modulu 10. Na primer, če je naravni
PIN 1234 in uporabnik želi imeti PIN 2345, je odmik 1111. Odmik se
lahko shrani bodisi na podatkih o poti, bodisi v podatkovni bazi pri
izdajatelju kartice. Za potrditev kode PIN banka izdajatelj izračuna
naravno PIN kot v zgornji metodi, nato doda odmik in dobljeno vrednost
primerja z vnešeno PIN kodo.

### Metoda VISA

Metoda VISA ustvari vrednost preverjanja PIN (PVV). Podobno kot vrednost
odmika se lahko shrani na podatke o skladbi kartice ali v podatkovni
bazi pri izdajatelju kartice. To se imenuje referenčni PVV.

Metoda VISA popelje najbolj desno petnajst številk PAN, razen vrednosti
kontrolne vsote, indeksa ključa za preverjanje PIN (PVKI, izbranega
od enega do šestih) in zahtevane vrednosti PIN za 64-bitno številko,
PVKI izbere ključ za preverjanje (PVK) , 128 bitov) za šifriranje te
številke. Iz te šifrirane vrednosti se najde PVV.

Za potrditev kode PIN banka izdajatelj izračuna vrednost PVV iz vnesene
PIN in PAN in primerja to vrednost z referenčnim PVV. Če se referenčni
PVV in izračunani PVV ujemata, je bil vnesen pravilen PIN.
Za razliko od IBM-ove metode metoda VISA ne prinaša PIN-a. Vrednost
PVV se uporablja za potrditev PIN-a, ki je bil vnesen na terminalu,
prav tako je bil uporabljen za generiranje referenčnega PVV-ja. PIN,
ki se uporablja za ustvarjanje PVV, se lahko naključno generira ali
izbere uporabnik ali celo izvede z uporabo metode IBM.


### Varnost PIN-a

Štirimestna PIN gesla imajo razpon od 0000 do 9999, kar je skupno 10 000
različnih kombinacij. Navadno sistem dopušča spreminjanje PIN-a, a je
priporočeno, da se ne uporablja rojstnih datumov, številk registrskih
tablic, ponavljajočih števil (1111, 2222, 3333, ....), zaporedij (1234,
2345, …), števil, ki se začnejo z ničlami.

Številni sistemi za preverjanje PIN-a omogočajo tri poskuse, s čimer
daje tatu kartice domnevno 0.3% verjetnost, da bo uganil pravilen PIN,
preden je kartica blokirana.To velja le, če so vsi PIN-i enako verjetni
in napadalec nima na voljo dodatnih informacij, kar pa ni bilo v primeru
številnih algoritmov za generiranje in preverjanje PIN, ki so jih v
preteklosti uporabljale finančne institucije in proizvajalci bankomatov.

Opravljene so bile raziskave o običajno uporabljenih kodah PIN. Posledica
tega je, da brez previdnosti velik del uporabnikov meni, da je njihov PIN
ranljiv. "Oboroženi s samo štirimi možnostmi lahko hekerji razbijejo
20% vseh PIN-ov. Dovolite jim ne več kot petnajst številk in lahko
uporabijo račune več kot četrtine imetnikov kartic."

Težava lahko ugibljivih PIN-ov se poveča, ko so stranke prisiljene
uporabljati dodatne številke, ki povečajo možnost razbitja na
okoli 25%, pri petnajstih številkah celo na več kot 30% (brez
upoštevanja 7-mestnih števil telefonskih številk). Približno
polovica vseh 9-mestnih PIN-ov ima dobre možnosti razbitja že po 35%
uporabi zaporedja 123456789, za preostalih 64% pa je dobra možnost,
da uporabljajo svoj EMŠO.

### Viri:
Personal identification number 
- [Wikipedia](https://en.wikipedia.org/wiki/Personal_identification_number)



<script>
  iFrameResize({ log: true, warningTimeout: 10000}, '#password_test_iframe, #password_test_insert_style_iframe, #md5_cracker_iframe, #passphrase_generator_Be_iframe, #passphrase_generator_Ze_iframe')
</script>
