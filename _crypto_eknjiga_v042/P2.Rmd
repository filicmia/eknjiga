# Kratice (BORIS)

```{r filtriranjeVsebine, echo=F}
source('params.R')
```

<!-- =========================================================== -->
## Uvod (TomazS/AJ)

<!-- --> 

**Krajšanje** oz. **zgoščevanje** (ang. hash) je postopek pri katerem 
poljubnemu nizu znakov dodelimo neko (kratko) oznako, ki ji bomo 
rekli **kratica** oz. **zgostitev**
(tudi *izvleček* ali *digitalni prstni odtis*).
<!-- iz poljubnega niza znakov. s določeno funkcijo. -->
<!-- Primer1: -->
Npr. do tega pride običajno na šoli, ko učence na začetku šolskega leta 
razporedimo v razrede, npr. 150 učencev v 4 razrede A-D (37+38+37+38). 
Sedaj je enostavno poklicati četrtino učencev (A-jevci pridite na
štart!).
<!-- % Primer2: -->
Recimo, da imamo v skupini 5 učencev: Janez Korenina, Zala Novak, 
Miha Copatar, Zvonko Nagode in Tone Rastlina.
<!-- -->
Vzamimo, da tokrat postopek krajšanja
<!-- % funkcijo \(F(x)\), ki -->
za vhod prejme učenca in ga zapiše kot niz 
začetnic njegovega imena in priimka. 
Dobimo torej vrednosti, kot so JK, ZN, itd.

**Slika 1** Začetnice učencev

```{r out.width=550, out.height=1000, echo=FALSE, }
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/hash_ucenci.png"))
```

<!-- Kot veliko protokolov in nasplošno tehnik, ki jih uporabljamo v
računalništvu, tudi zgoščevalne funkcije izhajajo iz našega
vsakodnevnega življenja. -->

```{r out.width='40%',echo=FALSE}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/waiter_writes_order.jpg"))
```

Oglejmo si še en primer. Zjutraj greš z družino na zajtrk. 
Vsak član družine naroči natakarju, kaj bo jedel in pil, 
na koncu pa pridni natakar, ki hoče biti siguren, 
da se ni zmotil pri zapisovanju naročila, vpraša:
"Torej skupaj boste 2 kavi in 3 rogljičke, je tako?"
<!-- -->
Pravkar opisan postopek, ki ga je uporabil natakar, je res zelo primitiven
primer zgoščevanja. Kaj je v bistvu naredil? 
Vzel je dolgo sporočilo, kjer je vsaki član družine naročil nekaj zase 
in ga predelal v krajšega, ki služi temu, da preveri, ali je med prenosom
informacije prišlo do napake.

Naštejmo nekaj primerov, kjer se krajšanje oz. zgoščevanje uporablja:

1. **Shranjevanje podatkov v tabeli.** 
Namesto, da iščemo nek zapis po tabeli, 
lahko izračunamo zgostitev in takoj skočimo na mesto v tabeli,
na katerega se sklicuje ta zgostitev. 
<!-- SAS: Bolj kot lastnost enosmernosti --> 
Tukaj je pomembno, da pri zgoščevanju prihaja do čim manj sovpadanj,
ki jih v kriptografji imenujemo **trčenja** (ang. collision).

2. **Preverjanje enakosti podatkov.** 
Tu želimo, da tudi najmanjša sprememba v vhodnem nizu vrne čim
bolj različno zgoščeno vrednost. Krajšanje se uporablja predvsem pri 
preverjanju pravilnosti oz. enakosti podatkov. 
Če imamo na voljo zaupanja vredno zgostitev nekih pomembnih 
pridobljenih podatkov, ki bi jih radi zaupali, 
lahko zgostitev izračunamo še sami in zgostitvi primerjamo. 
Če sta enaki, so podatki najverjetneje identični. 
S tem pristopom se lahko zelo pohitri obdelavo podatkov.
Uporablja se npr. pri prenosu večjih datotek.

3. **Hranjenje gesel.** 
Namesto, da bi hranili gesla v svoji prvotni obliki, 
shranimo v bazo namesto tega zgoščene vrednosti gesel. Tako zagotovimo,
da gesla uporabnikov ostanejo neznana, tudi če napadalec vdre v naš sistem. 
Magnetni trak na bančni kartici npr. vsebuje zgostitev našega PINa.
<!-- decentralizacija -->
Vendar se tu zgodba ne konča. Zaradi takšnega shranjevanje gesel so nastale 
t.i. *mavrične tabele*, ki vsebujejo slovar gesel, katere so jih 
nepridipravi odkrili do sedaj v različnih aplikacijah, in njihovih zgoščenih
vrednosti. To pa lahko rešimo na enostaven način, in sicer tako, 
da uporabnikovemu geslu dodamo t.i. *sol*. 
To je naključen niz, ki ga potem skupaj z geslom zgostimo in 
shranimo v podatkovno bazo. Ta naključnost zagotovi, da v mavričnih tabelah,
tudi če obstaja zapis za geslo, ne najdemo takšnega zapisa.


## Primeri (Boris)

Take funkcije so vsepovsod okoli nas, poglejmo si nekaj primerov:


### ISBN 

<iframe src="./P2/emso_isbn_plac/isbn.html" id = 'isbn-iframe'></iframe>
<div class='clear'></div>


ISBN (International Standart Book Number) je desetmestna (ali
trinajstmestna) številka, ki enolično definira neko knjigo. Vsaka
knjiga tiskana po letu 1970 ima svojo ISBN številko. Prvih 9 oz 12
cifer označuje naslov knjige, avtorja, založbo in jezik, v katerem je
napisana, zadnja cifra pa je zgoščena vrednost vodilnih cifer. Zato,
da bi izračunali to zgoščeno vrednost, vsako cifro pomnožimo z
ustreznim faktorjem, vsoto pa delimo s številom 11.

Natančen postopek samega izračuna lahko preveriš
[tukaj](https://www.geeksforgeeks.org/program-check-isbn/), 
sam pa lahko vzameš knjigo, ki ti je najbolj blizu, pa poskusiš, 
če ta postopek deluje ali ne!


### EMŠO 

<iframe src="./P2/emso_isbn_plac/emso.html" id = 'emso-iframe'></iframe>

Na hrbtni strani našega osebnega dokumenta je zapisana številka EMŠO
(Enotna Matična Številka Občana). Njena sestava je precej preprosta:
datum rojstva določa prvih sedem cifer, osma in deveta cifra je pri
nas vedno 50 (kar označuje rojstvo v Sloveniji), sledijo še tri
cifre, ki določajo zaporedno številko rojstva (pri tem se razlikuje
med rojstvi otrok moškega in ženskega spola). Sledi še 
[zadnja cifra](https://sl.wikipedia.org/wiki/Enotna_mati%C4%8Dna_%C5%A1tevilka_ob%C4%8Dana),
ki je v bistvu zgoščena vrednost vseh ostalih.
<!-- -->
Tukaj lahko poskusiš generirati svojo EMŠO številko, pa tudi tisto
izmišljenih oseb. Poskusi pa odgovoriti na naslednja vprašanja:
<!-- SAS2MET itemize-->

  1) Kako se spremeni EMŠO, če spremeniš ime?

  2) Ali lahko imata dva občana isto EMŠO številko?

  3) Kaj lahko sklepaš o imetniku dane EMŠO številke, če o njem veš
  samo EMŠO?

### Številke plačilnih kartic 

<iframe src="./P2/emso_isbn_plac/plac_kart.html" id = 'plac_kart-iframe'></iframe>

Tudi zadnja števka naših plačilnih kartic je zgoščena funkcija prvih
petnajstih. Izračunamo jo s tako imenovanim Luthovim algoritmom, ki
ima podoben postopek kot zgornja dva primera: ustrezno pomnožimo cifre
kartice, jih seštejemo, na koncu pa izvedemo še neko celoštevilsko
deljenje.
Na spletu najdemo več takih aplikacij, ki ponaredijo izmišljene kartice
(v bistvu izberejo skoraj naključno prvih 15 cifer in izračunajo
na koncu še kontrolno vsoto).Postopek, s katerim bi lahko trivialno
preverili, ali so take kartice resnične ali ne, ne obstaja; to pa
omogoča razna protizakonita obnašanja (na primer lahko vsaki mesec
[generiramo izmišljeno kartico](https://www.getcreditcardnumbers.com/)
in tako koristimo "brezplačni mesec" na raznih spletnih straneh kot
"Netflix". Ponovimo pa da je tako obnašanje protizakonito). Nekatere
spletne strani odvzamejo iz računa 1 cent in ga takoj vrnejo z namenom
preverjanja pristnosti kartice. Proti takemu sistemu za preverjanje
kartic seveda spodnji postopek ni odporen.

Tudi s spodnjim pripomočkom lahko generiramo izmišljene kartice:
vnesi prvih 15 cifer, dobil pa boš veljavno številko kartice!

#### Zaključek {-}

Iz zgoraj navedenih primerov bi moralo biti jasno, da osnovni podatki
enolično določajo zgoščeno vrednost, obratno pa ne velja. Na primer,
če se navežemo na zgornji primer ISBN-ja: obstaja namreč več milijonov
knjig, vsaka ima enolično določeno ISBN kodo, z druge strani pa obstaja
samo 11 zgoščenih vrednosti za podane osnovne podatke.

Podoben razmislek lahko uporabimo v računalništvu, tudi ko nas vsebina
in nasploh pomen podatkov, ki prenašamo, sploh ne zanima. Naprimer,
da želimo sporočilo dolgo 20 bitov zgostiti v vrednost dolgo 5
bitov. V takem primeru, možnih 1048576 ($2^{20}$) vhodnih
vrednosti bo zgoščevalna funkcija razdelila v skupine, ki bodo imele
isto zgoščeno vrednost; v povprečju bodo te skupine velike 32768
($2^{15}$), osnovna želja pa je seveda, da so skupine čim
bolj enakomerne. 
<!-- Takemu dogodku, kjer se dva ali več vhodnih podatkov
preslikata v isto zgoščeno vrednost, pravimo **trčenje**
(ang. collision), temu --> 
Trčenjem pa se seveda želimo izogniti.


## Lastnosti 

<!-- SASA -->
Zgoščevanje pa je en izmed temeljnih gradnikov tudi v kriptografiji. 
**Kriptografska zgoščevalna funkcija** vzame nek vhodni niz 
in učinkovito (beri hitro) generira drugačen niz, 
za njo pa si želimo naslednje lastnosti:

1. **ponovljivost** - funkcija ni odvisna od naključja, 
	za določen vhod vedno vrne enak izhod 
	(strokovnjaki prvijo, da gre za *deterministično* funkcijo),

2. **krajšanje** - vhodni niz poljubne dolžine pretvori 
	v niz točno določene dolžine, <!-- padding? -->

3. **razpršena podobnost** - majhna sprememba v sporočilu 
	povzroči dobro zaznavno spremembo v zgostitvi 
	(tj. stara in nova zgostitev ne izgledata podobno). 
   (V praksi si želimo, da bi sprememba enega samega bita na vhodu 
	spremenila približno polovico izhoda.))

4. **enakomernost** - 
   <!-- **uniformnost** SAS2TOM lahko najdes še kakšen drug izraz? -->
   torej za dva različna vhoda ne dobimo enakega izhoda
   (to je v praksi zelo težko doseči in so ponavadi zadovoljive 
   zgoščevalne funkcije, kjer je verjetnost sovpadanja dovolj majhna),
   <!-- SAS2TOM S tem so problemi!!!!!
	Dirichletov princip: Če damo $n+1$ žogic v $n$ predalov, 
	potem obstaja predal, v katerem sta vsaj dve žogici.
	% 2. način: $n+1$ žogic ne moremo dati v $n$ predalov, 
	% brez, da bi obstajal predal, v katerem sta dve žogici. -->

5. **enosmernost** - iz izhoda v doglednem času ne moremo izračunati vhoda.


Bi lahko rekli zgoraj vpeljani funkciji na učencih, ki smo jih 
priredili začetnice imen in priimkov, zgoščevalna funkcija?
<!-- -->
Pravilo 1 je izpolnjeno, saj bo Janez Korenina vsakič zgoščen v JK.
<!-- --> 
Pravilo 2 ni izpolnjeno, saj Zala Novak in Zvonko Nagode tvorita enak izhod,
tj. ZN. Verjetnost, da imata izmed petih učencev dva enake začetnice, je
sicer majhna (1.6%),
<!-- SAS2TOM če privzamemo enakomerno porazdelitev, 
sicer pa https://www.stat.si/ImenaRojstva/sl/FirstNames/ExpandNames
in grob izracun -->
a v svetu zgoščevalnih funkcij je to ogromno. 
Kot primer lahko vzamemo tudi scenarij, kjer je v razredu 20 učencev. 
Takrat je verjetnost, da imata dva učenca enak izhod že 26.5\%
(paradoks rojstnih dnevov).
<!-- -->
Pravilo 3 je izpolnjeno, saj je izhod vedno dolžine 2 
(ignorirajmo za zdaj primere, ko ima učenec več kot eno ime ali priimek 
ali pa privzemimo, da smo vzeli samo prvo ime in prvi priimek).
<!-- -->
Pravilo 4 ni izpolnjeno, saj iz izhoda lahko vidimo, kakšen je bil vhod. 
Npr. ZN ne more biti nič drugega kot Zala Novak, saj je edina z
začetnicami Z in N. Tudi v primeru, ko obstajata dva ali več učencev
z istimi začetnicami, lahko omejimo možen nabor imen, ki jim pripada.
<!-- SAS2TOM statistika za slovenska imena:
https://www.stat.si/ImenaRojstva/sl/FirstNames/ExpandNames -->

### Lastnosti (NEW) {-}

Osnovne lastnosti, ki želimo, da jih ima zgoščevalna funkcija, so:

1) Enosmernost (one-way): če imamo podano zgoščeno vrednost h, želimo, 
da je čim težje dobiti neko originalno sporočilo m (le-temu pravimo 
čistopis), ki ga je generiralo. Dejstvo, da je h zgoščena vrednost 
čistopisa m, zgoščenega s funkcijo H, označujemo h=H(m);

2)	Enostavnost: želimo, da je izračun zgoščene vrednosti čim bolj enostaven;

3)	Odpornost na trčenja (collision resistant): želimo, da je čim težje 
dobiti dva čistopisa m<sub>1</sub> in m<sub>2</sub>, ki se preslikata 
v isto zgoščeno vrednost, tj H(m<sub>1</sub>)=H(m<sub>2</sub>).

<div class='todo'>
Informally, we say that the hash function H is collision resistant if
finding a collision for H is diffcult. Since the digest space T is much
smaller than M, we know that many such collisions exist. Nevertheless,
if H is collision resistant, actually finding a pair m0,m1 that collide
should be diffcult.

At first glance, it may seem that collision resistant functions cannot
exist. The problem is this: since |M| > |T| there must exist inputs
$m_{0}$ and $m_{1}$ in M that collide, namely $H(m_{0}) = H(m_{1})$. 
An adversary A that simply prints $m_0$ and $m_1$ and exits is an effcient
adversary that breaks the collision resistance of $H$. We may not be
able to write the explicit program code for $A$ (since we do not know 
$m_0$, $m_1$), but this $A$ certainly exists. 
Consequently, for any hash function $H$ defined over $(M,T)$ 
there exists some effcient adversary $A_{H}$ that
breaks the collision resistance of $H$.

The way out of this is that, formally speaking, our hash functions are
parameterized by a system parameter: each choice of a system parameter
describes a different function H, and so we cannot simply “hardwire”
a fixed collision into an adversary: an effective adversary must be able
to effciently compute a collision as a function of the system parameter.
</div>

```{r out.width='45%'}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/Okrajšave_2.2.1_Rojstni_dnevi_clear.png"))
```

### Razvoj kriptografskih zgoščevalnih funkcij

Kot smo videli, funkcija z začetnicami ni za resno uporabo, 
zato si moramo izmisliti boljšo zgoščevalno funkcijo. 
<!-- SAS2TOM Kripto vs. drugi nameni (delitev po razredih) -->
Na srečo pa lahko izberemo že kakšno izmed obstoječih, 
kot so SHA-256, SHA-512, Bcrypt in mnoge druge.
<!-- -->
Leta 1993 je Nacionalni inštitut za standarde in tehnologijo (NIST) 
objavil zgoščevalno funkcijo SHA-0 zasnovano na algoritmih MD4 in MD5.
Kmalu jo je zamenjala njena izboljšana različica SHA-1, ki jo je 
predlagala Nacionalna varnostna agencija (NSA) 
(dodali so zamik za 1 na vsakem krogu). 
Dolžina vhodnega sporočila pri SHA-1 je omejena na $2^{64}$
bitov in kot rezultat vrne zgostitev dolžine 160 bitov. Leta 2005 je
bil na zgoščevalno funkcijo SHA-1 odkrit napad, s katerim je možno
v manj kot $2^{63}$ korakih poiskati različni vhodni sporočili z enako
vrednostjo zgostitve. Zato je NIST leta 2008 objavil nov standard za
zgoščevalne funkcije FIPS 180-3 [26]. Ta definira algoritme zasnovane
na SHA algoritmu, ki vračajo zgostitve velikosti od 224 do 512 bitov
in jih imenujemo algoritmi SHA-2, glej Pfleeger et al. [15, Sect. 12.4].


<!-- Iz Klavdijine diplome za primerjavo: \
Eden izmed temeljnih gradnikov moderne kriptografije so kriptografske
zgoščevalne funkcije. To so funkcije, ki poljubno dolgo sporočilo
pretvorijo v zaporedje bitov določene dolžine, imenovano *zgostitev* (ali
*izvleček*, t.i. *digitalni prstni odtis*), glej Stinson [31, Sect. 5.1]. 
V kombinaciji z asimetričnimi algoritmi se najpogosteje uporabljajo za
digitalno podpisovanje. Moderne zgoščevalne funkcije
morajo biti deterministične, računsko učinkovite in morajo izpolnjevati
naslednja kriterija:\
- so enosmerne, kar pomeni, da iz vhoda izračunajo zgostitev, iz zgostitve
pa ni mogoče učinkovito izračunati vhoda v funkcijo, \
- trčenja so težko izračunljiva, tj. nemogoče je učinkovito izračunati
različni zaporedji nizov, katerih zgostitvi zgoščevalne funkcije sta enaki.\
Najpogosteje uporabljene zgoščevalne funkcije pripadajo družini SHA
(Secure Hash Algorithm), ki jih bomo za potrebe tega dela tudi podrobneje 
opisali. Te funkcije imajo dodatno še to praktično lastnost, da majhna
sprememba v sporočilu povzroči dobro zaznavno spremembo v zgostitvi 
(tj. stara in nova zgostitev ne izgledata podobno). -->

<!-- =========================================================== -->

<!-- (Boris) 

Bolj formalno povedano:

   * Zgoščevalna funkcija  $H: M \to T$  je katerakoli funkcija, 
ki jo lahko uporabimo, z namenom da vhodni podatek poljubne velikosti 
preslikamo v nek podatek vnaprej definirane velikosti iz $T$ (angl. digest).
-->

<!-- =========================================================== -->
## Paradoks rojstnih dnevov (AJ) 

To seveda ni paradoks, a vseeno ponavadi zavede naš občutek.
Poglejmo si konkreten primer.
Na nogometni tekmi sta na igrišču dve enajsterici in sodnik, 
skupaj **23 oseb**. Kakšna je verjetnost, da imata **dve osebi** 
isti rojstni dan? Ali je ta verjetnost lahko večja od **$0.5$**? 
Čeprav je relativno 23 majhno število (glede na število dni v letu), 
je med 23 osebami 253 različnih parov.
To število je veliko bolj povezano z iskano verjetnostjo.
<!-- -->
Testirajte to na zabavah z več kot 23 osebami.
Organizirajte stave in dolgoročno boste gotovo na boljšem,  
na velikih zabavah pa boste zlahka zmagovali.

**Slika 2: Nogometna tekma. Kakšno naključje! Mar res?**

```{r out.width=400, out.height=500,echo=FALSE}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/coincidence.png"))
```

<blockquote class='quote'>
**V poljubni skupini 23-ih ljudi je verjetnost, 
da imata vsaj dva skupni rojstni dan $> 1/2$.**
</blockquote> <br>

V nemškem nogometnem prvenstvu so preverili zgornjo trditev tudi empirično,
mi pa jo uteljimo z neposrednim računanjem verjetnosti.
Ko vstopi v sobo $k$-ta oseba, je verjetnost, da 
je vseh $k$ rojstnih dnevov različnih enaka:

$$
{365 \over 365} \times               {364     \over 365} \times
{363 \over 365} \times \cdots \times {365-k+1 \over 365} 
= \left\{ \begin{array}{ll}
0.493,\ & \mbox{če je $k\!=\!22$} \\
0.507,\ & \mbox{če je $k\!=\!23$}
\end{array} \right.
$$

**Slika 3: Graf za verjetnost trčenja**

```{r out.width=350, out.height=5000,echo=FALSE}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/coin_graph.jpg"))
```

Ocenimo še splošno verjetnost.
<!-- -->
Mečemo $k$ žogic v $n$ posod in gledamo, ali sta v kakšni posodi
vsaj dve žogici.
<!-- -->
Poiščimo spodnjo mejo za verjetnost zgoraj opisanega dogodka.

Privzeli bomo, da je $|h^{-1}(x)|\approx m/n$, kjer je $n=|Z|$ in $m=|X|$ 
(v primeru, da velikosti praslik niso enake se verjetnost le še poveča).
$$
\biggl(1-{1 \over n}\biggr)
\biggl(1-{2 \over n}\biggr)
\cdots
\biggl(1-{k-1 \over n}\biggr)
=\prod_{i=1}^{k-1} \biggl(1-{i \over n}\biggr)
$$
Iz razvoja v Taylorjevo vrsto dobimo
$$
e^{-x} = 1-x+ {x^2 \over 2!} - {x^3 \over 3!} + \cdots,
$$
Od tod ocenimo $1-x \approx e^{-x}$ in dobimo
$$
\prod_{i=1}^{k-1} \biggl(1-{i \over n}\biggr)
\ \ \approx\ \  
\prod_{i=1}^{k-1} e^{-i\over n}\ \ =\ \ e^{-k(k-1)\over 2n}.
$$
Torej je verjetnost trčenja $1\ -\ e^{-k(k-1)/(2n)}$.
Potem velja $e^{-k(k-1)/(2n)}  \approx 1-\varepsilon$
oziroma $-k(k-1)/(2n) \approx \log(1-\varepsilon)$
ali $k^2-k \approx 2n \log (1-\varepsilon)^{-1}$
in če ignoriramo linearen člen $-k$, dobimo končno 
$$
k \approx \sqrt{ 2n \log (1-\varepsilon)^{-1}}.
$$
Za $\varepsilon=0.5$ je **$k \approx 1.17\sqrt{n}$**, kar pomeni, 
da, če zgostimo nekaj več kot $\sqrt{n}$ elementov,
je bolj verjetno, da pride do trčenja kot da ne pride do trčenja.

<blockquote class='quote'>
V splošnem si zapomnimo, da je $k$ proporcionalen z $\sqrt{n}$.
</blockquote>

Tehniki iskanja trčenja rečemo v kriptografiji
*napad s paradoksom rojstnih dnevov* (angl. Birthday Attack).
Le-ta nam določi spodnjo mejo za velikost zaloge vrednosti 
zgoščevalne funkcije.
<!-- =========================================================== -->

<!-- SASA commented:
Here we show the way to attack the weak collision resistance
property called `Birthday paradox`.

It’s a simple principle that comes from a simple problem. 
(SAS: simple simple??)
How many people have to be in a room, 
to have a probability bigger than 0.5 that two persons have been 
born the same day? (SAS: you do not really mean this!!!!!)<br />

The calculation, after some work 
(SAS: there is no point to write this way!!!), 
gives us the formula:
$m_{p} = \sqrt{2n \ln{2}} \approx 1.17\sqrt{n}$, 
where $n$ is the number of results in the image 
(for the birthday paradox, $n$ would be 365). 

And $m_{p}$ would be the number of persons we need. 
This gives us $m_{p} = 22.3$. 
So, if we have 23 people in a room,
there is a good chance (SAS: how good???)
that we find two born the same day.
-->

#### Interakcija (Denis) {-}

The following is the code snippet to calculate the same thing, 
variating the probability of the collision, $p$.
The code is written in python 3.
```{.py}
# Approximate number 
# of people in Birthday Paradox problem 
import math 
  
# Returns approximate number of  
# people for a given probability 
def find( p ): 
    return math.ceil(math.sqrt(2 * 365 *
                     math.log(1/(1-p)))); 
  
# Call 
print(find(0.70)) 
```

For cryptography, we can do the calculation with $m_p \approx n$, 
and replace $n$ for the number of possible results of the digest. 
If the digest has b bits, then $n = 2^b$. 
And, to have a good probability (more than $\frac{1}{2}$) of having 
the same digest with two different messages, we would need to try
$m_{\frac{1}{2}} = \sqrt{2n \ln{2}} \approx 1.17 * 2^{\frac{b}{2}}$ times.

Now run the snippet below, multiple times to recheck the formula 
$$
m_{p} = \sqrt{2 \ln{2} n} \approx 1.17\sqrt{n}.
$$

<stari-rojstni-dnevi>

*How to run the snippet in a right manner?* <br />
For i.e. birthday paradox check do the following:

1. Set $n = 365$, the number of different days that we have,
2. Calculate $m_p = 22.3$,
3. Run the snippet for example $k = 10$ times, with number of persons equal to 23,
4. Calculate the number of runs in which you get at least 2 dates that have the same day-month. It should be close to  $k/2$.

<iframe src="./P2/rojstniDnevi/aplikacija2_RojstniDnevi.html" 
id = 'aplikacija2_RojstniDnevi-iframe'></iframe>
<iframe src="./P2/rojstniDnevi/aplikacija1_RojstniDnevi.html" 
id = 'aplikacija1_RojstniDnevi-iframe'></iframe>

Iz zgoraj opisanih lastnosti sledi, da se mora zgoščevalna funkcija 
razpršiti čim bolj enakomerno po prostoru, ki ga ima na razpolago. 
Zaradi teh lastnosti imajo zgoščevalne funkcije veliko možnih primerov 
uporabe. V računalništvu jih najdemo, med drugim, v podatkovnih bazah, 
podatkovnih strukturah in nenazadnje v računalniških komunikacijah, kjer 
jih uporabljamo zato, da bi imeli čim bolj varno in zanesljivo komunikacijo,
v nadaljevanju pa se bomo omejili samo na ta zadnji primer uporabe.


### Zgoščevalne funkcije v računalniških komunikacijah (Denis)

Ker smo na spletu podvrženi [raznim
nevarnostim](https://www.spyzie.com/monitor/top-5-dangers-of-using-the-internet.html),
uporabljamo razne protokole (glej RSA, SSL, DES, ...), 
zato da bi zagotovili:

1) Integriteto (ang. integrity): želimo preprečiti možnost, 
da nekdo spremeni podatke, ki si jih dve osebi izmenjujeta, 
ne da bi le-ti zaznali spremembe v sporočilu;

2) Avtentikacijo (ang. authentication): želimo biti zmožni 
dokazati identiteto svojega sogovornika. Ta lastnost je še 
posebej pomembna, ko preko spleta pošiljamo osebne podatke;

3) Onemogočanje zanikanja (ang. non repudiation): želimo dokazati, 
da je neka oseba res bila tista, ki je poslala določeno sporočilo.

V osnovi so v dobri meri te lastnosti zagotovljene z uporabo
zgoščevalnih funkcij in enkripcije čistopisa. Naprimer, ko želimo
sporočilo m poslati preko interneta: sporočilu dodamo njegovo
zgoščeno vrednost H(m), prejemnik sporočila pa bo prejel sporočilo
m', ki je lahko enako sporočilu, ki smo poslali, z druge strani pa
se je lahko nekdo vrinil v komunikacijo in spremenil čistopis m in ga
nadomestil z m'. Prejemnik bo izračunal zgoščeno vrednost H(m') in
jo primerjal z H(m), ki je prejel. V primeru, da sta enaki, bo vedel,
da med komunikacijo sporočilo m ni bilo spremenjeno. Če nista, bo
vedel, da je nekdo sporočilo spremenil. Seveda je dejanski postopek
bistveno kompleksnejši, saj v primeru, da napadalec izve za zgoščevalno
funkcijo, ki jo sogovornika uporabljata, lahko nadomesti tudi zgoščeno
vrednost. Zato, da se to ne bi zgodilo, se poleg čistopisa zgosti še
neki tajni podatek, ki ga poznata samo pošiljatelj in prejemnik.

### Hashiranje gesel (Denis)

Drugo področje, kjer uporabljamo zgoščevalne funkcije za 
zagotavljanje varnosti, je v hranjenju gesel v serverjih. 

```{r out.width='45%'}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/Okrajšave_2.2.3_Hashiranje_gesel_clear.png"))
```

Osnovna ideja je, da v primeru, ko nekdo uspe iz kakršnegakoli razloga
dobiti dostop do računalnika, kjer so shranjena gesla, le-ta ne uspe
izluščiti gesel uporabnikov. Denimo konkreten primer: Facebook
ima več kot dve milijardi uporabnikov. Vsak,ki želi vstopiti v
svoj račun, mora dokazati svojo identiteto s tem, da vnese svoj
e-mail in neko geslo. E-mail je v večini primerov javen, z druge
strani pa je geslo nekaj, kar pozna samo uporabnik. Le-ta seveda
hoče, da ostane njegovo geslo tajno (še posebej če rabi isto
geslo za več spletnih strani). Recimo pa primer, da bi Facebook
hranil gesla v čistopisu (npr geslo uporabnika je '12345' in je
na serverju geslo shranjeno kot '12345'). Ko neki uporabnik želi
vstopiti, sistem preveri, ali je vnešeno geslo enako tistemu, ki
je hranjeno v serverju. Taki sistem deluje v redu, dokler napadalec
ne uspe vstopiti v računalnik, kjer so hranjena gesla in si tako
brez težav prilasti gesla vseh uporabnikov. Seveda se skušamo
izogibati takemu sistemu, žal pa je še vedno veliko [takih spletnih
strežnikov](http://plaintextoffenders.com/about/), ki hranijo gesla v
čistopisu; nenazadnje nismo omenili takega velikana kot je Facebook
zaman: ravno v prvih mesecih leta 2019 smo izvedeli, da je Facebook
hranil gesla stotin milijonov uporabnikov v čistopisu, lastniki gesel
pa so seveda bili v nevarnosti.

```{r out.width=600, out.height=750,echo=FALSE}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/salt_hash_img.png"))
```

Drugi način hranjenja gesel je z zgoščeno vrednostjo. V spletnih
strežnikih je shranjena zgoščena vrednost gesla, tako da tudi če
bi napadalec imel dostop do strežnika, ne bi uspel izluščiti gesla
uporabnikov. Ta tehnika je sicer boljša kot prešnja, vendar ima
tudi ta nekaj pomanjkljivosti: najhujša je, da se ista gesla slikajo
v isto zgoščeno vrednost. To lahko ustvari težave, saj v primeru,
da ima veliko uporabnikov isto geslo in se na kakšen način uspemo
polastiti enega od teh gesel, potem bomo vedeli gesla vseh uporabnikov,
ki imajo isto zgoščeno vrednost gesla (pri tem predpostavimo, da
je verjetnost, da pride do trčenja, skoraj ničelna). Temu služijo
[mavrične tabele](https://en.wikipedia.org/wiki/Rainbow_table), ki
so vnaprej sestavljene tabele čistopisov in njihovih zgoščenih
vrednosti, v odvisnosti od zgoščevalne funkcije. Take tabele
izredno pohitrijo brute-force napade. Zaradi takega pristopa je
[Adobe](https://www.theguardian.com/technology/2013/nov/07/adobe-password-leak-can-check)
imel kar nekaj težav leta 2013.

Danes najučinkovitejša strategija je hranjenje gesel z zgoščenimi
funkcijami, ki pa se zgostijo skupaj z naključnim podatkom. To zagotovi,
da se bodo ista gesla slikala v različne zgoščene vrednosti.

Ker pa varnosti ni nikoli preveč, se lahko uporablja kombinacija
zgornjih pristopov in enkripcije. Tako strategijo uporablja na primer
[Dropbox](https://blogs.dropbox.com/tech/2016/09/how-dropbox-securely-stores-your-passwords/).


## Zgoščevalne funkcije v kriptografiji (Mia)

Upper examples should give you the feeling 

1. What characterization hash function should have, 
2. What are some counterexamples of the hash functions in real-world,
3. Where in computer science, security and cryptography, hash function becomes useful.

Next, it would be nice to see what are some examples
of hash function instatiations in computer science. We
will mention just some. For more examples follow the
[link](https://en.wikipedia.org/wiki/Hash_function_security_summary).

```{r out.width=600, out.height=700,echo=FALSE}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/shaTable.png"))
```

### SHA-0, SHA-1 {-}

The Secure Hash Algorithm (SHA) was published by NIST in 1993 [FIPS 180]
as part of the design specification of the Digital Signature Standard
(DSS). This hash function, often called SHA0, outputs 160-bit digests.

Two years later, in 1995, NIST updated the standard [FIPS 180-1] by
adding one extra instruction to the compression function. The resulting
function is called SHA1. NIST gave no explanation for this change, but
it was later found that this extra instruction is crucial for collision
resistance. SHA1 became the de-facto standard for collision resistant
hashing and was widely deployed.

The birthday attack can find collisions for SHA1 using an expected 280
evaluations of the function.


### SHA-256, SHA-386, SHA-512 (Denis) {-}

In 2002 NIST added [FIPS 180-2] two new hash functions to the SHA
family: SHA256 and SHA512. They output larger digests (256 and 512-bit
digests respectively) and therefore provide better protection against
the birthday attack. NIST also approved SHA224 and SHA384 which are
obtained from SHA256 and SHA512 respectively by truncating the output
to 224 and 384 bits.

```{r out.width=500, out.height=750,echo=FALSE}
sourceDir <- getSrcDirectory(function(dummy) {dummy})
# All defaults
include_graphics(paste0(normalizePath(sourceDir), "/static/img/Okrajšave_2.3.2_SHA-256_clear.png"))
```

The years 2004–5 were bad years for collision resistant hash
functions. A number of new attacks showed how to find collisions for
several hash functions. In particular, Wang, Yao, and Yao [121] presented
a collision finder for SHA1 that uses 263 evaluations of the function
— far less than the birthday attack. The first collision for SHA1,
using an improved algorithm, was found in 2017. As a result SHA1 is no
longer considered collision resistant, and should not be used.

You can try those functions on an arbitrary input using 
the following snippet, enjoy :)

<iframe src="./P2/hash_funkcije/hash_funkcije.html" 
id = 'hash-funkcije-iframe'></iframe>


### Birthday attacks on collision resistant hash functions (Denis)

A far more devastating attack can be built using the birthday paradox
discussed in Section above. Let $H$ be a hash function defined over
message - tag space $(M, T)$,  and let $N := |T|$. For standard hash
functions $N$ is quite large, for example $N = 2^256$ for SHA256. Without
the loss of the generality, we can assume that the size of $M$ is
at least $100N$. This basically means that messages being hashed are
slightly longer than the output digest. We describe a general collision
finder that finds collisions for $H$ after an expected $O(\sqrt{N})$
evaluations of $H$.

For comparison, the brute-force attack would have take $O(N)$ evaluations. 
This more effcient collision finder forces us to use much larger digests.
 
Let us now define one birthday collision finder for $H$: 

1. it chooses $s \approx \sqrt{N}$ random and independent messages 
	$m_{1},\dots,m_{s}$ from $M$, and looks for a collision among these 
	$s$ messages.

We will show that the birthday paradox implies that a collision is likely
to exist among these messages. More precisely, the birthday collision
finder works as follows:

BirthdayAttack Algorithm:

1. Sets $s$ to $upperInt(2 * \sqrt{N} )+1$
2. Generate s uniform random messages $m_{1}, . . . , m_{s}$ in M
3. Compute $x_{i} := H(m_{i})$ for all $i = 1,...,s$
4. Look for distinct $i,j \in \{1,...,s\}$ such that $H(m_{i}) = H(m_{j})$ 
5. If such $i$,$j$ exists and $m_{i} \not= m_{j}$ then <br />
      output the pair $(m_{i},m_{j})$

We argue that when the adversary picks $s \in upperInt(2\sqrt{N})+1$
random messages in $M$, then with probability at least $1/2$, there
will exist distinct $i,j$ such that $H(m_{i}) = H(m_{j})$ and $m_{i}
\not= m_{j}$. This means that the algorithm will output a collision with
probability at least $\frac{1}{2}$.

<script>
  iFrameResize({ log: true, warningTimeout: 10000}, '#hash-funkcije-iframe, #aplikacija1_RojstniDnevi-iframe,#aplikacija2_RojstniDnevi-iframe, #isbn-iframe, #emso-iframe, #plac_kart-iframe')
</script>

