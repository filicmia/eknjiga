library(shiny)
ui <- fluidPage(
  tags$head(tags$script(src = "vaja.js"),
  tags$link(rel = "stylesheet", type = "text/css", href = "vaja.css"))
)

server <- function(input, output, session) {
}

shinyApp(ui, server)