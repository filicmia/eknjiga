//popravi napise
//popravi izpis cikla

window.addEventListener('load', function(){
	var n;
	var mojRezultat;
	var forma=document.getElementById("forma");
	document.getElementById("poskusi").style.visibility="hidden";

	function inizializiraj(){
		//zbrisem vse obstojece vrstice
		document.getElementById("poskusi").style.visibility="visible";
		n=document.getElementById("n").value;
		document.getElementById("trenutniModul").innerHTML="<strong>Trenutni modul je "+n+"</strong>";
		n=parseInt(n, 10);

		generirajRacun();
	}
	inizializiraj();

	forma.addEventListener("submit", function(){
		e.preventDefault();
		inizializiraj();
	});

	function gcd(x, y) {
		x = Math.abs(x);
		y = Math.abs(y);
		while(y) {
			var t = y;
			y = x % y;
			x = t;
		}
		return x;
	}

	function inverzni(a){
		for(var i=1; i<n; i++){
			if((a*i)%n==1)return i;
		}
		return -1;
	}

	//poskusi ti
	document.getElementById("rand").addEventListener("click", generirajRacun);

	function generirajRacun(){
		document.getElementById("nok").style.visibility="hidden";
		document.getElementById("ok").style.visibility="hidden";
		document.getElementById("vnesiRez").value="";
		var a=Math.floor(Math.random()*n);
		var b=Math.floor(Math.random()*n);
		var c=Math.floor(Math.random()*4);
		var racun=document.getElementById("racun");

		if(c==0){
			//+
			mojRezultat=(a+b)%n;
			racun.innerHTML=a+" + "+b+" = ";
		}else if(c==1){
			//-
			mojRezultat=(a-b)%n;
			if(mojRezultat<0)mojRezultat+=n;
			racun.innerHTML=a+" - "+b+" = ";
		}else if(c==2){
			//*
			mojRezultat=(a*b)%n;
			racun.innerHTML=a+" <span>&#183;</span> "+b+" = ";
		}else{
			//:
			var inv=inverzni(b);
			while(inv==-1){
				var b=Math.floor(Math.random()*n);
				inv=inverzni(b);		
			}
			mojRezultat=(a*inv)%n;
			racun.innerHTML=a+" : "+b+" = ";
		}
	}

	document.getElementById("form").addEventListener("submit", function(e){
		e.preventDefault();
		var vneseno=document.getElementById("vnesiRez").value;
		document.getElementById("nok").style.visibility="visible";
		document.getElementById("ok").style.visibility="visible";
		vneseno=parseInt(vneseno, 10);
		while(vneseno<0)
			vneseno+=n;
		if(vneseno%n==mojRezultat)document.getElementById("nok").style.visibility="hidden";
		else document.getElementById("ok").style.visibility="hidden";
	});


});	