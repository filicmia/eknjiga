library(shiny)

#Important, get the current dir inside shiny app - fixing the problem of source not found when
#running it from RMarkdown file.
sourceDir <- getSrcDirectory(function(dummy) {dummy})
setwd(sourceDir)
#path = paste(sourceDir, "/other.R", sep="")

ui <- fluidPage(
  tags$head(tags$script(src = "drag-n-drop.js"),
            tags$link(rel = "stylesheet", type = "text/css", href = "drag-n-drop.css"),
            tags$script(src = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js")),
  mainPanel(
    includeHTML("drag-n-drop.html")
  )
)

server <- function(input, output, session) {
}

app = shinyApp(ui, server)