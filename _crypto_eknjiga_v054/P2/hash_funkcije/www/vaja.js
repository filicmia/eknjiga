
window.addEventListener("load", function(event) {
  //initialise the view.
  var funkcije = ["SHA-1","SHA-256","SHA-384","SHA-512"];
  var x = document.createElement('div'); // Create New Element div
  x.classList.add('main');
  
  var createform = document.createElement('div'); // Create New Element div
  createform.classList.add('content');

  //naslov
  var heading = document.createElement('h1');
  heading.innerHTML = "Zgoščevalne funkcije";
  createform.appendChild(heading);

  //3x div 
  var div1 = document.createElement('div');
  div1.setAttribute("id", "div1");
  var div2 = document.createElement('div');
  div2.setAttribute("id", "div2");
  var div3 = document.createElement('div');
  div3.setAttribute("id", "div3");

  createform.appendChild(div1);
  createform.appendChild(div2);
  createform.appendChild(div3);

  //polje za vpis 
  var texta = document.createElement('textarea');
  texta.setAttribute("rows", "8");
  texta.setAttribute("cols","40");
  texta.setAttribute("name","text");
  texta.setAttribute("id", "text");
  div1.appendChild(texta);


  //polje za izpis
  var textb = document.createElement('textarea');
  textb.setAttribute("rows", "8");
  textb.setAttribute("cols","40");
  textb.setAttribute("name","textb");
  textb.setAttribute("id", "textb");
  textb.setAttribute("readonly","readonly");
  div3.appendChild(textb);



  //select za izbiro hash funkcije
  var izbira = document.createElement('select');
  izbira.setAttribute("name","hashFunction");
  izbira.setAttribute("id", "hashFunction");
  izbira.setAttribute("multiple","multiple");
  izbira.setAttribute("size","4");

  //vse hash funkcije dam v select
  var i;
  for (i=0; i<funkcije.length;i++){
    var t = document.createElement('option');
    t.setAttribute("value", funkcije[i]);
    var u = document.createTextNode(funkcije[i]);
    t.appendChild(u);
    izbira.appendChild(t)

  }
  div2.appendChild(izbira);

  //button submit
  var submitelement = document.createElement('input');
  submitelement.setAttribute("type", "submit");
  submitelement.setAttribute("value", "Start");
  submitelement.setAttribute("dname", "Start");
  submitelement.id = "do";
  submitelement.addEventListener("click",function izpis(event){   //funkcija za izpis besedila in hash-a
    var m = document.getElementById("hashFunction");
    var besedilo = document.getElementById("text").value;
    var h_funkcija = m.options[m.selectedIndex].value;
  
    function hexString(buffer) {
      const byteArray = new Uint8Array(buffer);
    
      const hexCodes = [...byteArray].map(value => {
        const hexCode = value.toString(16);
        const paddedHexCode = hexCode.padStart(2, '0');
        return paddedHexCode;
      });
      return hexCodes.join('');
    }

    function digestMessage(message,c) {
      const encoder = new TextEncoder();
      const data = encoder.encode(message);
      return window.crypto.subtle.digest(c, data);
    }
    
    if(besedilo!=""){
      digestMessage(besedilo,h_funkcija).then(function(digestValue) {
        textb.innerHTML =hexString(digestValue);
      });
    }
    else{
      textb.innerHTML="";
    }
    

  });




  div2.appendChild(submitelement);
 

  
  
  x.appendChild(createform);
  document.body.appendChild(x);
  
});

