var size = 10;
var base = 10;

var rangeSizeConfig = {
    polyfill: false,
    onSlide: function(position, value){
        size = value;
        $("#label_size").text("Velikost tabele: " + size);
        
        draw();
    }
  }

var rangeBaseConfig = {
    polyfill: false,
    onSlide: function(position, value){
        base = value;
        
        $("#label_base").text("Baza: " + base);

        draw();
    }
  }

function drawAddTable() {

    var table_div = document.getElementById("table_add");
    table_div.innerHTML = "";

    var table = document.createElement('table');

    var header = table.createTHead();
    var hrow = header.insertRow(0);
    var th = document.createElement('th');
    th.innerHTML = "+";
    hrow.appendChild(th);
    for (var i=0; i<size; i++) {
        var th = document.createElement('th');
        th.innerHTML = convert(i);
        hrow.appendChild(th);
    }
    for (var i=0; i<size; i++) {
        var row = table.insertRow(i+1);
        
        var th = document.createElement('th');
        th.innerHTML = convert(i);
        row.appendChild(th); 
        
        
        for(var j=0; j<size; j++) {
            var cell = row.insertCell(j+1);
            var value = convert(i+j);
            cell.innerHTML = value;
            cell.setAttribute("style", "background-color:hsl(" + (parseInt(value, base))*(360/(size*2)) +", 80%, 70%);")
            cell.setAttribute("att-first", i);
            cell.setAttribute("att-second", j);
            cell.setAttribute("att-value", value);
            cell.setAttribute("att-operation", "add");
        }
    }

    table_div.appendChild(table);
    
}

function drawMulTable() {

    var table_div = document.getElementById("table_mul");
    table_div.innerHTML = "";

    var table = document.createElement('table');

    var header = table.createTHead();
    var hrow = header.insertRow(0);
    var th = document.createElement('th');
    th.innerHTML = "*";
    hrow.appendChild(th);
    for (var i=0; i<size; i++) {
        var th = document.createElement('th');
        th.innerHTML = convert(i);
        hrow.appendChild(th);
    }

    for (var i=0; i<size; i++) {
        var row = table.insertRow(i+1);
        
        var th = document.createElement('th');
        th.innerHTML = convert(i);
        row.appendChild(th); 
        
        for(var j=0; j<size; j++) {
            var cell = row.insertCell(j+1);
            var value = convert(i*j);
            cell.innerHTML = value;
            cell.setAttribute("style", "background-color:hsl(" + (parseInt(value, base))*(360/(size*size)) +", 80%, 70%);");
            cell.setAttribute("att-first", i);
            cell.setAttribute("att-second", j);
            cell.setAttribute("att-value", value);
            cell.setAttribute("att-operation", "mul");
        }
    }

    table_div.appendChild(table);
}

function convert(number) {
    return number.toString(base);
}

function draw() {

    $("#mul_op").html("");
    $("#add_op").html("");
    
    drawAddTable();
    drawMulTable(); 
}

function showCal(event) {

    draw();
    
    var first = event.target.getAttribute("att-first");
    var second = event.target.getAttribute("att-second");
    var value = event.target.getAttribute("att-value");
    var operation = event.target.getAttribute("att-operation");
    

    if(operation == "mul") {
        var factors = primeFactors(value);
        $("#mul_op").html(first + "*" + second + "=" + value + "=" + factorsToString(factors));
    } else if (operation =="add") {
        $("#add_op").html(first + "+" + second + "=" + value);
    }

    

    $(".table td").each(function(index,element) {
        if(element.getAttribute("att-value") == value) {
            element.setAttribute("style", "background-color:white");
        }
    })
}

function factorsToString(factors) {
    var string = factors[0];
    for(var i=1; i<factors.length; i++) {
        string += "*" + factors[i];
    }
    return string;
}

function primeFactors(n){
    var factors = [], 
        divisor = 2;
  
    while(n >= 2){
      if(n % divisor == 0){
         factors.push(divisor); 
         n= n / divisor;
      }
      else{
        divisor++;
      }     
    }
    return factors;
  }
  

window.addEventListener("load", function(event) {
    $("#range_size").rangeslider(rangeSizeConfig);
    $("#range_base").rangeslider(rangeBaseConfig);

    this.document.getElementById("table_mul").addEventListener("click", showCal);
    this.document.getElementById("table_add").addEventListener("click", showCal);

    draw();
});