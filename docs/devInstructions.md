## Embeding the picture into the ebook

V datoteki

`P3.Rmd`

najdes primer, kako mora biti vstavljena slika:
```{r SASA, out.width=400, echo=FALSE, fig.cap="CD in veliko vec", fig.align="right"}
include_graphics("static/img/scd.png")
```

Pravtako pa najdes tudi primer sklica na sliko:

    diski/zgoščenke (CD), glej sliko \@ref(fig:SASA).

Vsaka slika mora biti vstavljena v `P/static/img/` diretorij.

## Embeding a new interaction (html snippet) into ebook

1. Set the interaction in the folder 'interaction-name',
2. Place the folder under P/P* folder. Inmeaning, if
your interaction corresponds to the 4th chapter of the book, 
then, you place it in P/P4, if it cirresponds to chapter 5,
then place it under P/P5 folder, etc.
4. Find the relative path from your app (currently 
located under P/P*/interaction-name/) `.html` file
and `P/js-libs/iframeResizer.contentWindow.min.js` file.
Let us call it `resizer-path`
5. (iframe resizing incuding into the interaction) Open the main .html file of your application (currently 
located under P/P*/interaction-name/) and include js script
located under `resizer-path`, i.e.
`<script src='../../js-libs/iframeResizer.contentWindow.min.js'></script>`
where `resizer-path = '../../js-libs/iframeResizer.contentWindow.min.js'`
6. (iframe resizing biding in the RmD file) Find the `.RmD` file of the chapter corresponding to your application.
It is usually named P*.RmD, where * denotes the chapter of the book, and 
placed under P folder (slovenian version, or under P/angl for english version).
7. Place 
`<iframe src="./P2/emso_isbn_plac/isbn.html" id = 'isbn-iframe'></iframe>`
   where `"./P2/emso_isbn_plac/isbn.html"` denotes the relative path from the
   selected `P*.RmD` file (in step before)
 to the interaction's (main) html file. The `id` attribute should be unique 
 for each application, i.e. 'interaction-name-iframe'.
8. Take the `id` of interaction just included and append it to the list
placed at the end of the selected `P*.RmD` file. For example, if you find
```
<script>
  iFrameResize({ log: true, warningTimeout: 10000}, '#hash-funkcije-iframe, #aplikacija1_RojstniDnevi-iframe,#aplikacija2_RojstniDnevi-iframe, #isbn-iframe, #emso-iframe, #plac_kart-iframe')
</script>
```
you transfer it into 
```
<script>
  iFrameResize({ log: true, warningTimeout: 10000}, '#hash-funkcije-iframe, #aplikacija1_RojstniDnevi-iframe,#aplikacija2_RojstniDnevi-iframe, #isbn-iframe, #emso-iframe, #plac_kart-iframe, #id')
</script>
```
where id is equal to the `id` of the iframe embeding the interaction into ebook.
 If there is no one, define the new one, i.e.
 ```
<script>
  iFrameResize({ log: true, warningTimeout: 10000}, '#id')
</script>
```
where id is equal to the `id` of the iframe embeding the interaction into ebook.
9. Then build the ebook (explained in the next chapter). Test if everything
   works fine. If not observe section **Known issues that might occur (with
   solutioins)**, it might help. If not, then try to contact me (@filicmia,
   gmail is with the same username.) or some other participant of the project.
10. (Unify the font) In `.css` file of your interaction's (main) file, style 
the html  tag so its  font is set to:
`font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;`
i.e. 
```{css }
html * 
{
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}
```

### Known issues that might occur (with solutioins)

**Issue 1** <br>
After including the interation and (re)building the book, I can see only part of
the interaction. <br>
*Soluton 1:* <br>
Add  styling
```{css }
body * {height: auto !important;}
```
into the `.css` file of the interaction's (main) html file. <br>

*Soluton 2:* <br>
Add  styling
```{css }
body * {height: auto !important;}
```
and remove `position: absolute;` style from the `div` that is not showing properly.

**Issue 2** <br>
After including the interation and (re)building the book, the interaction is 
height-scrollable. <br>
*Soluton 1:* <br>
This happens because iframe resizing is not working as either:
1. Iframe resizng script is not properly included into
your interaction (main html file) - step (iframe resizing incuding into the
   interaction),
2. or it is not properly binded in the .RmD file - step (iframe resizing biding
   in the RmD file)

**Issue 3** <br>
After including the interation and (re)building the book, the interaction-space
in the ebook is reducing after every click. <br>
*Soluton 1:* <br>
Add  styling
```{css }
body * {height: auto !important;}
```
and remove `position: absolute;` style from the `div` that is becoming smaller
and smaller.

**Issue 4**<br>
Lower corners are missing.<br>
*Soluton 1:* <br>
Try Solution for issue 3. <br>

*Soluton 2:* <br>
Add  styling
```{css }
body {padding-top: 0.3rem !important;}
```
where 0.3 can be modified, depending on the ebook goal-style.

## Build the ebook 

```
# Get the current git directory from source (if not having one).
...
cd eknjiga/
git checkout edi # checkout YOUR branch 
git pull # get all changes from online.

git add .
git commit -m "popravki"
git push
cd P/


cd P/
# vi P2.Rmd # optional: make changes.
./build.command 

# Observe the ebook built: Open index.html from _crypto_eknjiga/
cd _crypto_eknjiga/
#firefox index.html  #insted of firefox, use yor own browser. Or open in Finder/File manager.
cd ..
cd .. # cd into eknjiga.
git add .
git commit -m 'message'
git push
```

## Changing the visibity of the chapter (each .Rmd file content) in the ebook.
After the built (running `./build.command` within P chapter), you will be
presented with the ebook, saved under `P/_crypto_eknjiga`. The ebook will 
combine all `.Rmd` defined in `P/_bookdown.yml` file, under `rmd_files`. <br>

For example. If the content of the `P/_bookdown.yml` is the following
![text](img/devInst_bookdownCont.png)<br>
then the ebook will hold only the content of following `.Rmd` files:

*  P/index.Rmd,
*  P/P1.Rmd,
*  P/P2.Rmd,
*  P/P4.Rmd,
*  P/P5.Rmd,
*  P/reference.Rmd



## Style - colors
Depending on the Chapter in which your interaction fits, use the color fro m the list below as a base/main color of the application. For all color issues contact @NinaHostnik

### Gesla in Virusi / Računalniška Varnost: P1

	Hex: #01A3D9

	C  74,72 %    R  1      H  194,95 °
	M  18,2  %    G  163    S  99,75  %
	Y  1,94  %    B  217    B  84,96  %
	K  0     %

### Okrajšave / Zgoščevalne funkcije: P2

	Hex: #4CC3C6

	C  63,05 %    R  76     H  181,57 °
	M  0     %    G  195    S  61,5   %
	Y  25,71 %    B  198    B  77,55  %
	K  0     %

### Kode / Teorija kodiranja: P3

	Hex: #3CBC8D

	C  68,98 %    R  60     H  158,00 °
	M  0     %    G  188    S  52,00  %
	Y  25,00 %    B  141    B  49,00  %
	K  26,00 %

### Šifre / Simetrični kriptosistemi: P4

	Hex: #056B8D

	C  91,09 %    R  5      H  195,1  °
	M  51,32 %    G  107    S  96,47  %
	Y  29,47 %    B  141    B  55,16  %
	K  6,84  %

### Računala / Aritmetika in Algoritmi: P5

	Hex: #268080

	C  82,46 %    R  38     H  179,82 °
	M  32,51 %    G  128    S  70,4   %
	Y  48,57 %    B  128    B  50,36  %
	K  8,2   %

### Podpisi / Asimetrični kriptosistemi: P6

	Hex: #1F7857

	C  84,94 %    R  31     H  157,77 °
	M  30,3  %    G  120    S  74,21  %
	Y  76,5  %    B  87     B  47,2   %
	K  16,21 %

### Protokoli / Kriptografske sheme: P7

	Hex: #003A4D

	C  97,15 %    R  0     H  194,63 °
	M  68,57 %    G  58    S  99,97  %
	Y  48,54 %    B  77    B  30,09  %
	K  41,4  %

### Loto / Generator naključnih števil:

	Hex: #134040

	C  88,51 %    R  19    H  180,06 °
	M  54,81 %    G  64    S  71,14  %
	Y  61,97 %    B  64    B  25,2   %
	K  48,41 %

### Verige / Bločne verige:

	Hex: #0F3829

	C  85,24 %    R  15    H  157,71 °
	M  49,63 %    G  56    S  73,02  %
	Y  77,68 %    B  41    B  21,95  %
	K  60,84 %
