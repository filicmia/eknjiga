library(shiny)
ui <- fluidPage(
  tags$head(tags$script(src = "message-handler.js"),
            tags$link(rel = "stylesheet", type = "text/css", href = "message-handler.css")),
  tags$body(
    tags$div(id='cchiperWrapper'))
)

server <- function(input, output, session) {
}

shinyApp(ui, server)
