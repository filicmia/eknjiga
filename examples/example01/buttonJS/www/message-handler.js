// This recieves messages of type "testmessage" from the server.
// See http://shiny.rstudio.com/gallery/server-to-client-custom-messages.html
// for details
var caesarShift = function(str, amount) {

	// Wrap the amount
	if (amount < 0)
		return caesarShift(str, amount + 26);

	// Make an output variable
	var output = '';

	// Go through each character
	for (var i = 0; i < str.length; i ++) {

		// Get the character we'll be appending
		var c = str[i];

		// If it's a letter...
		if (c.match(/[a-z]/i)) {

			// Get its code
			var code = str.charCodeAt(i);

			// Uppercase letters
			if ((code >= 65) && (code <= 90))
				c = String.fromCharCode(((code - 65 + amount) % 26) + 65);

			// Lowercase letters
			else if ((code >= 97) && (code <= 122))
				c = String.fromCharCode(((code - 97 + amount) % 26) + 97);

		}

		// Append
		output += c;

	}

	// All done!
	return output;
};

function createButton(context, value, func) {
    var button = document.createElement("input");
    button.type = "button";
    button.value = value;
    button.onclick = func;
    context.appendChild(button);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function shiftAllLetters(event){
  if(isBlank(event.target.value)) return;
  var shift = event.target.value.charCodeAt(0)  - textDec[event.target.id].charCodeAt(0) ;
  //alert(shift);
  var div = document.getElementById('chiperButtons');
  var i;
  for (i = 0; i < div.childNodes.length; i++) { 
    var node = div.childNodes[i];
    node.value = caesarShift(textDec[i],shift);
  }
}

function createTextInput(context, value, id ,func) {
    var elem = document.createElement("input");
    elem.type = "text";
    elem.value = value;
    elem.addEventListener('input', func);
    elem.id = id;
    context.appendChild(elem);
}


textDec = [];

window.addEventListener("load", function(event) {
  //initialise the view.
  var x = document.createElement('div'); // Create New Element div
  x.classList.add('main');
  
  var createform = document.createElement('div'); // Create New Element div
  createform.classList.add('content');
  
  var heading = document.createElement('h2'); // Heading of Form
  heading.innerHTML = "Decryption form";
  createform.appendChild(heading);
  
  var line = document.createElement('hr'); // Giving Horizontal Row After Heading
  createform.appendChild(line);
  
  var linebreak = document.createElement('br');
  createform.appendChild(linebreak);
  
  var namelabel = document.createElement('label'); // Create Label for Name Field
  namelabel.innerHTML = "Write chipertext to dechiper : "; // Set Field Labels
  createform.appendChild(namelabel);
  
  var inputelement = document.createElement('input'); // Create Input Field for chipertext
  inputelement.setAttribute("type", "text");
  inputelement.setAttribute("name", "chipertext");
  inputelement.id = "chipertext";
  createform.appendChild(inputelement);
  
  var submitelement = document.createElement('input'); // Append Submit Button
  submitelement.setAttribute("type", "submit");
  submitelement.setAttribute("value", "Start");
  submitelement.setAttribute("dname", "Start");
  submitelement.id = "do";
  submitelement.addEventListener("click", function(event) {
    
    var input = document.getElementById('chipertext');
    textDec=input.value.replace(/\s+/g,"").split('');

    var div = document.getElementById('chiperButtons');
    div.innerHTML = "";
    
    i = 0;
    while (i < textDec.length){
      createTextInput(div, textDec[i], i, shiftAllLetters);
      i = i+1;
    }
    document.getElementById('cchiperWrapper').appendChild(div);
  });
  
  
  createform.appendChild(submitelement);
  
  x.appendChild(createform);
  
  var wrapper = document.getElementById('cchiperWrapper');
  wrapper.appendChild(x);
  
  var chiperButtons = document.createElement('div'); // Create Input Field for chipertext
  chiperButtons.id = "chiperButtons";
  wrapper.appendChild(chiperButtons);
});