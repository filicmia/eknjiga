## Enviroment setup

1. First you need to install R.
For writing and executing R/RMarkdown code I recommend installing RStudio IDE as well. It is specalized to work with R and 
all connected with R.  [The instalation](https://happygitwithr.com/install-r-rstudio.html) of R and RStudio is pretty easy: 
    1. Install a pre-compiled binary of R for your OS from here:
    https://cloud.r-project.org
    2. Install RStudio Desktop.
    3. Update your R packages: write 
    
    ```
    update.packages(ask = FALSE, checkBuilt = TRUE)
    ``` 
    in R terminal.
2. Install RMarkdown and Shiny package 
```
install.packages("shiny")
install.packages("rmarkdown")
install.packages('htmlwidgets')
``` 
3. Now everything is set to create your first RMarkdown + Shiny interactive document!


## Creating your first interactive document

1. Open RStudio
2. Create new project
![Creating new RMarkdown project](img/RStudio_newProject.png)
> If doing with RMarkdown (not just Markdown - what readme.md actually is), you are able to import the picture using R as well with:
> include_graphics("img/RStudio_newProject.png")

3. Create R Markdown document
![Creating first RMarkdown document](img/RStudio_newRMarkdownDocument.png)

4. Run it (Knit the current document).
![Running RMarkdown Document](img/RStudio_runRMarkdownDocument.png)

> * You should see the html document preview on the left.
> * First 10 lines of the RMarkdown document consider as defult, you do not to chenge them.

5. Add your own content to the document (i.e. sampleText.txt).
6. Add interactive part using shiny.
    1. Put 
    ````
    ```{r, echo = FALSE}
        shinyAppDir("name/",
          options = list(width = "100%", height = 150)
        )
    ```
    ````

    in the place of the document where you want the interaction to happen. 

    >The `name` can be changed into any name that you like.
    > It you run your RmD file now, you willl get an error.
        
    2. Create shiny app called `name`:
        1. Create folder called `name`
        2. Create `app.R` file inside it. 
        Example of `app.R`:
        
    ```
    library(shiny)
    ui <- fluidPage(
      tags$head(tags$script(src = "message-handler.js"),
                tags$link(rel = "stylesheet", type = "text/css", href = "message-handler.css")),
      tags$body(
        tags$div(id='cchiperWrapper'))
    )

    server <- function(input, output, session) {
    }

    shinyApp(ui, server)
    ```
      3. Create custom `message-handler.js` and `message-handler.css` files. `.css` can be set as empty.
      We do not care about the css design at this point. \
      Example of `message-handler.js` can be found [here](buttonJS/www/message-handler.js).\
      Extra: Example of `message-handler.css` can be found [here](buttonJS/www/message-handler.css).\
      
      >Put `.js` and `.css` files into `name/www` folder.
      
    3. Set runtime to shiny in the .RmD 6th line of the document.
7. If you like, do some more changes of the content (i.e. text).

Note:

1. I recomend to create one shiny application per one interactive snippet.
2. [Example01 Shiny application](https://mathtravel.shinyapps.io/main/)
3. Example01 Shiny application source code:

        * [RMarkdown part](main.RmD)
        * [Shiny part](buttonJS)