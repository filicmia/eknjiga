function myFunction() {
  alert("I am an alert box!");
}

window.addEventListener("load", function(event) {
  var btn = document.getElementById('alertButton');
  btn.addEventListener('click', myFunction);
});