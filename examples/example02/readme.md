## Enviroment setup

1. First you need to install R.
For writing and executing R/RMarkdown code I recommend installing RStudio IDE as well. It is specalized to work with R and 
all connected with R.  [The instalation](https://happygitwithr.com/install-r-rstudio.html) of R and RStudio is pretty easy: 
    1. Install a pre-compiled binary of R for your OS from here:
    https://cloud.r-project.org
    2. Install RStudio Desktop.
    3. Update your R packages: write 
    
    ```
    update.packages(ask = FALSE, checkBuilt = TRUE)
    ``` 
    in R terminal.
2. Install RMarkdown and Shiny package 
```
install.packages("shiny")
install.packages("rmarkdown")
install.packages('htmlwidgets')
``` 
3. Now everything is set to create your first RMarkdown + Shiny interactive document!


## Creating your first interactive document

1. Open RStudio
2. Create new project
![Creating new RMarkdown project](img/RStudio_newProject.png)
> If doing with RMarkdown (not just Markdown - what readme.md actually is), you are able to import the picture using R as well with:
> include_graphics("img/RStudio_newProject.png")

3. Create R Markdown document
![Creating first RMarkdown document](img/RStudio_newRMarkdownDocument.png)

4. Run it (Knit the current document).
![Running RMarkdown Document](img/RStudio_runRMarkdownDocument.png)

> * You should see the html document preview on the left.
> * First 10 lines of the RMarkdown document consider as defult, you do not to chenge them.

5. Add your own content to the document: 

    1. Put 
    ````
    <iframe src="./alertButton.html" id = 'alert-button-iframe'></iframe>
    ````

    in the place of the document where you want the `alert-button` interaction to happen. 
        
    2. Run the document.
6. If you like, change the rest of the auto-generated document.

Note:

1. Create one html document per one interactive snippet.