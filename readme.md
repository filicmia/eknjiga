## Automatic deployment
defined in `.gitlab-ci.yml` is jot yet working. I guess @jaanos will handle that
in his way :)

## Style - colors
Depending on the Chapter in which your interaction fits, use the color fro m the list below as a base/main color of the application. For all color issues contact @NinaHostnik

### Gesla in Virusi / Računalniška Varnost: P1

	Hex: #01A3D9

	C  74,72 %    R  1      H  194,95 °
	M  18,2  %    G  163    S  99,75  %
	Y  1,94  %    B  217    B  84,96  %
	K  0     %

### Okrajšave / Zgoščevalne funkcije: P2

	Hex: #4CC3C6

	C  63,05 %    R  76     H  181,57 °
	M  0     %    G  195    S  61,5   %
	Y  25,71 %    B  198    B  77,55  %
	K  0     %

### Kode / Teorija kodiranja: P3

	Hex: #3CBC8D

	C  68,98 %    R  60     H  158,00 °
	M  0     %    G  188    S  52,00  %
	Y  25,00 %    B  141    B  49,00  %
	K  26,00 %

### Šifre / Simetrični kriptosistemi: P4

	Hex: #056B8D

	C  91,09 %    R  5      H  195,1  °
	M  51,32 %    G  107    S  96,47  %
	Y  29,47 %    B  141    B  55,16  %
	K  6,84  %

### Računala / Aritmetika in Algoritmi: P5

	Hex: #268080

	C  82,46 %    R  38     H  179,82 °
	M  32,51 %    G  128    S  70,4   %
	Y  48,57 %    B  128    B  50,36  %
	K  8,2   %

### Podpisi / Asimetrični kriptosistemi: P6

	Hex: #1F7857

	C  84,94 %    R  31     H  157,77 °
	M  30,3  %    G  120    S  74,21  %
	Y  76,5  %    B  87     B  47,2   %
	K  16,21 %

### Protokoli / Kriptografske sheme: P7

	Hex: #003A4D

	C  97,15 %    R  0     H  194,63 °
	M  68,57 %    G  58    S  99,97  %
	Y  48,54 %    B  77    B  30,09  %
	K  41,4  %

### Loto / Generator naključnih števil:

	Hex: #134040

	C  88,51 %    R  19    H  180,06 °
	M  54,81 %    G  64    S  71,14  %
	Y  61,97 %    B  64    B  25,2   %
	K  48,41 %

### Verige / Bločne verige:

	Hex: #0F3829

	C  85,24 %    R  15    H  157,71 °
	M  49,63 %    G  56    S  73,02  %
	Y  77,68 %    B  41    B  21,95  %
	K  60,84 %

## Build the ebook 

```
# Get the current git directory from source (if not having one).
git clone https://gitlab.com/kriptogram/eknjiga.git
cd eknjiga/
git checkout -b my-new-build-branch # checkout new branch 
cd P/
# vi P2.Rmd # optional: make changes.
./build.command 

# Observe the ebook built: Open index.html from _crypto_eknjiga/
cd _crypto_eknjiga/
#firefox index.html  #insted of firefox, use yor own browser. Or open in Finder/File manager.
```