## Build the ebook 

[Developer's
instructions](https://gitlab.com/kriptogram/eknjiga/blob/devel/docs/devInstructions.md)
under **Build the ebook**

## Embed new interaction (html snippet) into ebook

[Developer's instructions](https://gitlab.com/kriptogram/eknjiga/blob/devel/docs/devInstructions.md)